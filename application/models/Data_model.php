<?php
/**
*
*/
class Data_model extends CI_Model
{
	public function instansi()
	{
		return $this->db->get('tr_instansi');
	}
	public function getKecamatan()
	{
		return $this->db->get('mst_kecamatan');
	}
	public function getNamaKecamatan($id_kecamatan)
	{
		$this->db->where('id_kecamatan',$id_kecamatan);
		$query = $this->db->get('mst_kecamatan');
		foreach ($query->result() as $row) {
			return $row->nm_kecamatan;
		}
	}
	public function getUserInfo($data)
	{
		$this->db->where($data);
		return $this->db->get('t_user',$data);
	}
	public function getKategori()
	{
		return $this->db->get('t_kategori');
	}
	public function getKategori_ById($id)
	{
		$this->db->where('kd_kategori',$id);
		return $this->db->get('t_kategori');
	}
	public function kategori_edit($id,$data)
	{
		$this->db->set($data);
		$this->db->where('kd_kategori',$id);
		$this->db->update('t_kategori');
	}
	public function kategori_delete($id)
	{
		$this->db->where('kd_kategori',$id);
		$this->db->delete('t_kategori');
	}
	public function getSubKategori($kd_kategori)
	{
		$this->db->where('kd_kategori',$kd_kategori);
		return $this->db->get('t_kategori_sub');
	}
	public function getSubKategori2($kd_subkategori)
	{
		$this->db->join('t_kategori','t_kategori_sub.kd_kategori = t_kategori.kd_kategori');
		$this->db->where('kd_subkategori',$kd_subkategori);
		return $this->db->get('t_kategori_sub');
	}
	public function subKategori()
	{
		return $this->db->get('t_kategori_sub');
	}
	public function getJenis()
	{
		return $this->db->get('t_jenis');
	}
	public function getSubjenis($kd_jenis)
	{
		$this->db->where('kd_jenis',$kd_jenis);
		return $this->db->get('t_subjenis');
	}
	public function getSubjenis2()
	{
		return $this->db->get('t_subjenis');
	}
	public function getSubsubjenis($kd_subjenis)
	{
		$this->db->where('kd_subjenis',$kd_subjenis);
		return $this->db->get('t_subsubjenis');
	}
	public function getPengguna()
	{
		$this->db->join('t_kategori','t_pengguna.kd_kategori = t_kategori.kd_kategori');
		$this->db->join('t_kategori_sub','t_pengguna.kd_subkategori = t_kategori_sub.kd_subkategori');
		$this->db->join('mst_kecamatan','t_pengguna.id_kecamatan = mst_kecamatan.id_kecamatan');
		return $this->db->get('t_pengguna');
	}
	public function getPengguna2($kd_pengguna)
	{
		$this->db->join('t_kategori','t_pengguna.kd_kategori = t_kategori.kd_kategori');
		$this->db->join('t_kategori_sub','t_pengguna.kd_subkategori = t_kategori_sub.kd_subkategori');
		$this->db->join('mst_kecamatan','t_pengguna.id_kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->where('t_pengguna.kd_pengguna',$kd_pengguna);
		return $this->db->get('t_pengguna');
	}
	public function getPtera2($kd_ptera)
	{
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		//$this->db->join('t_subsubjenis', 't_subjenis.kd_subjenis = t_subsubjenis.kd_subjenis');
		$this->db->where('t_ptera.kd_ptera',$kd_ptera);
		return $this->db->get('t_ptera');
	}
	public function getPtera_online($no_reg)
	{
		$this->db->join('t_pengguna_online', 't_ptera_online.kd_pengguna = t_pengguna_online.kd_pengguna');
		$this->db->join('mst_kecamatan','t_pengguna_online.id_kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('t_kategori','t_pengguna_online.kd_kategori = t_kategori.kd_kategori');
		$this->db->join('t_kategori_sub','t_pengguna_online.kd_subkategori = t_kategori_sub.kd_subkategori');
		$this->db->join('t_jenis', 't_ptera_online.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera_online.kd_subjenis = t_subjenis.kd_subjenis');
		//$this->db->join('t_subsubjenis', 't_ptera_online.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		$this->db->where('t_ptera_online.no_reg',$no_reg);
		return $this->db->get('t_ptera_online');
	}

	function cek_masa_berlaku($data)
	{
		$this->db->join('t_subjenis','t_subjenis.kd_subjenis = t_ptera.kd_subjenis');
		$this->db->join('t_pengguna','t_pengguna.kd_pengguna = t_ptera.kd_pengguna');
		$this->db->where($data);
		return $this->db->get('t_ptera');
	}

	public function getPenggunaByKd_pengguna($kd_pengguna)
	{
		$this->db->join('t_kategori','t_pengguna.kd_kategori = t_kategori.kd_kategori');
		$this->db->join('t_kategori_sub','t_pengguna.kd_subkategori = t_kategori_sub.kd_subkategori');
		$this->db->join('mst_kecamatan','t_pengguna.id_kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('t_ptera','t_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->where('t_pengguna.kd_pengguna',$kd_pengguna);
		return $this->db->get('t_pengguna');
	}
	public function getPenggunaByKd_penggunaOnline($kd_pengguna)
	{
		$this->db->join('t_kategori','t_pengguna_online.kd_kategori = t_kategori.kd_kategori');
		$this->db->join('t_kategori_sub','t_pengguna_online.kd_subkategori = t_kategori_sub.kd_subkategori');
		$this->db->join('mst_kecamatan','t_pengguna_online.id_kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->where('kd_pengguna',$kd_pengguna);
		return $this->db->get('t_pengguna_online');
	}
	public function getPenggunaByKd_pengguna2($kd_pengguna,$kd_ptera)
	{
		/* $this->db->join('t_kategori','t_pengguna.kd_kategori = t_kategori.kd_kategori');
		$this->db->join('t_kategori_sub','t_pengguna.kd_subkategori = t_kategori_sub.kd_subkategori');
		$this->db->join('mst_kecamatan','t_pengguna.id_kecamatan = mst_kecamatan.id_kecamatan');
		$this->db->join('t_ptera','t_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis'); */

		$this->db->where('t_ptera.kd_ptera',$kd_ptera);
		$this->db->where('t_ptera.kd_pengguna',$kd_pengguna);

		return $this->db->get('t_ptera');
	}
	public function getPtera()
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		return $this->db->get('t_ptera');
	}
	public function getPteraByNoTrans($no_trans)
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->where('no_trans',$no_trans);
		return $this->db->get('t_ptera');
	}
	public function getPtera_per_kecamatan($id_kecamatan)
	{
		$str = $id_kecamatan;
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->where("kd_ptera LIKE '".$str."%'");
		return $this->db->get('t_ptera');
	}

	public function early_warning_sys_satu_bln($id_kecamatan)
	{
		$str = substr($id_kecamatan, 0,2);
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->where("kd_ptera LIKE '".$str."%'");
		//$this->db->where("masaberlaku LIKE '_____".date('m')."%'");
		//$this->db->where("datediff(masaberlaku, CURRENT_DATE) >0");
		//$this->db->where("datediff(masaberlaku, CURRENT_DATE) >= 0");
		$this->db->where("datediff(masaberlaku, CURRENT_DATE) <= 30");
		return $this->db->get('t_ptera');
	}

	public function getListUser()
	{
		return $this->db->get('t_user');
	}
	public function tambah_user($data)
	{
		$this->db->insert('t_user',$data);
	}
	public function get_user($id)
	{
		$this->db->where('id_user',$id);
		return $this->db->get('t_user');
	}
	public function edit_user($id,$data)
	{
		$this->db->set($data);
		$this->db->where('id_user',$id);
		$this->db->update('t_user');
	}
	public function user_hapus($id)
	{
		$this->db->where('id_user',$id);
		$this->db->delete('t_user');
	}
	public function getNo_urut($id_kecamatan)
	{
		$this->db->where('id_kecamatan',$id_kecamatan);
		$get =  $this->db->get('t_pengguna');
		return $get->num_rows()+1;
	}
	public function getNo_urut_online($id_kecamatan)
	{
		$this->db->where('id_kecamatan',$id_kecamatan);
		$get =  $this->db->get('t_pengguna_online');
		return $get->num_rows()+1;
	}
	function tera_baru($data_pegguna,$data_ptera)
	{
		$this->db->insert('t_pengguna',$data_pegguna);
		$this->db->insert('t_ptera',$data_ptera);
	}
	function tera_baru2($data_ptera)
	{
		$this->db->insert('t_ptera',$data_ptera);
	}
	public function getDataUjiBaru()
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		$this->db->where('t_ptera.hasil_uji',NULL);
		$this->db->where('t_ptera.status2','baru');
		return $this->db->get('t_ptera');
	}
	public function getDataUjiUlang()
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		$this->db->where('t_ptera.hasil_uji',NULL);
		$this->db->where('t_ptera.status2','ulang');
		return $this->db->get('t_ptera');
	}
	public function konfirmasi_uji($no_reg,$data)
	{
		$this->db->where('no_reg',$no_reg);
		$this->db->set($data);
		$this->db->update('t_ptera');
	}
	public function getDataTera()
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		$this->db->where('t_ptera.cap_tera',NULL);
		$this->db->where('t_ptera.hasil_uji IS NOT NULL',NULL,FALSE);
		return $this->db->get('t_ptera');
	}
	public function konfirmasi_tera($no_reg,$data)
	{
		$this->db->where('no_reg',$no_reg);
		$this->db->set($data);
		$this->db->update('t_ptera');
	}
	public function getDataPenyerahan()
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		//$this->db->where('t_ptera.status','proses');
		$this->db->where('tgl_serah','0000-00-00');
		$this->db->where('t_ptera.jml_bayar >',0);
		return $this->db->get('t_ptera');
	}
	public function konfirmasi_penyerahan($no_reg,$tgl_serah)
	{
		$this->db->where('no_reg',$no_reg);
		$this->db->set('status','selesai');
		$this->db->set('tgl_serah',$tgl_serah);
		$this->db->update('t_ptera');
	}
	public function getDataPembayaran()
	{
		$current_date = date('Ymd');
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		//$this->db->where('t_ptera.tgl_daftar = ',$current_date);
		$this->db->where('t_ptera.cap_tera','Ada');
		$this->db->where('t_ptera.jml_bayar',0);
		return $this->db->get('t_ptera');
	}
	public function konfirmasi_pembayaran($no_reg,$data)
	{
		$this->db->where('no_reg',$no_reg);
		$this->db->set($data);
		$this->db->update('t_ptera');
	}
	public function cetak_bukti($kd_pengguna,$no_reg)
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->where('t_ptera.kd_pengguna',$kd_pengguna);
		$this->db->where('t_ptera.no_reg',$no_reg);
		return $this->db->get('t_ptera');
	}
	public function cetak_bukti_online($kd_pengguna,$no_reg)
	{
		$this->db->join('t_pengguna_online', 't_ptera_online.kd_pengguna = t_pengguna_online.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera_online.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera_online.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->where('t_ptera_online.kd_pengguna',$kd_pengguna);
		$this->db->where('t_ptera_online.no_reg',$no_reg);
		return $this->db->get('t_ptera_online');
	}
	public function cetak_bukti_tera_ulang_online($kd_pengguna,$no_reg)
	{
		$this->db->join('t_pengguna', 't_ptera_online.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera_online.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera_online.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->where('t_ptera_online.kd_pengguna',$kd_pengguna);
		$this->db->where('t_ptera_online.no_reg',$no_reg);
		return $this->db->get('t_ptera_online');
	}
	public function cetak_bukti_bayar($kd_pengguna)
	{
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->where('t_ptera.kd_pengguna',$kd_pengguna);
		return $this->db->get('t_ptera');
	}

	public function getLastRec()
	{
		$SQL = "SELECT no_trans FROM t_ptera ORDER BY no_trans DESC LIMIT 1";
		$query = $this->db->query($SQL);
		foreach($query->result() as $row){
			return $row->no_trans;
		}
	}
	public function update_no_reg($no_reg,$no_trans)
	{
		$this->db->set('no_reg',$no_reg);
		$this->db->where('no_trans',$no_trans);
		$this->db->update('t_ptera');
	}
	function tera_baru_online($data_pegguna,$data_ptera)
	{
		$this->db->insert('t_pengguna_online',$data_pegguna);
		$this->db->insert('t_ptera_online',$data_ptera);
	}
	function tera_baru_online2($data_ptera)
	{
		$this->db->insert('t_ptera_online',$data_ptera);
	}
	public function getLastRecOnline()
	{
		$SQL = "SELECT no_trans FROM t_ptera_online ORDER BY no_trans DESC LIMIT 1";
		$query = $this->db->query($SQL);
		foreach($query->result() as $row){
			return $row->no_trans;
		}
	}
	public function update_no_reg_online($no_reg,$no_trans)
	{
		$this->db->set('no_reg',$no_reg);
		$this->db->where('no_trans',$no_trans);
		$this->db->update('t_ptera_online');
	}
	public function cek_t_pengguna_online($kd_pengguna)
	{
		$this->db->where('kd_pengguna',$kd_pengguna);
		return $this->db->get('t_pengguna_online');
	}
	public function bog_sel_exp()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '10%' AND (datediff(masaberlaku, current_date())) < 0 AND status = 'expired' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where('kd_ptera LIKE','10%');
		$this->db->where('status','expired');
		//return $this->db->query($SQL)->num_rows();
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_sel_act()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '10%' AND (datediff(masaberlaku, current_date())) > 0 AND status = 'selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where('kd_ptera LIKE','10%');
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_tim_exp()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '20%' AND (datediff(masaberlaku, current_date())) < 0 AND status = 'expired' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		//return $this->db->query($SQL)->num_rows();
		$this->db->where('kd_ptera LIKE','20%');
		$this->db->where('status','expired');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_tim_act()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '20%' AND (datediff(masaberlaku, current_date())) > 0 AND status = 'selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where('kd_ptera LIKE','20%');
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_utr_exp()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '30%' AND (datediff(masaberlaku, current_date())) < 0 AND status = 'expired' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		//return $this->db->query($SQL)->num_rows();
		$this->db->where('kd_ptera LIKE','30%');
		$this->db->where('status','expired');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_utr_act()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '30%' AND (datediff(masaberlaku, current_date())) > 0 AND status = 'selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where('kd_ptera LIKE','30%');
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_teng_exp()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '40%' AND (datediff(masaberlaku, current_date())) < 0 AND status = 'expired' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		//return $this->db->query($SQL)->num_rows();
		$this->db->where('kd_ptera LIKE','40%');
		$this->db->where('status','expired');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_teng_act()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '40%' AND (datediff(masaberlaku, current_date())) > 0 AND status = 'selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where('kd_ptera LIKE','40%');
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_bar_exp()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '50%' AND (datediff(masaberlaku, current_date())) < 0 AND status = 'expired' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		//return $this->db->query($SQL)->num_rows();
		$this->db->where('kd_ptera LIKE','50%');
		$this->db->where('status','expired');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_bar_act()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '50%' AND (datediff(masaberlaku, current_date())) > 0 AND status = 'selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where('kd_ptera LIKE','50%');
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function tnh_sar_exp()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '60%' AND (datediff(masaberlaku, current_date())) < 0 AND status = 'expired' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		//return $this->db->query($SQL)->num_rows();
		$this->db->where("kd_ptera LIKE '60%'");
		$this->db->where('status','expired');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function tnh_sar_act()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '60%' AND (datediff(masaberlaku, current_date())) > 0 AND status = 'selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '60%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}


	public function bog_sel_data()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '10%' AND status='selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '10%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_tim_data()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '20%' AND status='selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '20%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_utr_data()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '30%' AND status='selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '30%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_teng_data()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '40%' AND status='selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '40%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function bog_bar_data()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '50%' AND status='selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '50%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}
	public function tnh_sar_data()
	{
		$next_year = date('Y')+1;
		$curr_year = date('Y');
		//$SQL = "SELECT * FROM t_ptera WHERE kd_ptera LIKE '60%' AND status='selesai' AND masaberlaku BETWEEN '".$curr_year."-00-00' AND '".$next_year."-12-31'";
		$this->db->where("kd_ptera LIKE '60%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera')->num_rows();
	}


	public function data_tera_baru_online()
	{
		$this->db->join('t_pengguna_online','t_ptera_online.kd_pengguna = t_pengguna_online.kd_pengguna');
		$this->db->where('t_ptera_online.status','not_confirmed');
		$this->db->where('status2','baru');
		return $this->db->get('t_ptera_online');
	}
	public function data_tera_ulang_online()
	{
		$this->db->join('t_pengguna_online','t_ptera_online.kd_pengguna = t_pengguna_online.kd_pengguna');
		$this->db->where('t_ptera_online.status','not_confirmed');
		$this->db->where('status2','ulang');
		return $this->db->get('t_ptera_online');
	}
	public function set_status($no_trans)
	{
		$this->db->set('status','confirmed');
		$this->db->where('no_trans',$no_trans);
		$this->db->update('t_ptera_online');
	}
	public function input_kategori($data)
	{
		$this->db->insert('t_kategori',$data);
	}
	public function get_urut_subkategori($kd_kategori)
	{
		$this->db->where('kd_kategori',$kd_kategori);
		$query = $this->db->get('t_kategori_sub');
		return $query->num_rows()+1;
	}
	public function sub_kategori_tambah($data)
	{
		$this->db->insert('t_kategori_sub',$data);
	}
	public function edit_subkategori($kd_subkategori,$data)
	{
		$this->db->where('kd_subkategori',$kd_subkategori);
		$this->db->set($data);
		$this->db->update('t_kategori_sub');
	}
	public function update_kd_subkategori($kd_subkategori,$kd_subkategori2)
	{
		$this->db->set('kd_subkategori',$kd_subkategori2);
		$this->db->where('kd_subkategori',$kd_subkategori);
		$this->db->update('t_kategori_sub');
	}
	public function delete_subkategori($id)
	{
		$this->db->where('kd_subkategori',$id);
		$this->db->delete('t_kategori_sub');
	}
	public function tambah_jenis($data)
	{
		$this->db->insert('t_jenis',$data);
	}
	public function delete_jenis($id)
	{
		$this->db->where('kd_jenis',$id);
		$this->db->delete('t_jenis');
	}
	public function getJenisById($id)
	{
		$this->db->where('kd_jenis',$id);
		return $this->db->get('t_jenis');
	}
	public function edit_jenis($kd_jenis,$data)
	{
		$this->db->where('kd_jenis',$kd_jenis);
		$this->db->set($data);
		$this->db->update('t_jenis');
	}
	public function get_subjenis()
	{
		return $this->db->get('t_subjenis');
	}
	public function get_subjenis_by_kd_subjenis($kd_subjenis)
	{
		$this->db->join('t_jenis','t_jenis.kd_jenis = t_subjenis.kd_jenis');
		$this->db->where('kd_subjenis',$kd_subjenis);
		return $this->db->get('t_subjenis');
	}
	public function get_urut_subjenis($kd_jenis)
	{
		$this->db->where('kd_jenis',$kd_jenis);
		$query = $this->db->get('t_subjenis');
		return $query->num_rows()+1;
	}
	public function tambah_subjenis($data)
	{
		$this->db->insert('t_subjenis',$data);
	}
	public function subjenis_edit($kd_subjenis,$data)
	{
		$this->db->where('kd_subjenis',$kd_subjenis);
		$this->db->set($data);
		$this->db->update('t_subjenis');
	}
	public function update_kd_subjenis($kd_subjenis,$kd_subjenis2)
	{
		$this->db->where('kd_subjenis',$kd_subjenis);
		$this->db->set('kd_subjenis',$kd_subjenis2);
		$this->db->update('t_subjenis');
	}
	public function delete_subjenis($kd_subjenis)
	{
		$this->db->where('kd_subjenis',$kd_subjenis);
		$this->db->delete('t_subjenis');
	}
	public function get_subsubjenis()
	{
		return $this->db->get('t_subsubjenis');
	}
	public function get_urut_subsubjenis($kd_subjenis)
	{
		$this->db->where('kd_subjenis',$kd_subjenis);
		$query = $this->db->get('t_subsubjenis');
		return $query->num_rows()+1;
	}
	public function tambah_subsubjenis($data)
	{
		$this->db->insert('t_subsubjenis',$data);
	}
	public function delete_subsubjenis($kd_subsubjenis)
	{
		$this->db->where('kd_subsubjenis',$kd_subsubjenis);
		$this->db->delete('t_subsubjenis');
	}
	public function get_subsubjenis_by_kd_subsubjenis($kd_subsubjenis)
	{
		$this->db->join('t_subjenis','t_subsubjenis.kd_subjenis = t_subsubjenis.kd_subjenis');
		$this->db->where('kd_subsubjenis',$kd_subsubjenis);
		return $this->db->get('t_subsubjenis');
	}
	public function subsubjenis_edit($kd_subsubjenis,$data)
	{
		$this->db->set($data);
		$this->db->where('kd_subsubjenis',$kd_subsubjenis);
		$this->db->update('t_subsubjenis');
	}
	public function update_kd_subsubjenis($kd_subsubjenis,$kd_subsubjenis2)
	{
		$this->db->set('kd_subsubjenis',$kd_subsubjenis2);
		$this->db->where('kd_subsubjenis',$kd_subsubjenis);
		$this->db->update('t_subsubjenis');
	}
	public function delete_pengguna($kd_pengguna)
	{
		$this->db->where('kd_pengguna',$kd_pengguna);
		$this->db->delete('t_pengguna');
	}
	public function delete_ptera($kd_pengguna)
	{
		$this->db->where('kd_pengguna',$kd_pengguna);
		$this->db->delete('t_ptera');
	}
	public function delete_ptera_by_no_trans($no_trans)
	{
		$this->db->where('no_trans',$no_trans);
		$this->db->delete('t_ptera');
	}
	public function update_status_lama($no_reg_lama)
	{
		$this->db->set('status','expired');
		$this->db->where('no_reg',$no_reg_lama);
		$this->db->update('t_ptera');
	}
	public function report_harian()
	{
		$this->db->join('t_subjenis','t_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_pengguna','t_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->where('tgl_bayar',date('Ymd'));
		return $this->db->get('t_ptera');
	}
	public function report_daftar_harian()
	{
		$this->db->join('t_subjenis','t_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_pengguna','t_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->where('tgl_daftar',date('Ymd'));
		return $this->db->get('t_ptera');
	}
	public function report_bulanan()
	{
		$this->db->join('t_subjenis','t_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_pengguna','t_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->where("tgl_bayar LIKE '".date('Y')."_".date('m')."%'");
		$this->db->where('status','selesai');
		return $this->db->get('t_ptera');
	}
	public function update_expired()
	{
		$this->db->where("datediff(masaberlaku, current_date()) < 0");
		$this->db->set('status','expired');
		$this->db->update('t_ptera');
	}
	
	function getUsaha()
	{
		return $this->db->get('t_kategori');
	}
	
	function getUttpKategori($kategori)
	{
		$this->db->where('kd_kategori',$kategori);
		$this->db->join('t_pengguna', 't_ptera.kd_pengguna = t_pengguna.kd_pengguna');
		$this->db->join('t_jenis', 't_ptera.kd_jenis = t_jenis.kd_jenis');
		$this->db->join('t_subjenis', 't_ptera.kd_subjenis = t_subjenis.kd_subjenis');
		$this->db->join('t_subsubjenis', 't_ptera.kd_subsubjenis = t_subsubjenis.kd_subsubjenis');
		return $this->db->get('t_ptera');
	}
}
