<?php
/**
* 
*/
class Penguji extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['uttp'] = $this->Data_model->getDataUjiBaru();
			$this->load->view('backend/penguji',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}

	public function konfirmasi_uji($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data = array(
					'kd_ptera'=>$this->input->post('kd_ptera'),
					'terakhir_tera'=>$this->input->post('tgl_tera'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'hasil_uji'=>$this->input->post('hasil_uji'),
					'keterangan'=>$this->input->post('keterangan')
				);
			$this->Data_model->konfirmasi_uji($no_reg,$data);
			$this->session->set_flashdata('confirm','Data berhasil dikonfirmasi');
			redirect(base_url('penguji/preview_bukti_tera/'.$kd_pengguna.'/'.$no_reg));
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cetak_bukti_tera()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['bukti_tera'] = $this->Data_model->cetak_bukti($this->input->post('kd_pengguna'),$this->input->post('no_reg'));
				if ($data['bukti_tera']->num_rows()>0) {
					$this->load->view('backend/cetak_bukti_tera2',$data);
				} else {
					$this->session->set_flashdata('kosong','Data tidak ditemukan');
					redirect(base_url('penguji/cetak_bukti_tera'));
				}
				
			} else {
				$this->load->view('backend/cetak_bukti_tera');
			}
			
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function preview_bukti_tera($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['bukti_tera'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
			$this->load->view('backend/cetak_bukti_tera2',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cetak_bukti_tera2($kd_pengguna,$no_reg)
	{
		$data['bukti_tera'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/cetak_bukti_tera_pdf',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/cetak_bukti_tera_pdf",array('Attachment'=>0),$data);
	}
}