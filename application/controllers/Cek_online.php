<?php
/**
* 
*/
class Cek_online extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['data'] = $this->Data_model->data_tera_baru_online();
			$this->load->view('backend/tera_online',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function view_daftar_online($no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['pengguna'] = $this->Data_model->getPtera_online($no_reg);
			$this->load->view('backend/view_daftar_online',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function acc()
	{
		if ($this->session->userdata('status')=='login') {
				$no_urut = $this->Data_model->getNo_urut($this->input->post('id_kecamatan'));
				$old_kd_pengguna;
				$kd_pengguna = $this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori');
				$data_pegguna = array(
						'kd_pengguna'=>$kd_pengguna,
						'id_kecamatan'=>$this->input->post('id_kecamatan'),
						'kd_kategori'=>$this->input->post('kd_kategori'),
						'kd_subkategori'=>$this->input->post('kd_subkategori'),
						'nm_pengguna'=>$this->input->post('nm_pengguna'),
						'nm_usaha'=>$this->input->post('nm_usaha'),
						'alamat'=>$this->input->post('alamat'),
						'telp'=>$this->input->post('telp'),
						'hp'=>$this->input->post('hp'),
						'email'=>$this->input->post('email')
					);

				$data_ptera = array(
						'kd_ptera'=>$this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori').$this->input->post('kd_jenis').$this->input->post('kd_subjenis').$this->input->post('kd_subsubjenis'),
						'no_trans'=>'',
						'kd_pengguna'=>$this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori'),
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>date('Ymd'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>'',
						'status2'=>'baru',
						'status'=>'proses'
					);
				$this->Data_model->tera_baru($data_pegguna,$data_ptera);
				$last_rec = $this->Data_model->getLastRec();
				$no_trans = $this->Data_model->getLastRec();
				$no_reg = "REG-".$last_rec;
				$this->Data_model->update_no_reg($no_reg,$no_trans);
				$this->Data_model->set_status($this->input->post('no_trans'));
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url("backend/bukti_daftar/".$kd_pengguna."/".$no_reg));
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cek_tera_ulang()
	{
		if ($this->session->userdata('status')=='login') {
			$data['data'] = $this->Data_model->data_tera_ulang_online();
			$this->load->view('backend/tera_ulang_online',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function view_tera_ulang_online($no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['pengguna'] = $this->Data_model->getPtera_online($no_reg);
			$this->load->view('backend/view_tera_ulang_online',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function acc_tera_ulang()
	{
		if ($this->session->userdata('status')=='login') {
			$kd_pengguna =  $this->input->post('kd_pengguna');
			$kd_ptera = $this->input->post('kd_ptera');
			$data_ptera = array(
						'kd_ptera'=>$kd_ptera,
						'no_trans'=>'',
						'kd_pengguna'=>$kd_pengguna,
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>date('Ymd'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>'',
						'status2'=>'ulang',
						'status'=>'proses'
					);
				$this->Data_model->tera_baru2($data_ptera);
				$last_rec = $this->Data_model->getLastRec();
				$no_trans = $this->Data_model->getLastRec();
				$no_reg = "REG-".$last_rec;
				$this->Data_model->update_no_reg($no_reg,$no_trans);
				$this->Data_model->set_status($this->input->post('no_trans'));
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url("backend/bukti_daftar/".$kd_pengguna."/".$no_reg));
		} else {
			redirect(base_url('front'));
		}
		
	}
}