<?php
/**
 *
 */
class Login extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('Data_model');
  }
  public function index()
  {
    if ($this->session->userdata('status')=='login') {
      redirect(base_url('backend'));
    } else {
      if (isset($_POST['btnSubmit'])) {
        $data = array(
          'username'=>$this->input->post('username'),
          'password'=>md5($this->input->post('password'))
        );
        $query = $this->Data_model->getUserInfo($data);
        if ($query->num_rows() == 1) {
          foreach ($query->result_array() as $key => $value) {
            $newdata = array(
              'id_user'=>$value['id_user'],
              'nama'=>$value['nama'],
              'level'=>$value['level'],
              'status'=>'login'
            );
          } 
          $this->session->set_userdata($newdata);
          redirect(base_url('backend'));
        } else {
          redirect(base_url('front'));
        }
      } else {
        $data['instansi'] = $this->Data_model->instansi();
        $this->load->view('frontend/landing',$data);
      }
    }
  }

  public function logout()
  {
    if ($this->session->userdata('status')=='login') {
      $this->session->sess_destroy();
      redirect(base_url('front'));
    } else {
      redirect(base_url('front'));
    }

  }
}
