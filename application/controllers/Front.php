<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}

	function index()
	{
		$this->load->view('frontend/landing');
	}
	function profile()
	{
		$this->load->view('frontend/profile');
	}
	function berita()
	{
		$this->load->view('frontend/berita');
	}
	function read_more()
	{
		$this->load->view('frontend/read_more');
	}
	function regulasi()
	{
		$this->load->view('frontend/regulasi');
	}

	function pendaftaran()
	{
		if (isset($_POST['btnSubmit'])) {
			$data['kecamatan'] = $this->Data_model->getKecamatan();
			$data['kategori'] = $this->Data_model->getKategori();
			$data['jenis'] = $this->Data_model->getJenis();
			$data['pengguna'] = $this->Data_model->getPenggunaByKd_penggunaOnline($this->input->post('kd_pengguna'));
			$data['kd_pengguna'] = $this->input->post('kd_pengguna');
			if ($data['pengguna']->num_rows() > 0) {
				$this->load->view('frontend/daftar_online2',$data);
			} else {
				$this->session->set_flashdata('kosong','Kode Pengguna tidak ditemukan');
				redirect(base_url('front/pendaftaran'));
			}
		} else {
			$data['kecamatan'] = $this->Data_model->getKecamatan();
			$data['kategori'] = $this->Data_model->getKategori();
			$data['jenis'] = $this->Data_model->getJenis();
			$this->load->view('frontend/pre_daftar_online',$data);
		}

	}
	public function pendaftaran2()
	{
		$no_urut = $this->Data_model->getNo_urut_online($this->input->post('id_kecamatan'));
		$kd_pengguna = $this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori');
		$data_pegguna = array(
						'kd_pengguna'=>$kd_pengguna,
						'id_kecamatan'=>$this->input->post('id_kecamatan'),
						'kd_kategori'=>$this->input->post('kd_kategori'),
						'kd_subkategori'=>$this->input->post('kd_subkategori'),
						'nm_pengguna'=>$this->input->post('nm_pengguna'),
						'nm_usaha'=>$this->input->post('nm_usaha'),
						'alamat'=>$this->input->post('alamat'),
						'rt'=>$this->input->post('rt'),
						'rw'=>$this->input->post('rw'),
						'telp'=>$this->input->post('telp'),
						'hp'=>$this->input->post('hp'),
						'email'=>$this->input->post('email'),
						'status'=>'not_confirmed'
		);

				$data_ptera = array(
						'kd_ptera'=>$this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori').$this->input->post('kd_jenis').$this->input->post('kd_subjenis').$this->input->post('kd_subsubjenis'),
						'no_trans'=>'',
						'kd_pengguna'=>$this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori'),
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>date('Ymd'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>'',
						'status2'=>'baru',
						'status'=>'not_confirmed'
					);
				$this->Data_model->tera_baru_online($data_pegguna,$data_ptera);
				$last_rec = $this->Data_model->getLastRecOnline();
				$no_trans = $this->Data_model->getLastRecOnline();
				$no_reg = "REG-".$last_rec;
				$this->Data_model->update_no_reg_online($no_reg,$no_trans);
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url('front/bukti_pendaftaran_online/'.$kd_pengguna.'/'.$no_reg));
	}
	public function pendaftaran3()
	{
		$kd_pengguna =  $this->input->post('kd_pengguna');
				    $data_ptera = array(
						'kd_ptera'=>$kd_pengguna.$this->input->post('kd_jenis').$this->input->post('kd_subjenis').$this->input->post('kd_subsubjenis'),
						'no_trans'=>'',
						'kd_pengguna'=>$kd_pengguna,
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>date('Ymd'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>'',
						'status2'=>'baru',
						'status'=>'not_confirmed'
					);
				$this->Data_model->tera_baru_online2($data_ptera);
				$last_rec = $this->Data_model->getLastRecOnline();
				$no_trans = $this->Data_model->getLastRecOnline();
				$no_reg = "REG-".$last_rec;
				$this->Data_model->update_no_reg_online($no_reg,$no_trans);
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url('front/bukti_pendaftaran_online/'.$kd_pengguna.'/'.$no_reg));

	}
	public function pendaftaran_online()
	 {
	 	$data['kecamatan'] = $this->Data_model->getKecamatan();
		$data['kategori'] = $this->Data_model->getKategori();
		$data['jenis'] = $this->Data_model->getJenis();
		$this->load->view('frontend/daftar_online',$data);
	 }

	public function bukti_pendaftaran_online($kd_pengguna,$no_reg)
	 {
	 	$data['bukti_daftar'] = $this->Data_model->cetak_bukti_online($kd_pengguna,$no_reg);
	 	$this->load->view('frontend/bukti_daftar_online',$data);
	 }

	 public function tera_ulang_online()
	 {
	  	$this->load->view('frontend/tera_ulang_online');
	 }
	public function tera_ulang_online_ac()
	{
		$data['kd_ptera'] = $kd_ptera = $this->input->post('kd_ptera');
		$data['kd_pengguna'] = $kd_pengguna = $this->input->post('kd_pengguna');
		$data['jenis'] = $this->Data_model->getJenis();
		$data['pengguna'] = $this->Data_model->getPengguna2($kd_pengguna);
		$data['tera'] = $this->Data_model->getPtera2($kd_ptera);
		if ($data['tera']->num_rows() && $data['pengguna']->num_rows() >0) {
			$this->load->view('frontend/tera_ulang_online2',$data);
		} else {
			$this->session->set_flashdata('kosong','Data tidak ditemukan');
			redirect(base_url('front/tera_ulang_online'));
		}
	}

	public function tera_ulang_online2()
	{
		$no_urut = $this->Data_model->getNo_urut_online($this->input->post('id_kecamatan'));
		$kd_pengguna = $this->input->post('kd_pengguna');
		$kd_ptera = $this->input->post('kd_ptera');
		$data_pegguna = array(
						'kd_pengguna'=>$kd_pengguna,
						'id_kecamatan'=>$this->input->post('id_kecamatan'),
						'kd_kategori'=>$this->input->post('kd_kategori'),
						'kd_subkategori'=>$this->input->post('kd_subkategori'),
						'nm_pengguna'=>$this->input->post('nm_pengguna'),
						'nm_usaha'=>$this->input->post('nm_usaha'),
						'alamat'=>$this->input->post('alamat'),
						'rt'=>$this->input->post('rt'),
						'rw'=>$this->input->post('rw'),
						'telp'=>$this->input->post('telp'),
						'hp'=>$this->input->post('hp'),
						'email'=>$this->input->post('email'),
						'status'=>'not_confirmed'
		);
		$data_ptera = array(
						'kd_ptera'=>$kd_ptera,
						'no_trans'=>'',
						'kd_pengguna'=>$kd_pengguna,
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>date('Ymd'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>'',
						'status2'=>'ulang',
						'status'=>'not_confirmed'
					);
		$cek_t_pengguna_online = $this->Data_model->cek_t_pengguna_online($this->input->post('kd_pengguna'));
		if ($cek_t_pengguna_online->num_rows()>0) {
			$this->Data_model->tera_baru_online2($data_ptera);
		} else {
			$this->Data_model->tera_baru_online($data_pegguna,$data_ptera);
		}
		$last_rec = $this->Data_model->getLastRecOnline();
		$no_trans = $this->Data_model->getLastRecOnline();
		$no_reg = "REG-".$last_rec;
		$this->Data_model->update_no_reg_online($no_reg,$no_trans);
		$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
		redirect(base_url('front/bukti_pendaftaran_online/'.$kd_pengguna.'/'.$no_reg));
	}
}
