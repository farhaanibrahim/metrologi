<?php
/**
* 
*/
class Penera extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['uttp'] = $this->Data_model->getDataTera();
			$this->load->view('backend/penera',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function konfirmasi_tera($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata()) {
			$data = array(
					'tgl_bubuh'=>$this->input->post('tgl_bubuh'),
					'cap_tera'=>$this->input->post('cap_tera'),
					'sertifikat'=>$this->input->post('sertifikat'),
					'stiker'=>$this->input->post('stiker'),
					'kondisi'=>$this->input->post('kondisi'),
					'tindakan'=>$this->input->post('tindakan')
				);
			$this->Data_model->konfirmasi_tera($no_reg,$data);
			$this->session->set_flashdata('confirm','Data berhasil dikonfirmasi');
			redirect(base_url('penera/preview_pembubuhan/'.$kd_pengguna.'/'.$no_reg));
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function kartu_pemilik()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['kartu'] = $this->Data_model->cetak_bukti($this->input->post('kd_pengguna'),$this->input->post('no_reg'));
				if ($data['kartu']->num_rows()>0) {
					$this->load->view('backend/cetak_kartu_pemilik',$data);
				} else {
					$this->session->set_flashdata('kosong','Data tidak ditemukan');
					redirect(base_url('penera/kartu_pemilik'));
				}
			} else {
				$this->load->view('backend/input_kartu_pemilik');
			}
			
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cetak_kartu_pemilik($kd_pengguna,$no_reg)
	{
		$data['kartu'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/cetak_kartu_pemilik2',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/cetak_kartu_pemilik2",array('Attachment'=>0),$data);
	}
	public function cetak_bukti_pembubuhan()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['bukti_bubuh'] = $this->Data_model->cetak_bukti($this->input->post('kd_pengguna'),$this->input->post('no_reg'));
				if ($data['bukti_bubuh']->num_rows()>0) {
					$this->load->view('backend/cetak_bukti_pembubuhan2',$data);
				} else {
					$this->session->set_flashdata('kosong','Data tidak ditemukan');
					redirect(base_url('penera/cetak_bukti_pembubuhan'));
				}
				
			} else {
				$this->load->view('backend/cetak_bukti_pembubuhan');
			}
			
		} else {
			redirect(base_url('front'));
		}
	}
	public function preview_pembubuhan($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['bukti_bubuh'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
			$this->load->view('backend/cetak_bukti_pembubuhan2',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cetak_bukti_pembubuhan_pdf($kd_pengguna,$no_reg)
	{
		$data['bukti_bubuh'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/cetak_bukti_pembubuhan_pdf',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/cetak_bukti_pembubuhan_pdf",array('Attachment'=>0),$data);
	}
}