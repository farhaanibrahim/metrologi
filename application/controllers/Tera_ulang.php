<?php
/**
* 
*/
class Tera_ulang extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$this->load->view('backend/tera_ulang');
		} else {
			redirect(baseUrl('front'));
		}
		
	}
	public function tera_ulang_ac()
	{
		if ($this->session->userdata('status')=='login') {
			$data['kd_ptera'] = $kd_ptera = $this->input->post('kd_ptera');
			$data['kd_pengguna'] = $kd_pengguna = $this->input->post('kd_pengguna');
			$data['jenis'] = $this->Data_model->getJenis();
			$data['pengguna'] = $this->Data_model->getPengguna2($kd_pengguna);
			$data['tera'] = $this->Data_model->getPtera2($kd_ptera);
			$this->load->view('backend/tera_ulang2',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function tera_ulang_ac2()
	{
		if ($this->session->userdata('status')=='login') {
			$kd_pengguna =  $this->input->post('kd_pengguna');
			$kd_ptera = $this->input->post('kd_ptera');
			$no_reg_lama = $this->input->post('no_reg_lama');
			$data_ptera = array(
						'kd_ptera'=>$kd_ptera,
						'no_trans'=>'',
						'kd_pengguna'=>$kd_pengguna,
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>date('Ymd'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>'',
						'status2'=>'ulang',
						'status'=>'proses'
					);
				$this->Data_model->tera_baru2($data_ptera);
				$last_rec = $this->Data_model->getLastRec();
				$no_trans = $this->Data_model->getLastRec();
				$no_reg = "REG-".$last_rec;
				$this->Data_model->update_no_reg($no_reg,$no_trans);
				$this->Data_model->update_status_lama($no_reg_lama);
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url("backend/bukti_daftar/".$kd_pengguna."/".$no_reg));
		} else {
			redirect(baseUrl('front'));
		}
		
	}
	public function uji()
	{
		if ($this->session->userdata('status')=='login') {
			$data['uttp'] = $this->Data_model->getDataUjiUlang();
			$this->load->view('backend/penguji_tera_ulang',$data);
		} else {
			redirect(baseUrl('front'));
		}
		
	}
	public function konfirmasi_uji($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data = array(
					'terakhir_tera'=>date('Ymd'),
					'masaberlaku'=>$this->input->post('masaberlaku'),
					'hasil_uji'=>$this->input->post('hasil_uji'),
					'keterangan'=>$this->input->post('keterangan')
				);
			$this->Data_model->konfirmasi_uji($no_reg,$data);
			$this->session->set_flashdata('confirm','Data berhasil dikonfirmasi');
			redirect(base_url('penguji/preview_bukti_tera/'.$kd_pengguna.'/'.$no_reg));
		} else {
			redirect(base_url('front'));
		}
		
	}
}