<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');

		$this->Data_model->update_expired();
	}

	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['kecamatan'] = $this->Data_model->getKecamatan();
			$data['jml_bog_sel_exp'] = $bog_sel_exp = $this->Data_model->bog_sel_exp();
			$data['jml_bog_tim_exp'] = $bog_tim_exp = $this->Data_model->bog_tim_exp();
			$data['jml_bog_utr_exp'] = $bog_utr_exp = $this->Data_model->bog_utr_exp();
			$data['jml_bog_teng_exp'] = $bog_teng_exp = $this->Data_model->bog_teng_exp();
			$data['jml_bog_bar_exp'] = $bog_bar_exp = $this->Data_model->bog_bar_exp();
			$data['jml_tnh_sar_exp'] = $tnh_sar_exp = $this->Data_model->tnh_sar_exp();
			
			$jml_uttp_exp = $bog_sel_exp+$bog_tim_exp+$bog_utr_exp+$bog_teng_exp+$bog_bar_exp+$tnh_sar_exp;
			$data['bog_sel_exp'] = ($jml_uttp_exp > 0 ? ($bog_sel_exp/$jml_uttp_exp)*100 : 0);
			$data['bog_tim_exp'] = ($jml_uttp_exp > 0 ? ($bog_tim_exp/$jml_uttp_exp)*100 : 0);
			$data['bog_utr_exp'] = ($jml_uttp_exp > 0 ? ($bog_utr_exp/$jml_uttp_exp)*100 : 0);
			$data['bog_teng_exp'] = ($jml_uttp_exp > 0 ? ($bog_teng_exp/$jml_uttp_exp)*100 : 0);
			$data['bog_bar_exp'] = ($jml_uttp_exp > 0 ? ($bog_bar_exp/$jml_uttp_exp)*100 : 0);
			$data['tnh_sar_exp'] = ($jml_uttp_exp > 0 ? ($tnh_sar_exp/$jml_uttp_exp)*100 : 0);
			
			$data['bog_sel_data'] = $this->Data_model->bog_sel_data();
			$data['bog_tim_data'] = $this->Data_model->bog_tim_data();
			$data['bog_utr_data'] = $this->Data_model->bog_utr_data();
			$data['bog_teng_data'] = $this->Data_model->bog_teng_data();
			$data['bog_bar_data'] = $this->Data_model->bog_bar_data();
			$data['tnh_sar_data'] = $this->Data_model->tnh_sar_data();
			$this->load->view('backend/dashboard',$data);
		} else {
			redirect(base_url('front'));
		}

	}

	public function pengaturan_kategori()
	{
		if ($this->session->userdata('status')=='login') {
			$data['kategori'] = $this->Data_model->getKategori();
			$this->load->view('backend/pengaturan_kategori',$data);
		} else {
			redirect(base_url('front'));
		}

	}

	public function pengaturan_kategori_tambah()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data = array(
						'kd_kategori'=>'',
						'nm_kategori'=>$this->input->post('nm_kategori')
						);

				$this->Data_model->input_kategori($data);
				$this->session->set_flashdata('input_kategori','Data berhasil dimasukkan');
				redirect(base_url('backend/pengaturan_kategori'));
			} else {
				$this->load->view('backend/pengaturan_kategori_tambah');
			}

		} else {
			redirect(base_url('front'));
		}

	}

	public function pengaturan_kategori_edit($id)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$id = $this->input->post('kd_kategori');
				$data = array('nm_kategori'=>$this->input->post('nm_kategori'));
				$this->Data_model->kategori_edit($id,$data);
				$this->session->set_flashdata('edit_kategori','Data berhasil diupdate');
				redirect(base_url('backend/pengaturan_kategori'));
			} else {
				$data['id'] = $id;
				$data['kategori'] = $this->Data_model->getKategori_ById($id);
				$this->load->view('backend/kategori_edit',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_kategori_hapus($id)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->kategori_delete($id);
			$this->session->set_flashdata('delete_kategori','Kategori Sudah dihapus');
			redirect(base_url('backend/pengaturan_kategori'));
		} else {
			redirect(base_url('front'));
		}

	}

	public function pengaturan_sub_kategori()
	{
		if ($this->session->userdata('status')=='login') {
			$data['sub_kategori'] = $this->Data_model->subKategori();
			$this->load->view('backend/pengaturan_sub_kategori',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subkategori_tambah()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$no_urut = $this->Data_model->get_urut_subkategori($this->input->post('kd_kategori'));
				$data = array(
						'kd_subkategori'=>$this->input->post('kd_kategori').$no_urut,
						'kd_kategori'=>$this->input->post('kd_kategori'),
						'nm_subkategori'=>$this->input->post('nm_subkategori')
					);
				$this->Data_model->sub_kategori_tambah($data);
				$this->session->set_flashdata('input_subkategori','Berhasil menambah Subkategori');
				redirect(base_url('backend/pengaturan_sub_kategori'));
			} else {
				$data['kategori'] = $this->Data_model->getKategori();
				$this->load->view('backend/sub_kategori_tambah',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subkategori_edit($id)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$kd_subkategori = $this->input->post('kd_subkategori');
				$data = array(
						'kd_kategori'=>$this->input->post('kd_kategori'),
						'nm_subkategori'=>$this->input->post('nm_subkategori')
					);
				$this->Data_model->edit_subkategori($kd_subkategori,$data);
				$this->session->set_flashdata('edit_subkategori','Sub kategori berhasil diubah');
				redirect(base_url('backend/pengaturan_sub_kategori'));
			} else {
				$data['kategori'] = $this->Data_model->getKategori();
				$data['sub_kategori'] = $this->Data_model->getSubKategori2($id);
			}

			$this->load->view('backend/sub_kategori_edit',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subkategori_hapus($id)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->delete_subkategori($id);
			$this->session->set_flashdata('delete_subkategori','Sub Kategori telah dihapus');
			redirect(base_url('backend/pengaturan_sub_kategori'));
		} else {
			redirect(base_url('front'));
		}

	}

	public function pengaturan_jenis()
	{
		if ($this->session->userdata('status')=='login') {
			$data['jenis'] = $this->Data_model->getJenis();
			$this->load->view('backend/pengaturan_jenis',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_jenis_tambah()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data = array(
					'kd_jenis'=>'',
					'nm_jenis'=>$this->input->post('nm_jenis'));
				$this->Data_model->tambah_jenis($data);
				$this->session->set_flashdata('tambah_jenis','Berhasil menambah jenis');
				redirect(base_url('backend/pengaturan_jenis'));
			} else {
				$this->load->view('backend/jenis_tambah');
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_jenis_edit($id)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$kd_jenis = $id;
				$data = array(
						'nm_jenis'=>$this->input->post('nm_jenis')
					);
				$this->Data_model->edit_jenis($kd_jenis,$data);
				$this->session->set_flashdata('edit_jenis','Data berhasil dirubah');
				redirect(base_url('backend/pengaturan_jenis'));
			} else {
				$data['jenis'] = $this->Data_model->getJenisById($id);
				$this->load->view('backend/jenis_edit',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_jenis_hapus($id)
	{
		$this->Data_model->delete_jenis($id);
		$this->session->set_flashdata('delete_jenis','Jenis telah dihapus');
		redirect(base_url('backend/pengaturan_jenis'));
	}

	public function data_pengguna()
	{
		if ($this->session->userdata('status')=='login') {
			$data['pengguna'] = $this->Data_model->getPengguna();
			$this->load->view('backend/olah_pengguna',$data);
		} else {
			redirect(base_url('front'));
		}

	}

	public function data_pengguna_edit($kd_pengguna)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				/* $data = array(
						'kd_pengguna'
						'id_kecamatan'
						'kd_kategori'
						''
					); */
			} else {
				$data['pengguna'] = $this->Data_model->getPenggunaByKd_pengguna($kd_pengguna);
				$this->load->view('backend/olah_pengguna_edit',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function data_pengguna_hapus($kd_pengguna)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->delete_pengguna($kd_pengguna);
			$this->Data_model->delete_ptera($kd_pengguna);
			$this->session->set_flashdata('delete_pengguna','Data telah dihapus');
			redirect(base_url('backend/data_pengguna'));
		} else {
			redirect(base_url('front'));
		}

	}

	public function data_ptera()
	{
		if ($this->session->userdata('status')=='login') {
			$data['tera'] = $this->Data_model->getPtera();
			$this->load->view('backend/olah_tera',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function data_tera_edit($no_trans)
	{
		if ($this->session->userdata('status')=='login') {
			$data['tera'] = $this->Data_model->getPteraByNoTrans($no_trans);
			$this->load->view('backend/olah_tera_edit',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function data_tera_edit2($no_trans)
	{
		if ($this->session->userdata('status')=='login') {
			$data['tera'] = $this->Data_model->getPteraByNoTrans($no_trans);
			$this->load->view('backend/olah_tera_edit2',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function data_tera_hapus($no_trans)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->delete_ptera_by_no_trans($no_trans);
			$this->session->set_flashdata('delete_ptera','Data telah dihapus');
			redirect(base_url('backend/data_ptera'));
		} else {
			redirect(base_url('front'));
		}

	}

	function create_barcode($kode){
		//kita load library nya ini membaca file Zend.php yang berisi loader
		//untuk file yang ada pada folder Zend
		$this->load->library('zend');

		//load yang ada di folder Zend
		$this->zend->load('Zend/Barcode');

		//generate barcodenya
		//$kode = 12345abc;
		Zend_Barcode::render('code128', 'image', array('text'=>$kode), array());
	}

	public function pengaturan_user()
	{
		if ($this->session->userdata('status')=='login') {
			$data['list_user'] = $this->Data_model->getListUser();
			$this->load->view('backend/pengaturan_user',$data);
		} else {
			redirect(base_url('login'));
		}

	}
	public function tambah_user()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data = array(
						'id_user'=>'',
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('password')),
						'nama'=>$this->input->post('nama'),
						'nip'=>$this->input->post('nip'),
						'level'=>$this->input->post('level')
					);
				$this->Data_model->tambah_user($data);
				$this->session->set_flashdata('tbh_usr_sukses','Berhasil Menambah User');
				redirect(base_url('backend/tambah_user'));
			} else {
				$this->load->view('backend/tambah_user');
			}

		} else {
			redirect(base_url('login'));
		}

	}
	public function tera_baru()
	{
		if ($this->session->userdata('status')=='login') {
			$this->load->view('backend/pre_tera_baru');
		} else {
			redirect(base_url('login'));
		}
	}
	public function tera_baru2()
	{
		if ($this->session->userdata('status')=='login') {
			$data['kecamatan'] = $this->Data_model->getKecamatan();
			$data['kategori'] = $this->Data_model->getKategori();
			$data['jenis'] = $this->Data_model->getJenis();
			$data['pengguna'] = $this->Data_model->getPenggunaByKd_pengguna($this->input->post('kd_pengguna'));
			$data['kd_pengguna'] = $this->input->post('kd_pengguna');
			if ($data['pengguna']->num_rows() > 0) {
				$this->load->view('backend/tera_baru2',$data);
			} else {
				$this->session->set_flashdata('kosong','Kode Pengguna tidak ditemukan');
				redirect(base_url('backend/tera_baru'));
			}
		} else {
			redirect(base_url('login'));
		}

	}
	public function form_tera_baru()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$no_urut = $this->Data_model->getNo_urut($this->input->post('id_kecamatan'));
				$kd_pengguna = $this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori');
				$data_pegguna = array(
						'kd_pengguna'=>$kd_pengguna,
						'id_kecamatan'=>$this->input->post('id_kecamatan'),
						'kd_kategori'=>$this->input->post('kd_kategori'),
						'kd_subkategori'=>$this->input->post('kd_subkategori'),
						'nm_pengguna'=>$this->input->post('nm_pengguna'),
						'nm_usaha'=>$this->input->post('nm_usaha'),
						'alamat'=>$this->input->post('alamat'),
						'rt'=>$this->input->post('rt'),
						'rw'=>$this->input->post('rw'),
						'telp'=>$this->input->post('telp'),
						'hp'=>$this->input->post('hp'),
						'email'=>$this->input->post('email')
					);

				$data_ptera = array(
						'kd_ptera'=>$this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori').$this->input->post('kd_jenis').$this->input->post('kd_subjenis').$this->input->post('kd_subsubjenis'),
						'no_trans'=>'',
						'kd_pengguna'=>$this->input->post('id_kecamatan').$no_urut.$this->input->post('kd_kategori').$this->input->post('kd_subkategori'),
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>$this->input->post('tgl_daftar'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>0,
						'status2'=>'baru',
						'status'=>'proses'
					);
				$this->Data_model->tera_baru($data_pegguna,$data_ptera);
				$no_trans = $this->Data_model->getLastRec();
				$no_reg = "REG-".$no_trans;
				$this->Data_model->update_no_reg($no_reg,$no_trans);
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url("backend/bukti_daftar/".$kd_pengguna."/".$no_reg));
			} else {
				$data['kecamatan'] = $this->Data_model->getKecamatan();
				$data['kategori'] = $this->Data_model->getKategori();
				$data['jenis'] = $this->Data_model->getJenis();
				$this->load->view('backend/tera_baru',$data);
			}
		} else {
			redirect(base_url('front'));
		}

	}
	public function form_tera_baru2()
	{
		if ($this->session->userdata('status')=='login') {
					$kd_pengguna =  $this->input->post('kd_pengguna');
				   $data_ptera = array(
						'kd_ptera'=>$kd_pengguna.$this->input->post('kd_jenis').$this->input->post('kd_subjenis').$this->input->post('kd_subsubjenis'),
						'no_trans'=>'',
						'kd_pengguna'=>$kd_pengguna,
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'kd_subsubjenis'=>$this->input->post('kd_subsubjenis'),
						'merk'=>$this->input->post('merk'),
						'no_seri'=>$this->input->post('no_seri'),
						'kapasitas'=>$this->input->post('kapasitas'),
						'stok_liter'=>$this->input->post('stok_liter'),
						'tgl_daftar'=>$this->input->post('tgl_daftar'),
						'tgl_bubuh'=>'',
						'tgl_serah'=>'',
						'terakhir_tera'=>'',
						'masaberlaku'=>'',
						'tgl_bayar'=>'',
						'cap_tera'=>NULL,
						'sertifikat'=>NULL,
						'stiker'=>NULL,
						'kondisi'=>NULL,
						'tindakan'=>NULL,
						'hasil_uji'=>NULL,
						'keterangan'=>NULL,
						'jml_bayar'=>0,
						'status2'=>'baru',
						'status'=>'proses'
					);
				$this->Data_model->tera_baru2($data_ptera);
				$last_rec = $this->Data_model->getLastRec();
				$no_trans = $this->Data_model->getLastRec();
				$no_reg = "REG-".$no_trans;
				$this->Data_model->update_no_reg($no_reg,$no_trans);
				$this->session->set_flashdata('tambah_sukses','Data berhasil dimasukkan');
				redirect(base_url("backend/bukti_daftar/".$kd_pengguna."/".$no_reg));
		} else {
			redirect(base_url('front'));
		}

	}

	public function cetak_bukti_daftar()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['pengguna'] = $this->Data_model->cetak_bukti($this->input->post('kd_pengguna'),$this->input->post('no_reg'));
				$this->load->view('backend/cetak_bukti_daftar',$data);
			} else {
				$this->load->view('backend/input_cetak_bukti_daftar');
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function bukti_daftar($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['pengguna'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
			$this->load->view('backend/cetak_bukti_daftar',$data);
		}
	}
	public function cetak_bukti_daftar2($kd_pengguna,$no_reg)
	{
		$data['pendaftaran'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/cetak_bukti_daftar2',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/cetak_bukti_daftar2",array('Attachment'=>0),$data);
	}

	//AJAX
	public function sub_kategori()
	{
		if($this->Data_model->getSubKategori($this->input->post("kd_kategori"))->num_rows() > 0){
			echo "<option value=''>-- Pilih Sub Kategori --</option>";
			foreach($this->Data_model->getSubKategori($this->input->post("kd_kategori"))->result() as $row){
				echo "<option value='$row->kd_subkategori'>$row->nm_subkategori</option>";
			}
		}else{
			echo "<option value='0'>-</option>";
		}
	}
	public function kd_subjenis()
	{
		if($this->Data_model->getSubjenis($this->input->post("kd_jenis"))->num_rows() > 0){
			echo "<option value=''>-- Pilih Sub Jenis --</option>";
			foreach($this->Data_model->getSubjenis($this->input->post("kd_jenis"))->result() as $row){
				echo "<option value='$row->kd_subjenis'>$row->nm_subjenis</option>";
			}
		}else{
			echo "<option value='0'>-</option>";
		}
	}
	public function kd_subsubjenis()
	{
		if($this->Data_model->getSubsubjenis($this->input->post("kd_subjenis"))->num_rows() > 0){
			echo "<option value=''>-- Pilih Sub sub Jenis --</option>";
			foreach($this->Data_model->getSubsubjenis($this->input->post("kd_subjenis"))->result() as $row){
				echo "<option value='$row->kd_subsubjenis'>$row->nm_subsubjenis</option>";
			}
		}else{
				echo "<option value='0'>-</option>";
		}
	}
	public function uttp_per_kecamatan()
	{
		if($this->Data_model->getPtera_per_kecamatan($this->input->post("id_kecamatan"))->num_rows() > 0){
			$no = 1;
			foreach($this->Data_model->getPtera_per_kecamatan($this->input->post("id_kecamatan"))->result() as $row){
				echo "<tr>";
				echo "<td>".$no."</td>";
				echo "<td>".$row->kd_ptera."</td>";
				echo "<td>".$row->nm_pengguna."</td>";
				echo "<td>".$row->nm_jenis."</td>";
				echo "<td>".$row->nm_subjenis."</td>";
				echo "<td>".$row->no_seri."</td>";
				echo "<td>".$row->kapasitas."</td>";
				echo "<td>".$row->terakhir_tera."</td>";
				echo "<td>".$row->masaberlaku."</td>";
				echo "</tr>";
				$no++;
			}
		}else{
			echo 'Not available';
		}
	}

	public function user_edit($id)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				if (md5($this->input->post('password'))==md5($this->input->post('re-password'))) {
					$id = $this->input->post('id_user');
					$data = array(
						'username'=>$this->input->post('username'),
						'password'=>md5($this->input->post('password')),
						'nama'=>$this->input->post('nama'),
						'nip'=>$this->input->post('nip'),
						'level'=>$this->input->post('level')
					);
					$this->Data_model->edit_user($id,$data);
					$this->session->set_flashdata('edit_sukses','Berhasil mengedit user');
					redirect(base_url('backend/user_edit/'.$id));
				} else {
					$this->session->set_flashdata('pass_not_match','Password tidak sama');
					redirect(base_url('backend/user_edit/'.$id));
				}

			} else {
				$data['user'] = $this->Data_model->get_user($id);
				$this->load->view('backend/edit_user',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function user_hapus($id)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->user_hapus($id);
			$this->session->set_flashdata('hapus','User telah dihapus');
			redirect(base_url('backend/pengaturan_user'));
		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subjenis()
	{
		if ($this->session->userdata('status')=='login') {
			$data['subjenis'] = $this->Data_model->get_subjenis();
			$this->load->view('backend/pengaturan_subjenis',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function tambah_subjenis()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$no_urut = $this->Data_model->get_urut_subjenis($this->input->post('kd_jenis'));
				$kd_subjenis = $this->input->post('kd_jenis').$no_urut;
				$data = array(
						'kd_subjenis'=>$kd_subjenis,
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'nm_subjenis'=>$this->input->post('nm_subjenis'),
						'tarif_baru'=>$this->input->post('tarif_baru'),
						'tarif_ulang'=>$this->input->post('tarif_ulang'),
						'satuan'=>$this->input->post('satuan')
					);
				$this->Data_model->tambah_subjenis($data);
				$this->session->set_flashdata('tambah_subjenis','Subjenis berhasil ditambah');
				redirect(base_url('backend/pengaturan_subjenis'));
			} else {
				$data['jenis'] = $this->Data_model->getJenis();
				$this->load->view('backend/subjenis_tambah',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subjenis_edit($kd_subjenis)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data = array(
						'kd_jenis'=>$this->input->post('kd_jenis'),
						'nm_subjenis'=>$this->input->post('nm_subjenis'),
						'tarif_baru'=>$this->input->post('tarif_baru'),
						'tarif_ulang'=>$this->input->post('tarif_ulang'),
						'satuan'=>$this->input->post('satuan')
					);
				$this->Data_model->subjenis_edit($kd_subjenis,$data);

				$this->session->set_flashdata('edit_subjenis','Data berhasil dirubah');
				redirect(base_url('backend/pengaturan_subjenis'));
			} else {
				$data['jenis'] = $this->Data_model->getJenis();
				$data['subjenis'] = $this->Data_model->get_subjenis_by_kd_subjenis($kd_subjenis);
				$this->load->view('backend/subjenis_edit',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subjenis_hapus($kd_subjenis)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->delete_subjenis($kd_subjenis);
			$this->session->set_flashdata('delete_subjenis','Subjenis telah dihapus');
			redirect(base_url('backend/pengaturan_subjenis'));
		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subsubjenis()
	{
		if ($this->session->userdata('status')=='login') {
			$data['subsubjenis'] = $this->Data_model->get_subsubjenis();
			$this->load->view('backend/pengaturan_subsubjenis',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function tambah_subsubjenis()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$no_urut = $this->Data_model->get_urut_subsubjenis($this->input->post('kd_subjenis'));
				$kd_subsubjenis = $this->input->post('kd_subjenis').$no_urut;
				$data = array(
						'kd_subsubjenis'=>$kd_subsubjenis,
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'nm_subsubjenis'=>$this->input->post('nm_subsubjenis'),
						'tarif_baru'=>$this->input->post('tarif_baru'),
						'tarif_ulang'=>$this->input->post('tarif_ulang'),
						'satuan'=>$this->input->post('satuan')
					);
				$this->Data_model->tambah_subsubjenis($data);
				$this->session->set_flashdata('tambah_subsubjenis','Sub-sub jenis berhasil ditambah');
				redirect(base_url('backend/pengaturan_subsubjenis'));
			} else {
				$data['subjenis'] = $this->Data_model->getSubJenis2();
				$this->load->view('backend/subsubjenis_tambah',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subsubjenis_hapus($kd_subsubjenis)
	{
		if ($this->session->userdata('status')=='login') {
			$this->Data_model->delete_subsubjenis($kd_subsubjenis);
			$this->session->set_flashdata('delete_subsubjenis','Subsub Jenis telah dihapus');
			redirect(base_url('backend/pengaturan_subsubjenis'));
		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_subsubjenis_edit($kd_subsubjenis)
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data = array(
						'kd_subjenis'=>$this->input->post('kd_subjenis'),
						'nm_subsubjenis'=>$this->input->post('nm_subsubjenis'),
						'tarif_baru'=>$this->input->post('tarif_baru'),
						'tarif_ulang'=>$this->input->post('tarif_ulang'),
						'satuan'=>$this->input->post('satuan')
					);
				$this->Data_model->subsubjenis_edit($kd_subsubjenis,$data);

				$this->session->set_flashdata('edit_subsubjenis','Data berhasil dirubah');
				redirect(base_url('backend/pengaturan_subsubjenis'));
			} else {
				$data['subjenis'] = $this->Data_model->getSubJenis2();
				$data['subsubjenis'] = $this->Data_model->get_subsubjenis_by_kd_subsubjenis($kd_subsubjenis);
				$this->load->view('backend/subsubjenis_edit',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function pengaturan_web()
	{
		if ($this->session->userdata('status')=='login') {

			$this->load->view('backend/edit_web',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	function setting_web_ac(){
		if($this->session->userdata('status')=='login'){

		}
		else{
			redirect(base_url('front'));
		}
	}
	public function report_pendaftaran_harian()
	{
		if ($this->session->userdata('status')=='login') {
			$data['report'] = $this->Data_model->report_daftar_harian();
			$data['jml_pendaftar'] = $this->Data_model->report_daftar_harian()->num_rows();
			$this->load->view('backend/report_pendaftaran_harian',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function report_pendaftaran_bulanan()
	{
		if ($this->session->userdata('status')=='login') {
			$data['report'] = $this->Data_model->report_bulanan();
			$data['jml_pendaftar'] = $this->Data_model->report_bulanan()->num_rows();
			$this->load->view('backend/report_pendaftaran_bulanan',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function early_warning_system()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['kecamatan'] = $this->Data_model->getKecamatan();
				$data['warning'] = $this->Data_model->early_warning_sys_satu_bln($this->input->post('id_kecamatan'));
				$data['nama_kecamatan'] = $this->Data_model->getNamaKecamatan($this->input->post('id_kecamatan'));
				$this->load->view('backend/data_warning',$data);
			} else {
				$data['kecamatan'] = $this->Data_model->getKecamatan();
			    $this->load->view('backend/input_bulan_early_wrn_sys',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}

	public function data_tera_kecamatan()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['kecamatan'] = $this->Data_model->getKecamatan();
				$data['uttp'] = $this->Data_model->getPtera_per_kecamatan($this->input->post('id_kecamatan'));
				$data['nm_kecamatan'] = $this->Data_model->getNamaKecamatan($this->input->post('id_kecamatan'));
				$this->load->view('backend/data_tera_kecamatan',$data);
			} else {
				$data['kecamatan'] = $this->Data_model->getKecamatan();
				$this->load->view('backend/input_kec_tera',$data);
			}

		} else {
			redirect(base_url('front'));
		}

	}
	
	public function reportPerUsaha()
	{
		if($this->session->userdata('status')=='login')
		{
			if(isset($_POST['btnSubmit']))
			{
				$usaha = $this->input->post('usaha');
				$data['uttp'] = $this->Data_model->getUttpKategori($usaha);
				$data['usaha'] = $this->Data_model->getKategori_ById($usaha);
				$data['kd_kategori'] = $usaha;
				
				$this->load->view('backend/reportPerUsaha',$data);
			} else {
				$data['usaha'] = $this->Data_model->getUsaha();
				$this->load->view('backend/lap_pasar_input',$data);
			}
		} else {
			redirect(base_url('front'));
		}
	}
	
	function reportPerUsahaPDF($kd_kategori)
	{
		$data['uttp'] = $this->Data_model->getUttpKategori($kd_kategori);
		$data['usaha'] = $this->Data_model->getKategori_ById($kd_kategori);
		
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/reportPerUsahaPDF',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/reportPerUsahaPDF",array('Attachment'=>0),$data);
	}
}
