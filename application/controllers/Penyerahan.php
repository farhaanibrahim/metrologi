<?php
/**
*
*/
class Penyerahan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['uttp'] = $this->Data_model->getDataPenyerahan();
			$this->load->view('backend/penyerahan',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function cetak()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['penyerahan'] = $this->Data_model->cetak_bukti($this->input->post('kd_pengguna'),$this->input->post('no_reg'));
				if ($data['penyerahan']->num_rows()>0) {
					$this->load->view('backend/cetak_bukti_serah',$data);
				} else {
					$this->session->set_flashdata('kosong','Data tidak ditemukan');
					redirect(base_url('penyerahan/cetak'));
				}
			} else {
				$this->load->view('backend/cetak_bukti_serah_input');
			}

		} else {
			redirect(base_url('front'));
		}

	}
	public function konfirmasi_penyerahan($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$tgl_serah = $this->input->post('tgl_serah');
			$this->Data_model->konfirmasi_penyerahan($no_reg,$tgl_serah);
			$this->session->set_flashdata('confirm','Confirmed');
			redirect(base_url('penyerahan/cetak_bukti_penyerahan/'.$kd_pengguna.'/'.$no_reg));
		} else {
			redirect(base_url('front'));
		}

	}
	public function cetak_bukti_penyerahan($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['penyerahan'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
			$this->load->view('backend/cetak_bukti_serah',$data);
		} else {
			redirect(base_url('front'));
		}

	}
	public function cetak_bukti_penyerahan_pdf($kd_pengguna,$no_reg)
	{
		$data['serah'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/cetak_bukti_serah_pdf',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/cetak_bukti_serah_pdf",array('Attachment'=>0),$data);
	}
}
