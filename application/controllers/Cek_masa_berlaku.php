<?php
/**
* 
*/
class Cek_masa_berlaku extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	function index()
	{
		$id_pengguna = array('t_pengguna.kd_pengguna'=>$this->input->post('kd_pengguna'));
		$data['hasil'] = $this->Data_model->cek_masa_berlaku($id_pengguna);
		$this->load->view('frontend/result_cek_masa_berlaku',$data);
	}	
}