<?php
/**
* 
*/
class Pembayaran extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	public function index()
	{
		if ($this->session->userdata('status')=='login') {
			$data['uttp'] = $this->Data_model->getDataPembayaran();
			$this->load->view('backend/pembayaran',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function konfirmasi_pembayaran($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data = array(
				'jml_bayar'=>$this->input->post('jml_bayar'),
				'tgl_bayar'=>$this->input->post('tgl_bayar')
				);
			$this->Data_model->konfirmasi_pembayaran($no_reg,$data);
			$this->session->set_flashdata('confirm');
			redirect(base_url('pembayaran/preview_pembayaran/'.$kd_pengguna.'/'.$no_reg));
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cetak_pembayaran()
	{
		if ($this->session->userdata('status')=='login') {
			if (isset($_POST['btnSubmit'])) {
				$data['bayar'] = $this->Data_model->cetak_bukti($this->input->post('kd_pengguna'),$this->input->post('no_reg'));
				if ($data['bayar']->num_rows() > 0) {
					$this->load->view('backend/cetak_pembayaran2',$data);
				} else {
					$this->session->set_flashdata('kosong','Data tidak ditemukan');
					redirect(base_url('pembayaran/cetak_pembayaran'));
				}
			} else {
				$this->load->view('backend/cetak_pembayaran');
			}
			
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function preview_pembayaran($kd_pengguna,$no_reg)
	{
		if ($this->session->userdata('status')=='login') {
			$data['bayar'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
			$this->load->view('backend/cetak_pembayaran2',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function cetak_pembayaran_pdf($kd_pengguna,$no_reg)
	{
		$data['bayar'] = $this->Data_model->cetak_bukti($kd_pengguna,$no_reg);
		$this->load->library('pdf');

	    $this->pdf->load_view('backend/cetak_tanda_bayar',$data);
	    $this->pdf->set_paper('B5','landscape');
	    $this->pdf->render();
	    $this->pdf->stream("backend/cetak_tanda_bayar",array('Attachment'=>0),$data);
	}
	public function report_pembayaran_harian()
	{
		if ($this->session->userdata('status')=='login') {
			$data['report'] = $this->Data_model->report_harian();
			$this->load->view('backend/report_pembayaran_harian',$data);
		} else {
			redirect(base_url('front'));
		}
		
	}
	public function report_pembayaran_bulanan()
	{
		if ($this->session->userdata('status')=='login') {
			$data['report'] = $this->Data_model->report_bulanan();
			$this->load->view('backend/report_pembayaran_bulanan',$data);
		} else {
			redirect(base_url('front'));
		}
	}
}