<?php foreach ($bayar->result() as $row) {} ?>
<link href="<?php echo base_url('assets/backend'); ?>/assets/css/bootstrap.css" rel="stylesheet">
<title>Bukti Pendaftaran</title>
<style type="text/css">
	table{
          border: 1px solid;
          width: 700px;
          padding-top: 100px;
          padding-left: 0px;
        }
	.bukti_daftar h3{
          text-align: center;
        }
	table tr{
          padding-left: 20px;
          padding-bottom: 50px;
        }
	table td {
          padding: 15px 35px;
      }
</style>
<div class="col-md-12">
	<div class="bukti_daftar">
                        <table>
                        <tr>
                        	<td colspan="4"><h3>BUKTI PEMBAYARAN</h3></td>
                        </tr>
                        <tr>
                          <td><strong>No. Pendaftaran</strong></td>
                          <td>:<?php echo $row->no_reg; ?></td>
                          <td><strong>Tanggal Bayar</strong></td>
                          <td>:<?php echo $row->tgl_bayar; ?></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Nama Pemilik</strong></td>
                          <td>:<?php echo $row->nm_pengguna; ?></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Kode Pemilik</strong></td>
                          <td>:<?php echo $row->kd_pengguna; ?></td>
                          <td></td>
                          <td></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Total</strong></td>
                          <td>:Rp <?php echo $row->jml_bayar; ?></td>
                          <td></td>
                          <td></td>
                        </tr>
                      	</table>
                      	</div>
</div>					