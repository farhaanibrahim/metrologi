      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Laporan</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <div class="content-panel">
                  <h4><i class="fa fa-angle-right"></i> <?php foreach($usaha->result() as $usaha){echo $usaha->nm_kategori;} ?></h4>
                  <div class="container-fluid">
                    <section id="unseen">
						<a href="<?php echo base_url('backend/reportPerUsahaPDF'); ?>/<?php echo $kd_kategori; ?>" class="btn btn-success pull-right">PDF</a>
						<br></br></br>
                      <table id="mytable" class="table table-bordered">
						<thead>
							<tr>
								<th>No.</th>
								<th>Barcode</th>
								<th>Nama Pengguna</th>
								<th>Jenis</th>
								<th>Sub jenis</th>
								<th>No. Seri</th>
								<th>Kapasitas</th>
								<th>Masaberlaku</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach($uttp->result() as $row): ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><img style="width:100px;" src="<?php echo base_url("backend/create_barcode"); ?>/<?php echo $row->kd_ptera; ?>" /></td>
									<td><?php echo $row->nm_pengguna; ?></td>
									<td><?php echo $row->nm_jenis; ?></td>
									<td><?php echo $row->nm_subjenis; ?></td>
									<td><?php echo $row->no_seri; ?></td>
									<td><?php echo $row->kapasitas; ?></td>
									<td><?php echo $row->masaberlaku; ?></td>
									<td>
										<?php if ($row->status == "expired"): ?>
										  <span class="label label-danger">Expired</span>
										<?php endif; ?>
										<?php if ($row->status == "proses"): ?>
										  <span class="label label-default">On Progress</span>
										<?php endif; ?>
										<?php if ($row->status == "selesai"): ?>
										  <span class="label label-success">Active</span>
										<?php endif; ?>
									</td>
								</tr>
								<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					  </table>
                    </section>
                  </div>
                </div>
          		</div>
          	</div>
			
		      </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
