      <?php 
        foreach ($tera->result() as $data) {}
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Pengolahan <i class="fa fa-angle-right"></i> Edit Tera</h3>
          	<div class="row mt">
              <div class="col-lg-12">
                  <div class="form-panel">
                      <h4 class="mb"><i class="fa fa-angle-right"></i> Form Elements</h4>
                      <form class="form-horizontal style-form" action="<?php echo base_url('backend/data_tera_edit_ac'); ?>" method="post">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Kode Tera</label>
                              <div class="col-sm-10">
                                  <input class="form-control" id="disabledInput" type="text" value="<?php echo $data->kd_ptera; ?>" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Kode Pengguna</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" value="<?php echo $data->kd_pengguna; ?>" disabled>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Jenis</label>
                              <div class="col-sm-10">
                                  <select class="form-control">
                                    <option><?php echo $data->nm_jenis; ?></option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">No. Seri</label>
                              <div class="col-sm-10">
                                  <select class="form-control">
                                    <option><?php echo $data->no_seri; ?></option>
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Kapasitas</label>
                              <div class="col-sm-10">
                                  <input type="number" class="form-control" value="<?php echo $data->kapasitas; ?>">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Stok Liter</label>
                              <div class="col-sm-10">
                                  <input type="number" class="form-control" value="<?php echo $data->stok_liter; ?>">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Cap tera</label>
                              <div class="col-sm-10">
                                <div class="radio">
                                <label>
                                  <input type="radio" name="cap_tera" id="optionsRadios1" value="Ada" required>
                                  Ada
                                  <br>
                                  <input type="radio" name="cap_tera" id="optionsRadios1" value="Tidak Ada" required>
                                  Tidak Ada
                                </label>
                              </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Kondisi</label>
                              <div class="col-sm-10">
                                <div class="radio">
                                <label>
                                  <input type="radio" name="kondisi" id="optionsRadios1" value="Baik" required>
                                  Baik
                                  <br>
                                  <input type="radio" name="kondisi" id="optionsRadios1" value="Rusak" required>
                                  Rusak
                                </label>
                              </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Tindakan</label>
                              <div class="col-sm-10">
                                <div class="radio">
                                <label>
                                  <input type="radio" name="tindakan" id="optionsRadios1" value="Diganti" required>
                                  Diganti
                                  <br>
                                  <input type="radio" name="tindakan" id="optionsRadios1" value="Tidak Diganti" required>
                                  Tidak Diganti
                                  <br>
                                  <input type="radio" name="tindakan" id="optionsRadios1" value="DiTera" required>
                                  Ditera
                                </label>
                              </div>
                              </div>
                          </div>
                          <div class="form-group">
                            <div class="col-sm-2 col-sm-2"></div>
                            <div class="col-sm-10">
                              <input type="submit" name="btnSubmit" class="btn btn-round btn-primary" value="Simpan">
                            </div>
                          </div>
                      </form>
                  </div>
              </div><!-- col-lg-12-->       
            </div><!-- /row -->
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
