<table border="1">
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Tera</th>
								<th>Nama Pengguna</th>
								<th>Jenis</th>
								<th>Sub jenis</th>
								<th>No. Seri</th>
								<th>Kapasitas</th>
								<th>Masaberlaku</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php foreach($uttp->result() as $row): ?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $row->kd_ptera; ?></td>
									<td><?php echo $row->nm_pengguna; ?></td>
									<td><?php echo $row->nm_jenis; ?></td>
									<td><?php echo $row->nm_subjenis; ?></td>
									<td><?php echo $row->no_seri; ?></td>
									<td><?php echo $row->kapasitas; ?></td>
									<td><?php echo $row->masaberlaku; ?></td>
									<td>
										<?php if ($row->status == "expired"): ?>
										  <span class="label label-danger">Expired</span>
										<?php endif; ?>
										<?php if ($row->status == "proses"): ?>
										  <span class="label label-default">On Progress</span>
										<?php endif; ?>
										<?php if ($row->status == "selesai"): ?>
										  <span class="label label-success">Active</span>
										<?php endif; ?>
									</td>
								</tr>
								<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					  </table>