<?php foreach ($pendaftaran->result() as $row) {} ?>
<link href="<?php echo base_url('assets/backend'); ?>/assets/css/bootstrap.css" rel="stylesheet">
<title>Bukti Pendaftaran</title>
<style type="text/css">
	table{
          border: 1px solid;
          width: 800px;
          padding-top: 100px;
          padding-left: 50px;
        }
	.bukti_daftar h3{
          text-align: center;
        }
	table tr{
          padding-left: 20px;
          padding-bottom: 50px;
        }
	table td {
          padding: 15px 35px;
      }
</style>
<div class="col-md-12">
	<div class="bukti_daftar">
                        <table>
                        <tr>
                        	<td colspan="3"><h3>BUKTI PENDAFTARAN</h3></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>No. Pendaftaran</strong></td>
                          <td>:</td>
                          <td><?php echo $row->no_reg; ?></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Nama Pemilik</strong></td>
                          <td>:</td>
                          <td><?php echo $row->nm_pengguna; ?></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Kode Pemilik</strong></td>
                          <td>:</td>
                          <td><?php echo $row->kd_pengguna; ?></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Jenis UTTP</strong></td>
                          <td>:</td>
                          <td><?php echo $row->nm_subjenis; ?></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>No. Seri</strong></td>
                          <td>:</td>
                          <td><?php echo $row->no_seri; ?></td>
                        </tr>
                        <tr class="item-tabel">
                          <td><strong>Tanggal</strong></td>
                          <td>:</td>
                          <td><?php echo $row->tgl_daftar; ?></td>
                        </tr>
                      	</table>
                      	</div>
</div>					