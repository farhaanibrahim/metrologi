      <?php foreach ($pengguna->result() as $pengguna) {} ?>
      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Cek Online</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <div class="content-panel">
                  <h4><i class="fa fa-angle-right"></i> View Pendaftar Tera Ulang Online</h4>
                  <div class="container-fluid">
                    <section id="unseen">
                      <form action="<?php echo base_url('cek_online/acc_tera_ulang'); ?>" method="post">
                      <input type="hidden" name="no_trans" value="<?php echo $pengguna->no_trans; ?>">
                      <input type="hidden" name="kd_pengguna" value="<?php echo $pengguna->kd_pengguna; ?>">
                      <input type="hidden" name="kd_ptera" value="<?php echo $pengguna->kd_ptera; ?>">

                      <input type="hidden" name="id_kecamatan" value="<?php echo $pengguna->id_kecamatan; ?>">
                      <input type="hidden" name="kd_kategori" value="<?php echo $pengguna->kd_kategori; ?>">
                      <input type="hidden" name="kd_subkategori" value="<?php echo $pengguna->kd_subkategori; ?>">
                      <input type="hidden" name="nm_pengguna" value="<?php echo $pengguna->nm_pengguna; ?>">
                      <input type="hidden" name="nm_usaha" value="<?php echo $pengguna->nm_usaha; ?>">
                      <input type="hidden" name="alamat" value="<?php echo $pengguna->alamat; ?>">
                      <input type="hidden" name="telp" value="<?php echo $pengguna->telp; ?>">
                      <input type="hidden" name="hp" value="<?php echo $pengguna->hp; ?>">
                      <input type="hidden" name="email" value="<?php echo $pengguna->email; ?>">
                      <input type="hidden" name="kd_jenis" value="<?php echo $pengguna->kd_jenis; ?>">
                      <input type="hidden" name="kd_subjenis" value="<?php echo $pengguna->kd_subjenis; ?>">
                      <input type="hidden" name="kd_subsubjenis" value="<?php echo $pengguna->kd_subsubjenis; ?>">
                      <input type="hidden" name="merk" value="<?php echo $pengguna->merk; ?>">
                      <input type="hidden" name="no_seri" value="<?php echo $pengguna->no_seri; ?>">
                      <input type="hidden" name="kapasitas" value="<?php echo $pengguna->kapasitas; ?>">
                      <input type="hidden" name="stok_liter" value="<?php echo $pengguna->stok_liter; ?>">

                        <div class="col-md-6">
                                <h4><u>A. DATA PEMILIK/PENGGUNA</u></h4>
                                <table class="table">
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_kecamatan; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kategori</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_kategori; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Sub Kategori</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_subkategori; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pemilik/Pengguna</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_pengguna; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Usaha</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_usaha; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->alamat; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->telp; ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Handphone</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->hp; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->email; ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h4><u>A. DATA JENIS ALAT</u></h4>
                                <table class="table">
                                    <tr>
                                        <td>Jenis</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_jenis; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Sub Jenis</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->nm_subjenis; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Sub-sub Jenis</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->kd_subsubjenis; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Merk</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->merk; ?></td>
                                    </tr>
                                    <tr>
                                        <td>No. Seri</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->no_seri; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Kapasitas</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->kapasitas; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Stok Liter</td>
                                        <td>:</td>
                                        <td><?php echo $pengguna->stok_liter; ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="form-group">
                                                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Konfirmasi">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                      </form>
                    </section>
                  </div>
                </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
