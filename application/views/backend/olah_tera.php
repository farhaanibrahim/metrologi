      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Pengolahan <i class="fa fa-angle-right"></i> Tera</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <div class="content-panel">
                  <h4><i class="fa fa-angle-right"></i>Data Tera</h4>
                    <div class="container-fluid">
                      <section id="unseen">
                      <?php if ($this->session->flashdata('delete_ptera')): ?>
                        <div class="alert alert-info">
                          <strong>Info!</strong> <?php echo $this->session->flashdata('delete_ptera'); ?>
                        </div>
                      <?php endif ?>
                      <table id="mytable" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>No. Transk</th>
                            <th>Barcode</th>
                            <th>Kode Tera</th>
                            <th>Pemilik</th>
                            <th>Jenis</th>
                            <th>Sub Jenis</th>
                            <th>Sub sub jenis</th>
                            <th>No Seri</th>
                            <th>Kapasitas</th>
                            <th>Tgl Daftar</th>
                            <th>Terakhir Tera</th>
                            <th>Masa berlaku</th>
                            <th>Cap Tera</th>
                            <th>Kondisi</th>
                            <th>Tindakan</th>
                            <th>Status</th>
                            <th>Hasil Uji</th>
                            <th>Keterangan</th>
                            <th>Opsi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($tera->result() as $tera): ?>
                            <tr>
                              <td><?php echo $tera->no_trans; ?></td>
                              <td align="center"><img style="width:100px;" src="<?php echo base_url("backend/create_barcode"); ?>/<?php echo $tera->kd_ptera; ?>" /></td>
                              <td><?php echo $tera->kd_ptera; ?></td>
                              <td><?php echo $tera->nm_pengguna; ?></td>
                              <td><?php echo $tera->nm_jenis; ?></td>
                              <td><?php echo $tera->nm_subjenis; ?></td>
                              <td><?php echo $tera->nm_subsubjenis; ?></td>
                              <td><?php echo $tera->no_seri; ?></td>
                              <td><?php echo $tera->kapasitas; ?> (Kg)</td>
                              <td><?php echo $tera->tgl_daftar; ?></td>
                              <td><?php echo $tera->terakhir_tera; ?></td>
                              <td><?php echo $tera->masaberlaku; ?></td>
                              <td><?php echo $tera->cap_tera; ?></td>
                              <td><?php echo $tera->kondisi; ?></td>
                              <td><?php echo $tera->tindakan; ?></td>
                              <td><?php echo $tera->status; ?></td>
                              <td><?php echo $tera->hasil_uji; ?></td>
                              <td><?php echo $tera->keterangan; ?></td>
                              <td>
                                <a href="<?php echo base_url('backend/data_tera_hapus'); ?>/<?php echo $tera->no_trans; ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                              </td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </section>
                    </div> 
                </div>
          		</div>
          	</div>
			
		</section><!--/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
