<!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="<?php echo base_url('upload'); ?>/logo.png" class="img" width="60"></a></p>
              	  <h5 class="centered"><?php echo $this->session->userdata('nama'); ?></h5>
              	  	
                  <li class="mt">
                      <a class="active" href="<?php echo base_url('backend'); ?>">
                          <i class="fa fa-dashboard"></i>
                          <span>Dashboard</span>
                      </a>
                  </li>

                  <li>
                      <a href="<?php echo base_url('backend/data_tera_kecamatan'); ?>">
                          <i class="fa fa-dashboard"></i>
                          <span>Data Tera Kecamatan</span>
                      </a>
                  </li>

                  <li>
                    <a href="<?php echo base_url('backend/early_warning_system'); ?>">
                      <i class="fa fa-dashboard"></i>
                      <span>Early Warning System</span>
                    </a>
                  </li>

                  <?php if ($this->session->userdata('level')=='super_admin'||$this->session->userdata('level')=='pimpinan'||$this->session->userdata('level')=='pembayaran'||$this->session->userdata('level')=='pendaftaran'): ?>
                    <li>
                    <a href="#">
                      <i class="fa fa-archive"></i>
                      <span>Laporan</span>
                    </a>
                    <ul class="sub">
                        <?php if ($this->session->userdata('level')=='super_admin'||$this->session->userdata('level')=='pembayaran'): ?>
                          <li><a href="<?php echo base_url('pembayaran/report_pembayaran_harian'); ?>">Laporan Pembayaran Harian</a></li>
                          <li><a href="<?php echo base_url('pembayaran/report_pembayaran_bulanan'); ?>">Laporan Pembayaran Bulanan</a></li>
                        <?php endif ?>
                        <?php if ($this->session->userdata('level')=='super_admin'||$this->session->userdata('level')=='pendaftaran'): ?>
                          <li><a href="<?php echo base_url('backend/report_pendaftaran_harian'); ?>">Lap. Pendaftaran Harian</a></li>
							<li><a href="<?php echo base_url('backend/report_pendaftaran_bulanan'); ?>">Lap. Pendaftaran Bulanan</a></li>
                        <?php endif ?>
						<li><a href="<?php echo base_url('backend/reportPerUsaha'); ?>">Lap. Pasar / Perusahaan</a></li>
                    </ul>
                  </li>
                  <?php endif ?>

                  <?php if ($this->session->userdata('level')=='pendaftaran'||$this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-archive"></i>
                          <span>Cek Online</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('cek_online'); ?>">Tera Baru Online</a></li>
                        <li><a href="<?php echo base_url('cek_online/cek_tera_ulang'); ?>">Tera Ulang Online</a></li>
                      </ul>
                  </li>
                  <?php endif ?>

                  <?php if ($this->session->userdata('level')=='pendaftaran'||$this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-archive"></i>
                          <span>Pendaftaran</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('backend/tera_baru'); ?>">Permohonan Tera Baru</a></li>
                        <li><a href="<?php echo base_url('tera_ulang'); ?>">Permohonan Tera Ulang</a></li>
                        <li><a href="<?php echo base_url('backend/cetak_bukti_daftar'); ?>">Cetak Bukti Pendaftaran</a></li>
                      </ul>
                  </li>
                  <?php endif ?>
                  <?php if ($this->session->userdata('level')=='penguji'||$this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-archive"></i>
                          <span>Penera</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('penguji'); ?>">Tera Baru</a></li>
                        <li><a href="<?php echo base_url('tera_ulang/uji'); ?>">Tera Ulang</a></li>
                        <li><a href="<?php echo base_url('penguji/cetak_bukti_tera'); ?>">Cetak Bukti Tera</a></li>
                      </ul>
                  </li>
                  <?php endif ?>
                  <?php if ($this->session->userdata('level')=='penera'||$this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-archive"></i>
                          <span>Pembubuhan</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('penera'); ?>">Daftar Pembubuhan</a></li>
                        <li><a href="<?php echo base_url('penera/cetak_bukti_pembubuhan') ?>">Cetak Bukti Pembubuhan</a></li>
                        <li><a href="">Cetak Sertifikat</a></li>
                        <li><a href="">Cetak Stiker</a></li>
                        <li><a href="<?php echo base_url('penera/kartu_pemilik'); ?>">Cetak Kartu Pemilik</a></li>
                      </ul>
                  </li>
                  <?php endif ?>
                  <?php if ($this->session->userdata('level')=='pembayaran'||$this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-archive"></i>
                          <span>Pembayaran</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('pembayaran'); ?>">Daftar Pembayaran</a></li>
                        <li><a href="<?php echo base_url('pembayaran/cetak_pembayaran'); ?>">Cetak Bukti Pembayaran</a></li>
                      </ul>
                  </li>
                  <?php endif ?>
                  <?php if ($this->session->userdata('level')=='penyerahan'||$this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-archive"></i>
                          <span>Penyerahan</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('penyerahan'); ?>">Penyerahan UTTP</a></li>
                        <li><a href="<?php echo base_url('penyerahan/cetak'); ?>">Cetak Bukti Penyerahan</a></li>
                      </ul>
                  </li>
                  <?php endif ?>

                  <?php if ($this->session->userdata('level')=='super_admin'): ?>
                    <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-desktop"></i>
                          <span>Pengelolaan Data</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('backend/data_pengguna'); ?>">Data Pengguna</a></li>
                        <li><a href="<?php echo base_url('backend/data_ptera'); ?>">Data Peneraan</a></li>
                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="#" >
                          <i class="fa fa-cogs"></i>
                          <span>Pengaturan Data</span>
                      </a>
                      <ul class="sub">
                        <li><a href="<?php echo base_url('backend/pengaturan_kategori'); ?>">Kategori</a></li>
                        <li><a href="<?php echo base_url('backend/pengaturan_sub_kategori'); ?>">Sub-Kategori</a></li>
                        <li><a href="<?php echo base_url('backend/pengaturan_jenis'); ?>">Jenis</a></li>
                        <li><a href="<?php echo base_url('backend/pengaturan_subjenis'); ?>">Sub Jenis</a></li>
                        <li><a href="<?php echo base_url('backend/pengaturan_subsubjenis'); ?>">Sub-sub Jenis</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="<?php echo base_url('backend/pengaturan_user'); ?>" >
                          <i class="fa fa-cogs"></i>
                          <span>Pengaturan User</span>
                      </a>
                  </li>
                  <?php endif ?>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->