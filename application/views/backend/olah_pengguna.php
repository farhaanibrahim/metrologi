      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Pengolahan data <i class="fa fa-angle-right"></i>Pengguna</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
              <?php if ($this->session->flashdata('tambah_sukses')): ?>
                <div class="alert alert-success">
                  <strong>Success!</strong> <?php echo $this->session->flashdata('tambah_sukses'); ?>
                </div>
              <?php endif ?>
            		<div class="content-panel">
                  <h4><i class="fa fa-angle-right"></i>Data Pengguna</h4>
                    <div class="container-fluid">
                      <section id="unseen">
                      <?php if ($this->session->flashdata('delete_pengguna')): ?>
                        <div class="alert alert-info">
                          <strong>Info!</strong> <?php echo $this->session->flashdata('delete_pengguna'); ?>
                        </div>
                      <?php endif ?>
                      <table id="mytable" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>Kode Pengguna</th>
                            <th>Kecamatan</th>
                            <th>Kategori</th>
                            <th>Sub Kategori</th>
                            <th>Nama Pengguna</th>
                            <th>Nama Usaha</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Handphone</th>
                            <th>Email</th>
                            <th>Opsi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($pengguna->result() as $pengguna): ?>
                            <tr>
                              <td><?php echo $pengguna->kd_pengguna; ?></td>
                              <td><?php echo $pengguna->nm_kecamatan; ?></td>
                              <td><?php echo $pengguna->nm_kategori; ?></td>
                              <td><?php echo $pengguna->nm_subkategori; ?></td>
                              <td><?php echo $pengguna->nm_pengguna; ?></td>
                              <td><?php echo $pengguna->nm_usaha; ?></td>
                              <td><?php echo $pengguna->alamat; ?></td>
                              <td><?php echo $pengguna->telp; ?></td>
                              <td><?php echo $pengguna->hp; ?></td>
                              <td><?php echo $pengguna->email; ?></td>
                              <td>
                                <a href="<?php echo base_url('backend/data_pengguna_hapus'); ?>/<?php echo $pengguna->kd_pengguna; ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                              </td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </section>
                    </div>
                </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
