      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Pengaturan <i class="fa fa-angle-right"></i> Sub Kategori</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
            		<div class="content-panel">
                  <h4><i class="fa fa-angle-right"></i>Data Sub Kategori</h4>
                    <div class="container-fluid">
                      <section id="unseen">
                      <?php if ($this->session->flashdata('input_subkategori')): ?>
                        <div class="alert alert-success">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('input_subkategori'); ?>
                        </div>
                      <?php endif ?>
                      <?php if ($this->session->flashdata('edit_subkategori')): ?>
                        <div class="alert alert-info">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('edit_subkategori'); ?>
                        </div>
                      <?php endif ?>
                      <?php if ($this->session->flashdata('delete_subkategori')): ?>
                        <div class="alert alert-info">
                          <strong>Success!</strong> <?php echo $this->session->flashdata('delete_subkategori'); ?>
                        </div>
                      <?php endif ?>
                      <table id="mytable" class="table table-bordered table-striped">
                        <a href="<?php echo base_url('backend/pengaturan_subkategori_tambah'); ?>" class="btn btn-success">(+) Tambah Sub Kategori</a><br><br>
                        <thead>
                          <tr>
                            <th>Kode Sub Kategori</th>
                            <th>Kode Kategori</th>
                            <th>Nama Sub Kategori</th>
                            <th>Opsi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($sub_kategori->result() as $sub_kategori): ?>
                            <tr>
                              <td><?php echo $sub_kategori->kd_subkategori; ?></td>
                              <td><?php echo $sub_kategori->kd_kategori; ?></td>
                              <td><?php echo $sub_kategori->nm_subkategori; ?></td>
                              <td>
                                <a href="<?php echo base_url('backend/pengaturan_subkategori_edit'); ?>/<?php echo $sub_kategori->kd_subkategori; ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                <a href="<?php echo base_url('backend/pengaturan_subkategori_hapus'); ?>/<?php echo $sub_kategori->kd_subkategori; ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                              </td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </section>
                    </div>td
                </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
