      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i> Pengaturan <i class="fa fa-angle-right"></i> Tambah User</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <div class="form-panel">
          		  <?php if ($this->session->flashdata('tbh_usr_sukses')): ?>
          		  	<div class="alert alert-success">
					  <strong>Success!</strong> <?php echo $this->session->flashdata('tbh_usr_sukses'); ?>
					</div>
          		  <?php endif ?>
                  <h4 class="mb"><i class="fa fa-angle-right"></i> Form Tambah User</h4>
                  <form class="form-horizontal style-form" method="post" action="<?php echo base_url('backend/tambah_user'); ?>">
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label"><b>Username</b></label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" name="username" placeholder="Username">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label"><b>Nama</b></label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" name="nama" placeholder="Nama">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label"><b>NIP</b></label>
                      <div class="col-sm-4">
                          <input type="text" class="form-control" name="nip" placeholder="NIP">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label"><b>Level</b></label>
                      <div class="col-sm-4">
                          <select class="form-control" name="level">
                            <option>Level</option>
                            <option value="super_admin">Administrator</option>
                            <option value="pimpinan">Pimpinan</option>
                            <option value="petugas">Petugas</option>
                          </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label"><b>Password</b></label>
                      <div class="col-sm-4">
                          <input type="password" class="form-control" name="password" placeholder="Password">
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-sm-2 col-sm-2 control-label"></div>
                      <div class="col-sm-4">
                          <input type="submit" class="btn btn-primary" name="btnSubmit" value="Tambah">
                      </div>
                    </div>
                  </form>
                </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>

  </body>
</html>
