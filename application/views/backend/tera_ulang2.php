      <?php 
        foreach ($pengguna->result() as $pengguna) {}
        foreach ($tera->result() as $tera) {}
      ?>
      <?php 
        include 'template/header.php'; 
        include 'template/sidebar.php';
      ?>
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          	<h3><i class="fa fa-angle-right"></i>Pemohon/Pemilik</h3>
          	<div class="row mt">
          		<div class="col-lg-12">
          		  <div class="content-panel">
                  <h4><i class="fa fa-angle-right"></i>Pengguna Baru</h4>
                  <div class="container-fluid">
                    <section id="unseen">
                    <form action="<?php echo base_url('tera_ulang/tera_ulang_ac2'); ?>" method="post">
                    <input type="hidden" name="kd_pengguna" value="<?php echo $kd_pengguna; ?>">
                    <input type="hidden" name="kd_ptera" value="<?php echo $kd_ptera; ?>">
                    <input type="hidden" name="no_reg_lama" value="<?php echo $tera->no_reg; ?>">
                    <div class="tabbable-panel">
                      <div class="tabbable-line">
                        <ul class="nav nav-tabs ">
                          <li class="active">
                            <a href="#tab_default_1" data-toggle="tab">
                            Form Pengguna Baru</a>
                          </li>
                          <li>
                            <a href="#tab_default_2" data-toggle="tab">
                            Form Pemohon Tera </a>
                          </li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_default_1">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Kecamatan</label>
                                  <select class="form-control" name="id_kecamatan" id="id_kecamatan" disabled>
                                    <option value="<?php echo $pengguna->id_kecamatan; ?>"><?php echo $pengguna->nm_kecamatan; ?></option>
                                  </select>
                                  <input type="hidden" name="no_urut" id="no_urut" value="">
                                </div>
                                <div class="form-group">
                                  <label>Kategori</label>
                                  <select class="form-control" name="kd_kategori" id="kd_kategori" disabled>
                                    <option value="<?php echo $pengguna->kd_kategori; ?>"><?php echo $pengguna->nm_kategori; ?></option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label>Sub Kategori</label>
                                  <select class="form-control" name="kd_subkategori" id="kd_subkategori" disabled>
                                    <option value="<?php echo $pengguna->kd_subkategori; ?>"><?php echo $pengguna->nm_subkategori; ?></option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label>Nama Pengguna</label>
                                  <input type="text" name="nm_pengguna" class="form-control" value="<?php echo $pengguna->nm_pengguna; ?>" disabled>
                                </div>
                                <div class="form-group">
                                  <label>Nama Usaha</label>
                                  <input type="text" name="nm_usaha" class="form-control" value="<?php echo $pengguna->nm_usaha; ?>" disabled>
                                </div>
								<div class="form-group">
                                  <label>Alamat</label>
                                  <input type="text" name="alamat" class="form-control" value="<?php echo $pengguna->alamat; ?>" disabled>
                                </div>
                              </div>
                              <div class="col-md-6">
								<div class="form-group">
                                  <label>RT</label>
                                  <input type="number" name="rt" class="form-control" value="<?php echo $pengguna->rt ?>" disabled>
                                </div>
                                <div class="form-group">
                                  <label>RW</label>
                                  <input type="number" name="rw" class="form-control" value="<?php echo $pengguna->rw ?>" disabled>
                                </div>
                                <div class="form-group">
                                  <label>Telepon</label>
                                  <input type="text" name="telp" class="form-control" value="<?php echo $pengguna->telp; ?>" disabled>
                                </div>
                                <div class="form-group">
                                  <label>Handphone</label>
                                  <input type="text" name="hp" class="form-control" value="<?php echo $pengguna->hp; ?>" disabled>
                                </div>
                                <div class="form-group">
                                  <label>Email</label>
                                  <input type="text" name="email" class="form-control" value="<?php echo $pengguna->email; ?>" disabled>
                                </div>
								<div class="form-group">
                                  <label>Tanggal Pendaftaran</label>
                                  <input type="date" name="tgl_daftar" class="form-control" value="<?php echo $tera->tgl_daftar; ?>">
                                </div>
                              </div>
                              <div class="col-md-12" align="right">
                                <a href="#tab_default_2" data-toggle="tab" class="btn btn-primary">Selanjutnya</a>
                              </div>
                          </div>
                          <div class="tab-pane" id="tab_default_2">
                            <div class="col-md-6">
                              <div class="form-group">
                                <label>Jenis</label>
                                <select class="form-control" name="kd_jenis" required>
                                  <option value="<?php echo $tera->kd_jenis; ?>"><?php echo $tera->nm_jenis; ?></option>
                                  <!--<?php foreach ($jenis->result() as $jenis): ?>
                                    <option value="<?php echo $jenis->kd_jenis; ?>"><?php echo $jenis->nm_jenis; ?></option>
                                  <?php endforeach ?>-->
                                </select>
                              </div>
                              <div class="form-group">
                                <label>Sub Jenis</label>
                                <select class="form-control" name="kd_subjenis" id="kd_subsubjenis" required>
                                  <option value="<?php echo $tera->kd_subjenis; ?>"><?php echo $tera->nm_subjenis; ?></option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label>Sub sub jenis</label>
                                <select class="form-control" name="kd_subsubjenis" id="kd_subsubjenis" required>
                                  <option><?php echo $tera->kd_subsubjenis; ?></option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label>Merk</label>
                                <input type="text" name="merk" class="form-control" value="<?php echo $tera->merk; ?>">
                              </div>
                              <div class="form-group">
                                <label>No. Seri</label>
                                <input type="text" name="no_seri" class="form-control" value="<?php echo $tera->no_seri; ?>" >
                              </div>
                              <div class="form-group">
                                <label>Kapasitas</label>
                                <input type="number" name="kapasitas" class="form-control" value="<?php echo $tera->kapasitas; ?>">
                              </div>
                              <div class="form-group">
                                <label>Stok Liter</label>
                                <input type="number" name="stok_liter" class="form-control" value="<?php echo $tera->stok_liter; ?>">
                              </div>
                            </div>
                            <div class="col-md-6">
                              
                            </div>
                            <div class="col-md-12" align="right">
                              <div class="form-group">
                                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Simpan">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    </form>
                    </section>
                  </div>
                </div>
          		</div>
          	</div>
			
		</section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <?php include 'template/footer.php'; ?>
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/sparkline-chart.js"></script>    
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/zabuto_calendar.js"></script>
    
  <script type="application/javascript">
        $(document).ready(function () {
            $("#date-popover").popover({html: true, trigger: "manual"});
            $("#date-popover").hide();
            $("#date-popover").click(function (e) {
                $(this).hide();
            });
        
            $("#my-calendar").zabuto_calendar({
                action: function () {
                    return myDateFunction(this.id, false);
                },
                action_nav: function () {
                    return myNavFunction(this.id);
                },
                ajax: {
                    url: "show_data.php?action=1",
                    modal: true
                },
                legend: [
                    {type: "text", label: "Special event", badge: "00"},
                    {type: "block", label: "Regular event", }
                ]
            });
        });
        
        
        function myNavFunction(id) {
            $("#date-popover").hide();
            var nav = $("#" + id).data("navigation");
            var to = $("#" + id).data("to");
            console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>
    <script type="text/javascript">
      $("#kd_kategori").change(function(){
        var kd_kategori = $("#kd_kategori option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/sub_kategori')?>",
          type: "POST",
          data  : "kd_kategori="+kd_kategori,
          success : function (msg) {
            document.getElementById("kd_subkategori").disabled = false;
            $("#kd_subkategori").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_jenis").change(function(){
        var kd_jenis = $("#kd_jenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subjenis')?>",
          type: "POST",
          data  : "kd_jenis="+kd_jenis,
          success : function (msg) {
            document.getElementById("kd_subjenis").disabled = false;
            $("#kd_subjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_subjenis").change(function(){
        var kd_subjenis = $("#kd_subjenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subsubjenis')?>",
          type: "POST",
          data  : "kd_subjenis="+kd_subjenis,
          success : function (msg) {
            document.getElementById("kd_subsubjenis").disabled = false;
            $("#kd_subsubjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
    </script>
  </body>
</html>
