<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>E-Metrologi</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/backend'); ?>/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="<?php echo base_url('assets/backend'); ?>/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.4.3/morris.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/backend'); ?>/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/backend'); ?>/assets/css/style-responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
		.donut-legend > span {
		  display: inline-block;
		  margin-right: 25px;
		  margin-bottom: 10px;
		  font-size: 13px;
		}
		.donut-legend > span:last-child {
		  margin-right: 0;
		}
		.donut-legend > span > i {
		  display: inline-block;
		  width: 15px;
		  height: 15px;
		  margin-right: 7px;
		  margin-top: -3px;
		  vertical-align: middle;
		  border-radius: 1px;
		}

    .donut-legend2 > span {
      display: inline-block;
      margin-right: 25px;
      margin-bottom: 10px;
      font-size: 13px;
    }
    .donut-legend2 > span:last-child {
      margin-right: 0;
    }
    .donut-legend2 > span > i {
      display: inline-block;
      width: 15px;
      height: 15px;
      margin-right: 7px;
      margin-top: -3px;
      vertical-align: middle;
      border-radius: 1px;
    }
	</style>
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>E-METROLOGI DISPERINDAG KOTA BOGOR</b></a>
            <!--logo end-->
            <div class="top-menu">
              <ul class="nav pull-right top-menu">
                    <li><a class="logout" href="<?php echo base_url('login/logout'); ?>">Logout</a></li>
              </ul>
            </div>
        </header>
      <!--header end--><?php echo base_url('assets/backend'); ?>

      <?php include 'template/sidebar.php'; ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
          <h3><i class="fa fa-angle-right"></i> Dashboard</h3>
              <!-- page start-->
              <div id="morris">
                  <div class="row mt">
                      <div class="col-lg-6">
                          <div class="content-panel">
                              <h4><i class="fa fa-angle-right"></i> Prosentase Kadaluarsa UTTP per Kecamatan <!--Tahun <?php echo date('Y'); ?> - <?php echo date('Y')+1; ?>--></h4>
                              <div class="panel-body">
                                  <div id="hero-donut" class="graph"></div>
								  <div id="legend" class="donut-legend"></div>
                              </div>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="content-panel">
                              <h4><i class="fa fa-angle-right"></i> Jumlah UTTP per Kecamatan <!--Tahun <?php echo date('Y'); ?> - <?php echo date('Y')+1; ?>--></h4>
                              <div class="panel-body">
                                  <div id="hero-donut2" class="graph"></div>
                  <div id="legend2" class="donut-legend2"></div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
      </section><!-- /MAIN CONTENT -->

      <!--main content end-->
      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2017 - E-Metrologi
              <a href="morris.html#" class="go-top">
                  <i class="fa fa-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/jquery.nicescroll.js" type="text/javascript"></script>


    <!--common script for all pages-->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.4.3/morris.min.js"></script>
		<script src="<?php echo base_url('assets/backend'); ?>/assets/js/common-scripts.js"></script>

    <!-- DataTables -->
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url('assets/backend'); ?>/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <!--script for this page
    <script src="<?php echo base_url('assets/backend'); ?>/assets/js/morris-conf.js"></script> -->
    <script type="text/javascript">

		$(document).ready(function () {
			var color_array = ['#03658C', '#7CA69E', '#F2594A','#F28C4B','#7E6F6A','#36AFB2'];

			var browsersChart = Morris.Donut({
					element: 'hero-donut',
					data: [
					  {label: 'Bogor Selatan \n'+<?php echo $jml_bog_sel_exp; ?>+' UTTP', value: <?php echo substr($bog_sel_exp,0,5); ?> },
					  {label: 'Bogor Timur \n'+<?php echo $jml_bog_tim_exp; ?>+' UTTP', value: <?php echo substr($bog_tim_exp,0,5); ?> },
					  {label: 'Bogor Utara \n'+<?php echo $jml_bog_utr_exp; ?>+' UTTP', value: <?php echo substr($bog_utr_exp,0,5); ?> },
					  {label: 'Bogor Tengah \n'+<?php echo $jml_bog_teng_exp; ?>+' UTTP', value: <?php echo substr($bog_teng_exp,0,5); ?> },
					  {label: 'Bogor Barat \n'+<?php echo $jml_bog_bar_exp; ?>+' UTTP', value: <?php echo substr($bog_bar_exp,0,5); ?> },
					  {label: 'Tanah Sareal \n'+<?php echo $jml_tnh_sar_exp; ?>+' UTTP', value: <?php echo substr($tnh_sar_exp,0,5); ?> }
					],
					  colors: color_array,
					  formatter: function (y) { return y + "%" }
				  });
				  browsersChart.options.data.forEach(function(label, i){
					var legendItem = $('<span></span>').text(label['label']).prepend('<i>&nbsp;</i>');
					legendItem.find('i').css('backgroundColor', browsersChart.options.colors[i]);
					$('#legend').append(legendItem)
				  })
		});
    </script>
    <script type="text/javascript">
      $(document).ready(function () {
      var color_array = ['#03658C', '#7CA69E', '#F2594A','#F28C4B','#7E6F6A','#36AFB2'];

      var browsersChart = Morris.Donut({
          element: 'hero-donut2',
          data: [
            {label: 'Bogor Selatan', value: <?php echo $bog_sel_data; ?> },
            {label: 'Bogor Timur', value: <?php echo $bog_tim_data; ?> },
            {label: 'Bogor Utara', value: <?php echo $bog_utr_data; ?> },
            {label: 'Bogor Tengah', value: <?php echo $bog_teng_data; ?> },
            {label: 'Bogor Barat', value: <?php echo $bog_bar_data; ?> },
            {label: 'Tanah Sareal', value: <?php echo $tnh_sar_data; ?> }
          ],
            colors: color_array,
            formatter: function (y) { return y + " UTTP" }
          });
          browsersChart.options.data.forEach(function(label, i){
          var legendItem = $('<span></span>').text(label['label']).prepend('<i>&nbsp;</i>');
          legendItem.find('i').css('backgroundColor', browsersChart.options.colors[i]);
          $('#legend2').append(legendItem)
          })
    });
    </script>
    <script>
      $(function () {
        $('#mytable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });
      });
      $("#id_kecamatan").change(function(){
        var id_kecamatan = $("#id_kecamatan option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/uttp_per_kecamatan')?>",
          type: "POST",
          data  : "id_kecamatan="+id_kecamatan,
          success : function (msg) {
            $("#result").html(msg);
          }
        });
      });
    </script>

  </body>
</html>
