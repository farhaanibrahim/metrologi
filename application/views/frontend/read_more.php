<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <div class="col-md-12">
                                <article class="post">
                <header class="post-header">
                    <div class="fotorama" data-allowfullscreen="true">
                        <img src="<?php echo base_url('assets/source'); ?>/berita.jpg" alt="Image Alternative text" title="196_365" />
                    </div>
                </header>
                <div class="post-inner">
                    <h4 class="post-title text-darken">KOTA BOGOR URUS TERA DAN TERA ULANG UTTP</h4>
                    <ul class="post-meta">
                        <li><i class="fa fa-calendar"></i><a href="#">Mar 27, 2017</a>
                        </li>
                    <p>mediabogor.com, Bogor – kewenangan dan pengawasan terkait penyelenggaraan metrologi legal tera dan tera ulang alat-alat ukur, takar, timbang dan perlengkapannya (UTTP) kini dilimpahkan dari Pemerintah Provinsi kepada Kabupaten/ Kota. Hal itu berdasarkan UU nomor 23 Tahun 2014 tentang Pemerintahan Daerah.</p>
                    
                    <br>
                    <p>Kepala Dinas Perindustrian dan Perdagangan (Disperindag) Kota Bogor Achsin Prasetyo mengatakan, bahwa pelimpahan tera dan tera ulang UTTP diperkuat dengan diterbitkannya Surat Keterangan Kemampuan Pelayanan Tera dan Tera Ulang (SKKPTTU) No : 45/PTKN/KKPTTU/3/2017 tertanggal 17 Maret 2017 oleh Direktur Jenderal Perlindungan Konsumen dan Tertib Niaga (Ditjen PKTN) Kementerian Perdagangan (Kemendag).</p>
                    <br>

                    <p>“Iya, pastinya potensi dari retribusi jasa umum tera dan tera ulang masuk Pendapatan Asli Daerah (PAD). Sebelumnya, ada bagi hasil dari provinsi dari sektor retribusi ini hanya 35 persen atau sekitar Rp400 juta. Sekarang bisa 100 persen dan diperkirakan target tahun 2017 capai Rp1 miliar,,” ujar Achsin disela – sela Peresmian pelimpahan di Gedung eks Balai Metrologi, Jalan Raya Tajur, Kota Bogor, Senin (27/3).</p>
                    <br>

                    <p>Selain itu, Lanjut Achsin, karena pihaknya sudah memiliki kewenangan akan melakukan pengawasan termasuk sosialisasi kegiatan tera dan tera ulang UTTP di dibeberapa tempat seperti halnya pasar, perusahaan-perusahaan dan lainnya. “Bagi yang mau yang melaksanakan itu tinggal datang ke Gedung eks Balai Metrologi, yang sekarang Bidang Metrologi dan Tertib Niaga. Untuk yang besaran tarif retribusi tera dan tera ulang sudah diatur dalam Perda 4/2012,” pungksanya. (AW).</p>
                </div>
            </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>