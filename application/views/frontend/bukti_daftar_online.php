<?php foreach ($bukti_daftar->result() as $row) {} ?>
<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <?php if ($this->session->flashdata('tambah_sukses')): ?>
                            <div class="alert alert-success">
                              <strong>Success!</strong> <?php echo $this->session->flashdata('tambah_sukses'); ?>
                            </div>
                              <?php endif ?>
                              <table class="table table-stripped">
                                  <tr>
                                      <td>Nama</td>
                                      <td>:</td>
                                      <td><?php echo $row->nm_pengguna; ?></td>
                                  </tr>
                                  <tr>
                                      <td>No. Pendaftaran</td>
                                      <td>:</td>
                                      <td><?php echo $row->no_reg; ?></td>
                                  </tr>
                                  <tr>
                                      <td>Kode Pengguna/Pemilik</td>
                                      <td>:</td>
                                      <td><?php echo $row->kd_pengguna; ?></td>
                                  </tr>
                              </table>
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>
<script type="text/javascript">
      $("#kd_kategori").change(function(){
        var kd_kategori = $("#kd_kategori option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/sub_kategori')?>",
          type: "POST",
          data  : "kd_kategori="+kd_kategori,
          success : function (msg) {
            document.getElementById("kd_subkategori").disabled = false;
            $("#kd_subkategori").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_jenis").change(function(){
        var kd_jenis = $("#kd_jenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subjenis')?>",
          type: "POST",
          data  : "kd_jenis="+kd_jenis,
          success : function (msg) {
            document.getElementById("kd_subjenis").disabled = false;
            $("#kd_subjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_subjenis").change(function(){
        var kd_subjenis = $("#kd_subjenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subsubjenis')?>",
          type: "POST",
          data  : "kd_subjenis="+kd_subjenis,
          success : function (msg) {
            document.getElementById("kd_subsubjenis").disabled = false;
            $("#kd_subsubjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
    </script>