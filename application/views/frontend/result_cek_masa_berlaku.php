<?php foreach ($hasil->result() as $tahun) {}?>
<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                            <div class="gap"></div>

                            <div class="col-md-12">
                            	<h4><i class="fa fa-angle-right"></i>Masa Berlaku</h4>
                            	<div class="list-group">
                            	<?php if ($hasil->num_rows() > 0): ?>
                            		<?php foreach ($hasil->result() as $row): ?>
                            			<div class="list">
			                            	<div class="col-md-12">
												<strong>Nama</strong> : <?php echo $row->nm_pengguna; ?><br>
			                            	<strong>Masa Berlaku</strong> : <?php echo $row->masaberlaku; ?><br><br>
											</div>
											<div class="col-md-2">
												<img style="width:80%;" src="<?php echo base_url("backend/create_barcode"); ?>/<?php echo $row->kd_ptera; ?>" />
											</div>
											
											<div class="col-md-2">
												<strong><?php echo $row->kd_ptera; ?></strong>
											</div>
											
											<div class="col-md-2">
												<strong><?php echo $row->nm_subjenis; ?></strong>
											</div>
										</div>
                            		<?php endforeach ?>
                            	<?php else: ?>
                            		<a href="#" class="list-group-item">
			                            	<strong>Id Pengguna Tidak Ada</strong>
			                        </a>
                            	<?php endif ?>
                            	</div>
                                <div class="gap"></div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>