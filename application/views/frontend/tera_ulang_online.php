<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <?php if ($this->session->flashdata('kosong')): ?>
                            <div class="alert alert-warning">
                              <strong>Warning!</strong> <?php echo $this->session->flashdata('kosong'); ?>
                            </div>
                              <?php endif ?>
                              <form action="<?php echo base_url('front/tera_ulang_online_ac'); ?>" method="post">
                              <div class="col-md-8">
                                <div class="form-group">
                                  
                                  <div class="col-md-4">
                                    <label>Masukkan Kode Pengguna</label>
                                    <input type="text" name="kd_pengguna" class="form-control" placeholder="Kode Pengguna">
                                  </div>
                                  <div class="col-md-4">
                                  <label>Masukkan Kode Tera</label>
                                    <input type="text" name="kd_ptera" class="form-control" placeholder="Kode Ptera">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-md-4">
                                    <input type="submit" name="btnSubmit" class="btn btn-warning" value="Next">
                                  </div>
                                </div>
                              </div>
                            </form>
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>
<script type="text/javascript">
      $("#kd_kategori").change(function(){
        var kd_kategori = $("#kd_kategori option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/sub_kategori')?>",
          type: "POST",
          data  : "kd_kategori="+kd_kategori,
          success : function (msg) {
            document.getElementById("kd_subkategori").disabled = false;
            $("#kd_subkategori").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_jenis").change(function(){
        var kd_jenis = $("#kd_jenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subjenis')?>",
          type: "POST",
          data  : "kd_jenis="+kd_jenis,
          success : function (msg) {
            document.getElementById("kd_subjenis").disabled = false;
            $("#kd_subjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_subjenis").change(function(){
        var kd_subjenis = $("#kd_subjenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subsubjenis')?>",
          type: "POST",
          data  : "kd_subjenis="+kd_subjenis,
          success : function (msg) {
            document.getElementById("kd_subsubjenis").disabled = false;
            $("#kd_subsubjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
    </script>