<?php foreach ($pengguna->result() as $pengguna) {} ?>
<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <h4>Formulir Pendaftaran</h4>
                            <form action="<?php echo base_url('front/pendaftaran3'); ?>" method="post">
                                <div class="col-md-6">
                                <input type="hidden" name="kd_pengguna" value="<?php echo $kd_pengguna; ?>">
                                <strong><u>A. DATA PEMILIK/PENGGUNA</u></strong>
                                <table>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="id_kecamatan" id="id_kecamatan" disabled>
                                                    <option value="<?php echo $pengguna->id_kecamatan; ?>"><?php echo $pengguna->nm_kecamatan; ?></option>
                                                  </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kategori</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                              <select class="form-control" name="kd_kategori" id="kd_kategori" disabled>
                                                <option value="<?php echo $pengguna->kd_kategori; ?>"><?php echo $pengguna->nm_kategori; ?></option>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Kategori</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subkategori" id="kd_subkategori" disabled>
                                                <option value="<?php echo $pengguna->kd_subkategori; ?>"><?php echo $pengguna->nm_subkategori; ?></option>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pemilik/Pengguna</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="nm_pengguna" class="form-control" value="<?php echo $pengguna->nm_pengguna; ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Usaha</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="nm_usaha" class="form-control" value="<?php echo $pengguna->nm_usaha; ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="alamat" class="form-control" value="<?php echo $pengguna->alamat; ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="telp" class="form-control" value="<?php echo $pengguna->telp; ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Handphone</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="hp" class="form-control" value="<?php echo $pengguna->hp; ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="email" class="form-control" value="<?php echo $pengguna->email; ?>" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <strong><u>A. DATA JENIS ALAT</u></strong>
                                <table>
                                    <tr>
                                        <td>Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_jenis" id="kd_jenis">
                                                  <option>-- Pilih Jenis --</option>
                                                  <?php foreach ($jenis->result() as $jenis): ?>
                                                    <option value="<?php echo $jenis->kd_jenis; ?>"><?php echo $jenis->nm_jenis; ?></option>
                                                  <?php endforeach ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subjenis" id="kd_subjenis" disabled>
                                                  <option>-- Pilih Sub Jenis --</option>
                                                </select>
                                            </div> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub-sub Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subsubjenis" id="kd_subsubjenis" disabled>
                                              <option>-- Pilih Sub sub Jenis --</option>
                                            </select>    
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Merk</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="merk" class="form-control" placeholder="Merk">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Seri</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="no_seri" class="form-control" placeholder="No. Seri">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kapasitas</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="kapasitas" class="form-control" placeholder="Kapasitas">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Stok Liter</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="stok_liter" class="form-control" placeholder="Stok Liter">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="form-group">
                                                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Submit">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            </form>
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>
<script type="text/javascript">
      $("#kd_kategori").change(function(){
        var kd_kategori = $("#kd_kategori option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/sub_kategori')?>",
          type: "POST",
          data  : "kd_kategori="+kd_kategori,
          success : function (msg) {
            document.getElementById("kd_subkategori").disabled = false;
            $("#kd_subkategori").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_jenis").change(function(){
        var kd_jenis = $("#kd_jenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subjenis')?>",
          type: "POST",
          data  : "kd_jenis="+kd_jenis,
          success : function (msg) {
            document.getElementById("kd_subjenis").disabled = false;
            $("#kd_subjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_subjenis").change(function(){
        var kd_subjenis = $("#kd_subjenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subsubjenis')?>",
          type: "POST",
          data  : "kd_subjenis="+kd_subjenis,
          success : function (msg) {
            document.getElementById("kd_subsubjenis").disabled = false;
            $("#kd_subsubjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
    </script>