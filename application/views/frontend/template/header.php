<!DOCTYPE HTML>
 <?php //foreach ($instansi->result() as $instansiInfo) {} ?>
<html>

<head>
    <title>Metrologi</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="Template, html, premium, themeforest" />
    <meta name="description" content="Traveler - Premium template for travel companies">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/icomoon.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/mystyles.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/frontend'); ?>/css/AdminLTE.min.css">
    <script src="<?php echo base_url('assets/frontend'); ?>/js/modernizr.js"></script>

    <style type="text/css">
        footer{
            color: white;
        }
        .panel {
            margin-top: 50px;
            border-radius: 10px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }
        .custom_btn{
            width: 100%;
            margin-top: 5px;
        }
        .header-text{
            font-size: 25px;
        }
        #main-footer{
        }
    </style>
</head>

<body>

    <!-- FACEBOOK WIDGET -->
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- /FACEBOOK WIDGET -->
    <div class="global-wrap">
        <header id="main-header">
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a class="logo" href="<?php echo base_url('back'); ?>" style="color: white;">
                                <p class="header-text">APLIKASI E-METROLOGI DISPERINDAG KOTA BOGOR</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
              <div class="nav navbar-left">
                  <ul class="slimmenu" id="slimmenu">
                      <li><a href="<?php echo base_url('front'); ?>">Home</a>
                      </li>
                      <li><a href="<?php echo base_url('front/profile'); ?>">Profil</a>
                      </li>
                      <li><a href="<?php echo base_url('front/regulasi'); ?>">Regulasi</a>
                      </li>
                      <li><a href="<?php echo base_url('front/berita'); ?>">Berita</a>
                      </li>
                      <li>
                        <a href="#">Pendaftaran Online</a>
                        <ul class="">
                          <li><a href="<?php echo base_url('front/pendaftaran'); ?>">Pendaftaran Baru Online</a></li>
                          <li><a href="<?php echo base_url('front/tera_ulang_online'); ?>">Tera Ulang Online</a></li>
                        </ul>
                      </li>
                  </ul>
              </div>
              <div class="nav navbar-right">
              <form action="<?php echo base_url('cek_masa_berlaku'); ?>" method="post">
                <ul class="slimmenu" id="slimmenu">
                  <li><a href="#">Cek Masa Berlaku</a></li>
                    <li>
                    <input type="text" name="kd_pengguna" class="form-control" placeholder="Input Id Pemilik">
                  </li>
                  <li><input type="submit" name="btnSubmit" class="btn btn-warning" value="Oke"></li>
                </ul>
              </form>
              </div>
          </div>
        </header>