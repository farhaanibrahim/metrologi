<footer id="main-footer">
            <div class="container">
                <div class="row row-wrap" align="center">
                  <h5>&copy; 2017 | Dinas Perumahan dan Permukiman Kota Bogor - Aplikasi Metrologi Kota Bogor</h5>
                </div>
            </div>
        </footer>

        <script src="<?php echo base_url('assets/frontend'); ?>/js/jquery.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/bootstrap.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/slimmenu.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/bootstrap-timepicker.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/nicescroll.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/dropit.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/ionrangeslider.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/icheck.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/typeahead.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/card-payment.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/magnific.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/owl-carousel.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/fitvids.js"></script>
        <script src="<?php echo base_url('assets/frontenfrontendd'); ?>/js/tweet.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/countdown.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/gridrotator.js"></script>
        <script src="<?php echo base_url('assets/frontend'); ?>/js/custom.js"></script>
    </div>
</body>

</html>