<?php
    foreach ($pengguna->result() as $pengguna) {}
    foreach ($tera->result() as $tera) {}
?>
<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <h4>Formulir Pendaftaran</h4>
                            <form action="<?php echo base_url('front/tera_ulang_online2'); ?>" method="post">
                                <div class="col-md-6">
                                <input type="hidden" name="kd_pengguna" value="<?php echo $kd_pengguna; ?>">
                                <input type="hidden" name="kd_ptera" value="<?php echo $kd_ptera; ?>">
                                <strong><u>A. DATA PEMILIK/PENGGUNA</u></strong>
                                <table>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="id_kecamatan" id="id_kecamatan">
                                                    <option value="<?php echo $pengguna->id_kecamatan; ?>"><?php echo $pengguna->nm_kecamatan; ?></option>
                                                  </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kategori</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                              <select class="form-control" name="kd_kategori" id="kd_kategori">
                                                <option value="<?php echo $pengguna->kd_kategori; ?>"><?php echo $pengguna->nm_kategori; ?></option>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Kategori</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subkategori" id="kd_subkategori">
                                                <option value="<?php echo $pengguna->kd_subkategori; ?>"><?php echo $pengguna->nm_subkategori; ?></option>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pemilik/Pengguna</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="nm_pengguna" class="form-control" value="<?php echo $pengguna->nm_pengguna; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Usaha</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="nm_usaha" class="form-control" value="<?php echo $pengguna->nm_usaha; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="alamat" class="form-control" value="<?php echo $pengguna->alamat; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>RT</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" name="rt" class="form-control" value="<?php echo $pengguna->rt; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>RW</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" name="rw" class="form-control" value="<?php echo $pengguna->rw; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="telp" class="form-control" value="<?php echo $pengguna->telp; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Handphone</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="hp" class="form-control" value="<?php echo $pengguna->hp; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="email" class="form-control" value="<?php echo $pengguna->email; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <strong><u>A. DATA JENIS ALAT</u></strong>
                                <table>
                                    <tr>
                                        <td>Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_jenis" required>
                                                  <option value="<?php echo $tera->kd_jenis; ?>"><?php echo $tera->nm_jenis; ?></option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subjenis" id="kd_subsubjenis" required>
                                                  <option value="<?php echo $tera->kd_subjenis; ?>"><?php echo $tera->nm_subjenis; ?></option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub-sub Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subsubjenis" id="kd_subsubjenis" required>
                                              <option><?php echo $tera->kd_subsubjenis; ?></option>
                                            </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Merk</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <input type="text" name="merk" class="form-control" value="<?php echo $tera->merk; ?>">
                                                  </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Seri</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="no_seri" class="form-control" value="<?php echo $tera->no_seri; ?>" >
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kapasitas</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" name="kapasitas" class="form-control" value="<?php echo $tera->kapasitas; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Stok Liter</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="number" name="stok_liter" class="form-control" value="<?php echo $tera->stok_liter; ?>">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="form-group">
                                                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Submit">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            </form>
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>
<script type="text/javascript">
      $("#kd_kategori").change(function(){
        var kd_kategori = $("#kd_kategori option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/sub_kategori')?>",
          type: "POST",
          data  : "kd_kategori="+kd_kategori,
          success : function (msg) {
            document.getElementById("kd_subkategori").disabled = false;
            $("#kd_subkategori").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_jenis").change(function(){
        var kd_jenis = $("#kd_jenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subjenis')?>",
          type: "POST",
          data  : "kd_jenis="+kd_jenis,
          success : function (msg) {
            document.getElementById("kd_subjenis").disabled = false;
            $("#kd_subjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_subjenis").change(function(){
        var kd_subjenis = $("#kd_subjenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subsubjenis')?>",
          type: "POST",
          data  : "kd_subjenis="+kd_subjenis,
          success : function (msg) {
            document.getElementById("kd_subsubjenis").disabled = false;
            $("#kd_subsubjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
    </script>
