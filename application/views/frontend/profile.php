<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                            <div class="gap"></div>
                            <div class="col-md-12 text-center">
                            	<h2>PROFIL BALAI METROLOGI DISPERINDAG KOTA BOGOR</h2>
                            </div>
                            <div class="col-md-12">
                                <p>
                                    Balai kemetrologian bogor historinya merupakan salah satu UPTD dari dinas perindustrian dan perdagangan provinsi jawa barat. 
                                </p>
                                <p>Saat ini, Sudah diserah terimakan kewenangannya kepada Dinas Perindustrian Perdagangan Kota Bogor. Yang fungsinya untuk memberikan pelayanan kemetrologian di kota : Bogor dan sekitarnya, yang beralamat di jalan Tajur no.52. kota Bogor. </p>
                                <p>Rincian tugas kemetrologian Bogor yaitu :</p>
                                <ol>
                                    <li>Menyelengggarakan penyusunan program kerja Balai kemetrologian bogor</li>
                                    <li>Menyelenggarakan pengkajian bahan petunjuk teknis kemetrologian</li>
                                    <li>Menyelenggarakan pengelolaan standar ukuran,laboratorium dan verifikasi standar ukuran di daerah dan kabupaten / kota.</li>
                                    <li>Menyelenggarakan interkomparasi standar ukuran di daerah</li>
                                    <li>menyelenggarakan rekomendasi,penilaian dan kriteria standar ukuran dan laboratorium uji metrologi legal kabupaten / kota</li>
                                    <li>Menyelenggarakan  pelayanan tera,tera ulang,kalibrasi,UTTP dan pengujian BDKT</li>
                                    <li>menyelenggarakan fasilitasi pemungutan retribusi sesuai ketentuan peraturan perundang-undangan</li>
                                    <li>Menyelenggarakan penyuluhan, Pengawasan dan pengamatan UTTP,BDKT,dan SI di daerah</li>
                                    <li>Menyelenggarakan koordinasi dan pembinaan pembuat UTTP,importir UTTP, penyusunan bahan rekomendasi permohonan ijin tanda pabrik serta perpanjangan dan memberikan penilaian ijin perbaikan UTTP.</li>
                                    <li>Menyelenggarakan ketatausahaan balai kemetrologian bogor</li>
                                    <li>menyelengggarakan telaahan staf sebagai bahan pertimbangan pengambilan kebijakan</li>
                                    <li>Menyelenggarakan koordinasi dengan unit kerja terkait</li>
                                    <li>Menyelenggarakan evaluasi dan pelaporan</li>
                                    <li>menyelenggarakan tugas lain sesuai dengan tugas pokok dan fungsinya</li>
                                </ol>
                                <p>
                                adapun Visi Misi Undang-undang metrologi legal adalah ; ”Bahwa untuk melindungi kepentingan umum perlu adanya ketertiban dan kepastian hukum dalam pemakaian satuan ukuran, standar satuan,metode pengukuran dan alat-alat ukur, takar,timbang dan perlengkapannya”
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>