<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <h4>Formulir Pendaftaran</h4>
                            <form action="<?php echo base_url('front/pendaftaran2'); ?>" method="post">
                                <div class="col-md-6">
                            <strong><u>A. DATA PEMILIK/PENGGUNA</u></strong>
                                <table>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="id_kecamatan" id="id_kecamatan">
                                                <option>-- Pilih Kecamatan --</option>
                                                <?php foreach ($kecamatan->result() as $row): ?>
                                                  <option value="<?php echo $row->id_kecamatan; ?>"><?php echo $row->nm_kecamatan; ?></option>
                                                <?php endforeach ?>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kategori</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_kategori" id="kd_kategori">
                                                <option>-- Pilih Kategori --</option>
                                                <?php foreach ($kategori->result() as $row2): ?>
                                                  <option value="<?php echo $row2->kd_kategori; ?>"><?php echo $row2->nm_kategori; ?></option>
                                                <?php endforeach ?>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Kategori</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subkategori" id="kd_subkategori" disabled>
                                                <option>-- Pilih Sub Kategori --</option>
                                              </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Pemilik/Pengguna</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="nm_pengguna" class="form-control" placeholder="Nama Pengguna">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Nama Usaha</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="nm_usaha" class="form-control" placeholder="Nama Usaha">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Alamat</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="alamat" class="form-control" placeholder="Alamat">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>RT</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="rt" class="form-control" placeholder="RT">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>RW</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="rw" class="form-control" placeholder="RW">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Telepon</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="telp" class="form-control" placeholder="Telepon">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Handphone</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="hp" class="form-control" placeholder="Handphone">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="email" class="form-control" placeholder="Email">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <strong><u>A. DATA JENIS ALAT</u></strong>
                                <table>
                                    <tr>
                                        <td>Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_jenis" id="kd_jenis">
                                                  <option>-- Pilih Jenis --</option>
                                                  <?php foreach ($jenis->result() as $jenis): ?>
                                                    <option value="<?php echo $jenis->kd_jenis; ?>"><?php echo $jenis->nm_jenis; ?></option>
                                                  <?php endforeach ?>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subjenis" id="kd_subjenis" disabled>
                                                  <option>-- Pilih Sub Jenis --</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub-sub Jenis</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <select class="form-control" name="kd_subsubjenis" id="kd_subsubjenis" disabled>
                                              <option>-- Pilih Sub sub Jenis --</option>
                                            </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Merk</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="merk" class="form-control" placeholder="Merk">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>No. Seri</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="no_seri" class="form-control" placeholder="No. Seri">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kapasitas</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="kapasitas" class="form-control" placeholder="Kapasitas">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Stok Liter</td>
                                        <td>:</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" name="stok_liter" class="form-control" placeholder="Stok Liter">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <div class="form-group">
                                                <input type="submit" name="btnSubmit" class="btn btn-primary" value="Submit">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            </form>
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>
<script type="text/javascript">
      $("#kd_kategori").change(function(){
        var kd_kategori = $("#kd_kategori option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/sub_kategori')?>",
          type: "POST",
          data  : "kd_kategori="+kd_kategori,
          success : function (msg) {
            document.getElementById("kd_subkategori").disabled = false;
            $("#kd_subkategori").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_jenis").change(function(){
        var kd_jenis = $("#kd_jenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subjenis')?>",
          type: "POST",
          data  : "kd_jenis="+kd_jenis,
          success : function (msg) {
            document.getElementById("kd_subjenis").disabled = false;
            $("#kd_subjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
      $("#kd_subjenis").change(function(){
        var kd_subjenis = $("#kd_subjenis option:selected").val();
        $.ajax({
          url: "<?php echo base_url('backend/kd_subsubjenis')?>",
          type: "POST",
          data  : "kd_subjenis="+kd_subjenis,
          success : function (msg) {
            document.getElementById("kd_subsubjenis").disabled = false;
            $("#kd_subsubjenis").html(msg);
            //$("#kelurahan").css("color","black");
          }
        });
      });
    </script>
