<?php include 'template/header.php'; ?>

        <!-- TOP AREA -->
        <div class="bg-holder full">
          <div class="bg-mask"></div>
          <div class="bg-parallax" style="background-image:url(http://localhost/metrologi/assets/frontend/img/backgrounds/background.jpg);"></div>
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6"> 
                              <div class="loc-info text-left hidden-xs hidden-sm">
                                <img style="width: 50%; margin: 55px 0px 0px 75px;" src="<?php echo base_url('assets/frontend/img/img_front'); ?>/logo-tertib-tera.png">
                              </div>
                            </div>
                            <div class="col-md-6">
                                <div class="gap"></div>
                                <div class="gap"></div>
                                <div class="panel panel-default">
                                  <div class="panel-body">
                                    <form action="<?php echo base_url('login'); ?>" method="post">
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-user input-icon"></i>
                                            <label>Username</label>
                                            <input class="form-control" placeholder="Username" name="username" type="text" />
                                        </div>
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-lock input-icon"></i>
                                            <label>Password</label>
                                            <input class="form-control" placeholder="Password" name="password" type="password" />
                                        </div>
                                        <input class="btn btn-primary btn-lg" type="submit" name="btnSubmit" value="Login">
                                    </form>
                                    <ul id="finalResult"></ul>
                                  </div>
                                </div>
                            </div>
                            <div class="gap"></div>
                            <div class="gap"></div>
                            <div class="gap"></div>
                            <div class="gap"></div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->

<?php include 'template/footer.php'; ?>