<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                            <div class="gap"></div>
                            <div class="col-md-12">
                            	<h4>Regulasi</h4>
                                                <ul class="booking-list">
                                                    <li>
                                                        <div class="booking-item-container">
                                                            <div class="booking-item">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label>TAHUN 2012 NOMOR 1 SERI C PERATURAN DAERAH KOTA BOGOR NOMOR 4 TAHUN 2012</label> 
                                                                    </div> 
                                                                    <div class="col-md-4">
                                                                        <p>RETRIBUSI JASA UMUM</p>
                                                                    </div> 
                                                                    <div class="col-md-4" align="right">
                                                                        <a href="<?php echo base_url('assets/source'); ?>/PERDA__NO_4_TAHUN_2012_.pdf" class="btn btn-primary">View</a> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="booking-item-details">
                                                                <div class="row">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-container">
                                                            <div class="booking-item">
                                                                <div class="row">
                                                                    <div class="col-md-4">
                                                                        <label>UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 23 TAHUN 2014</label> 
                                                                    </div> 
                                                                    <div class="col-md-4">
                                                                        <p>PEMERINTAHAN DAERAH</p>
                                                                    </div> 
                                                                    <div class="col-md-4" align="right">
                                                                        <a href="<?php echo base_url('assets/source'); ?>/UU Nomor 23 Tahun 2014 (UU_23_2014_P) (1).pdf" class="btn btn-primary">View</a> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="booking-item-details">
                                                                <div class="row">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="booking-item-container">
                                                            <div class="booking-item">
                                                                <div class="row">
                                                                   <div class="col-md-4">
                                                                        <label>UNDANG-UNDANG REPUBLIK INDONESIA NOMOR 2 TAHUN 1981</label> 
                                                                    </div> 
                                                                    <div class="col-md-4">
                                                                        <p>METROLOGI LEGAL</p>
                                                                    </div> 
                                                                    <div class="col-md-4" align="right">
                                                                        <a href="<?php echo base_url('assets/source'); ?>/UU no.2_1981.pdf" class="btn btn-primary">View</a> 
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                            <div class="booking-item-details">
                                                                <div class="row">
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>