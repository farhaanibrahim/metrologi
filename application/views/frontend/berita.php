<?php include 'template/header.php'; ?>
<!-- TOP AREA -->
        <div class="bg-holder full">
                <div class="bg-content">
                    <div class="container">
                        <div class="row">
                        <div class="gap"></div>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                <a href="#" class="thumbnail">
                                    <img src="<?php echo base_url('assets/source'); ?>/berita.jpg" alt="">
                                </a>
                            </div>
                            <div class="col-md-9">
                                <h3>KOTA BOGOR URUS TERA DAN TERA ULANG UTTP</h3>
                                <label>Mar 27, 2017</label>
                                <p>
                                  mediabogor.com, Bogor – kewenangan dan pengawasan terkait penyelenggaraan metrologi legal tera dan tera ulang alat-alat ukur, takar, timbang dan perlengkapannya (UTTP) kini dilimpahkan dari Pemerintah Provinsi kepada Kabupaten/ Kota. Hal itu berdasarkan UU nomor 23 Tahun 2014 tentang Pemerintahan Daerah. ... <a href="<?php echo base_url('front/read_more'); ?>">read more</a>
                                </p>
                            </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        <!-- END TOP AREA  -->
<?php include 'template/footer.php'; ?>