-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2018 at 02:37 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_metrologi`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_kecamatan`
--

CREATE TABLE `mst_kecamatan` (
  `id_kecamatan` varchar(4) NOT NULL,
  `nm_kecamatan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_kecamatan`
--

INSERT INTO `mst_kecamatan` (`id_kecamatan`, `nm_kecamatan`) VALUES
('10', ' Bogor Selatan'),
('20', ' Bogor Timur'),
('30', ' Bogor Utara'),
('40', ' Bogor Tengah'),
('50', ' Bogor Barat'),
('60', ' Tanah Sereal');

-- --------------------------------------------------------

--
-- Table structure for table `tr_instansi`
--

CREATE TABLE `tr_instansi` (
  `id` int(1) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kepsek` varchar(100) NOT NULL,
  `nip_kepsek` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `email` varchar(40) NOT NULL,
  `tentang_singkat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_instansi`
--

INSERT INTO `tr_instansi` (`id`, `nama`, `alamat`, `kepsek`, `nip_kepsek`, `logo`, `no_telp`, `email`, `tentang_singkat`) VALUES
(1, 'Sistem Informasi Monitoring & Evaluasi BUMD Bank Pasar', 'Jalan Pengadilan No. 8A, Telp. (0251) 8322 001 ', 'Drs. Test Test, SH, MM', '19620706 198001 1 011', 'logo_bogor.png', '(0251) 8322 001 ', 'diswasbangkim@kotabogor.go.id', 'Sistem Informasi Monitoring & Evaluasi BUMD Bank Pasar adalah sebuah sistem yang dipergunakan untuk memonitoring dan mengevaluasi kinerja Bank Pasar.');

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis`
--

CREATE TABLE `t_jenis` (
  `kd_jenis` int(3) NOT NULL,
  `nm_jenis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jenis`
--

INSERT INTO `t_jenis` (`kd_jenis`, `nm_jenis`) VALUES
(1, 'TIMBANGAN'),
(2, 'BAHAN BAKAR MINYAK'),
(3, 'METERAN'),
(4, 'TABUNG'),
(5, 'TAKARAN');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori`
--

CREATE TABLE `t_kategori` (
  `kd_kategori` int(2) NOT NULL,
  `nm_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori`
--

INSERT INTO `t_kategori` (`kd_kategori`, `nm_kategori`) VALUES
(1, 'Pasar Rakyat'),
(2, 'Pasar Modern'),
(3, 'Ekspedisi'),
(4, 'Minimarket'),
(5, 'Toko'),
(6, 'PDAM'),
(7, 'SPBU'),
(8, 'PGN'),
(9, 'PLN'),
(12, 'Perorangan'),
(13, 'Kantor');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori_sub`
--

CREATE TABLE `t_kategori_sub` (
  `kd_subkategori` varchar(4) NOT NULL,
  `kd_kategori` varchar(2) NOT NULL,
  `nm_subkategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori_sub`
--

INSERT INTO `t_kategori_sub` (`kd_subkategori`, `kd_kategori`, `nm_subkategori`) VALUES
('11', '1', 'Pasar Gunung Batu'),
('12', '1', 'Pasar Kebon Kembang'),
('121', '12', '-'),
('13', '1', 'Pasar Bogor Baru'),
('131', '13', '-'),
('14', '1', 'Pasar Merdeka'),
('15', '1', 'Pasar Pada Suka'),
('16', '1', 'Pasar Sukasari'),
('17', '1', 'Pasar Induk Kemang'),
('18', '1', 'Pasar Jambu Dua'),
('21', '2', 'Hypermart'),
('210', '2', 'Ngesti'),
('211', '2', 'Ada Swalayan'),
('212', '2', 'Fresh Mart'),
('213', '2', 'Total'),
('214', '2', 'Superindo'),
('22', '2', 'Yogya'),
('23', '2', 'Junction'),
('24', '2', 'BTM'),
('25', '2', 'Ramayana'),
('26', '2', 'Robinson'),
('27', '2', 'Lottemart'),
('28', '2', 'Giant'),
('29', '2', 'Grand'),
('31', '3', 'JNE'),
('32', '3', 'Tiki'),
('41', '4', 'Indomart'),
('42', '4', 'Alfamart'),
('43', '4', 'Alfamidi'),
('44', '4', 'Ceria Mart'),
('45', '4', 'Prima'),
('51', '5', 'Apotik'),
('52', '5', 'Toko Mas'),
('53', '5', 'Toko Perak'),
('61', '6', 'Meteran Air PDAM'),
('62', '6', 'Meteran Listrik PLN'),
('63', '6', 'Meteran Gas PGN'),
('64', '6', 'Meteran SPBU'),
('71', '7', 'Meteran BBM'),
('81', '8', 'Tabung Gas LPG'),
('91', '9', 'Meteran Listrik');

-- --------------------------------------------------------

--
-- Table structure for table `t_pengguna`
--

CREATE TABLE `t_pengguna` (
  `kd_pengguna` varchar(12) NOT NULL,
  `id_kecamatan` varchar(2) NOT NULL,
  `kd_kategori` varchar(2) NOT NULL,
  `kd_subkategori` varchar(4) NOT NULL,
  `nm_pengguna` varchar(50) NOT NULL,
  `nm_usaha` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `rt` varchar(3) NOT NULL,
  `rw` varchar(3) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pengguna`
--

INSERT INTO `t_pengguna` (`kd_pengguna`, `id_kecamatan`, `kd_kategori`, `kd_subkategori`, `nm_pengguna`, `nm_usaha`, `alamat`, `rt`, `rw`, `telp`, `hp`, `email`) VALUES
('-- Pilih Kec', '--', '12', '121', 'TB SUMBER BANGUNAN', '', 'JL. SITU GEDE', '', '', '', '', ''),
('1010012121', '10', '12', '121', 'ALAMNAULI/BERAS', '', 'JL. BATU TULIS', '1', '8', '', '', ''),
('1010112121', '10', '12', '121', 'TB MEKARJAYA', '', 'JL. BATU TULIS', '3', '5', '', '', ''),
('101012121', '10', '12', '121', 'BU INDRAWATI', '', 'JL. CISADANE', '', '', '', '', ''),
('1010212121', '10', '12', '121', 'TB. VENTRIA', '', 'JL. BATU TULIS', '3', '3', '', '', ''),
('101112121', '10', '12', '121', 'BUB NINING', '', 'JL. CISADANE', '', '', '', '', ''),
('10112121', '10', '12', '121', 'RM Telor Barokah', '', 'Jl. Sadane', '', '', '', '', ''),
('101212121', '10', '12', '121', 'BPK. UJANG', '', 'JL. CISADANE', '', '', '', '', ''),
('101312121', '10', '12', '121', 'BP MAMA', '', 'JL. CISADANE', '', '', '', '', ''),
('101412121', '10', '12', '121', 'BU IMAS', '', 'JL. CISADANE', '', '', '', '', ''),
('101512121', '10', '12', '121', 'BU EWONG', '', 'JL. CISADANE', '', '', '', '', ''),
('101612121', '10', '12', '121', 'USMAN TK DAGING', '', 'JL. CISADANE', '', '', '', '', ''),
('101712121', '10', '12', '121', 'KULSUM TK TELOR', '', 'JL. CISADANE', '', '', '', '', ''),
('101812121', '10', '12', '121', 'ARIS', '', 'JL. CISADANE', '', '', '', '', ''),
('101912121', '10', '12', '121', 'M. ALI TELOR BERKAH', '', 'JL. CISADANE', '', '', '', '', ''),
('102012121', '10', '12', '121', 'BU MIMI- SEMBAKO', '', 'JL. CISADANE', '', '', '', '', ''),
('102112121', '10', '12', '121', 'KOSASIH-DAGING', '', 'JL. CISADANE', '', '', '', '', ''),
('10212121', '10', '12', '121', 'Bpk Ade', '', 'Jl. Cisadane', '', '', '', '', ''),
('102212121', '10', '12', '121', 'BU TITIN', '', 'JL. CISADANE', '', '', '', '', ''),
('102312121', '10', '12', '121', 'BPK AIP', '', 'JL. CISADANE', '', '', '', '', ''),
('102412121', '10', '12', '121', 'BU MITA', '', 'JL. CISADANE', '', '', '', '', ''),
('102512121', '10', '12', '121', 'BP IBRAHIM/BUAH', '', 'JL. CISADANE', '', '', '', '', ''),
('102612121', '10', '12', '121', 'H. AMUNG/SEMBAKO', '', 'JL. CISADANE', '', '', '', '', ''),
('102712121', '10', '12', '121', 'PERCETAKAN KERTAS MATAHARI', '', 'KEL. EMPANG', '', '', '', '', ''),
('102812121', '10', '12', '121', 'PERCETAKAN', '', 'JL. EMPANG', '', '', '', '', ''),
('102912121', '10', '12', '121', 'LUBIS', '', 'CIPINANG GADING', '3', '4', '', '', ''),
('103012121', '10', '12', '121', 'TITI', '', 'CIPINANG RANGGA', '3', '4', '', '', ''),
('103112121', '10', '12', '121', 'ALI/SBK', '', 'CIPINANG MEKAR', '3', '4', '', '', ''),
('10312121', '10', '12', '121', 'Toko Pelangi', '', 'Jl. Layung Sari', '', '', '', '', ''),
('103212121', '10', '12', '121', 'IKAH/PLASTIK', '', 'CIPINANG RANGGA MEKAR', '3', '4', '', '', ''),
('103312121', '10', '12', '121', 'TOKO LIZA/SBK', '', 'CIPINANG RANGGA MEKAR', '3', '4', '', '', ''),
('103412121', '10', '12', '121', 'IBU NANI', '', 'CIPINANG RANGGA MEKAR', '3', '4', '', '', ''),
('103512121', '10', '12', '121', 'RAJAB', '', 'CIPINANG RANGGA MEKAR', '3', '4', '', '', ''),
('103612121', '10', '12', '121', 'CUCUM', '', 'CIPINANG RANGGA MEKAR', '2', '13', '', '', ''),
('103712121', '10', '12', '121', 'TOKO BERAS/AANG', '', 'SINARGALIH/CIPINANG GADING', '1', '3', '', '', ''),
('103812121', '10', '12', '121', 'TOKO TELOR/FIRMAN', '', 'CIPINANG GADING RANGGA MEKAR', '1', '3', '', '', ''),
('103912121', '10', '12', '121', 'IBU MAYA', '', 'CIPINANG GADING RANGGA MEKAR', '1', '3', '', '', ''),
('104012121', '10', '12', '121', 'H. YAKUB/SBK', '', 'CIPINANG GAIDNG RANGGA MEKAR', '1', '3', '', '', ''),
('104112121', '10', '12', '121', 'ACANG/SBK', '', 'CIPINANG GADING RANGGA GADING', '1', '3', '', '', ''),
('10412121', '10', '12', '121', 'P sukirno', '', 'JL. LAYUNG SARI', '', '', '', '', ''),
('104212121', '10', '12', '121', 'AHMAD/SAYUR', '', 'CIPINANG GADING RANGGA MEKAR', '1', '3', '', '', ''),
('104312121', '10', '12', '121', 'ANSARI/SBK', '', 'CIPINANG GADING', '1', '12', '', '', ''),
('104412121', '10', '12', '121', 'ARIF/SBK', '', 'CIPINANG GADUNG RANGGA MEKAR', '1', '12', '', '', ''),
('104512121', '10', '12', '121', 'YUSUF/SBK', '', 'CIPINANG GADING RANGGA MEKAR', '4', '7', '', '', ''),
('104612121', '10', '12', '121', 'PAKAN AYAM', '', 'CIPINANG GADING RANGGA MEKAR', '4', '7', '', '', ''),
('104712121', '10', '12', '121', 'ROSIDA/SBK', '', 'CIPINANG GADING RANGGA MEKAR', '3', '7', '', '', ''),
('104812121', '10', '12', '121', 'H. USMAN', '', 'CIPINANG GADINGRANGGA MEKAR', '3', '7', '', '', ''),
('104912121', '10', '12', '121', 'MUSIKAH/SBK', '', 'JL. PAMOYANAN RANGGA MEKAR', '4', '1', '', '', ''),
('105012121', '10', '12', '121', 'NANO/SBK', '', 'JL. PAMOYANAN/SBK', '2', '1', '', '', ''),
('105112121', '10', '12', '121', 'NANO/SAYUR', '', 'JL. PAMOYANAN RANGGA MEKAR', '2', '1', '', '', ''),
('10512121', '10', '12', '121', 'TOKO DANI', '', 'JL. LOLONGOK', '', '', '', '', ''),
('105212121', '10', '12', '121', 'M. FAHRUDIN/SBK', '', 'JL. PAMOYANAN RANGGA MEKAR', '2', '1', '', '', ''),
('105312121', '10', '12', '121', 'TOKO BERAS', '', 'JL. RAYA EMPANG', '', '', '', '', ''),
('105412121', '10', '12', '121', 'TOKO AQUARIUM EMPANG', '', 'JL. RAYA EMPANG', '', '', '', '', ''),
('105512121', '10', '12', '121', 'TOKO MULYAJAYA/SBK', '', 'JL. RAYA WANGUN SINDANGSARI', '', '', '', '', ''),
('105612121', '10', '12', '121', 'BU ARI', '', 'KEL MUARA SARI', '2', '2', '', '', ''),
('105712121', '10', '12', '121', 'IBU UCI/SBK', '', 'JL. MUARA SARI', '1', '6', '', '', ''),
('105812121', '10', '12', '121', 'NATUSION/SBK', '', 'KEL. MUARA SARI', '2', '2', '', '', ''),
('105912121', '10', '12', '121', 'P. FENDI/SBK', '', 'KEL. MUARA SARI', '1', '2', '', '', ''),
('106012121', '10', '12', '121', 'FIKRI/SBK', '', 'KEL. MUARA SARI', '1', '2', '', '', ''),
('106112121', '10', '12', '121', 'KESIH/SBK', '', 'KEL. MUARA SARI', '3', '2', '', '', ''),
('10612121', '10', '12', '121', 'BU DEWI', '', 'JL. CISADANE', '', '', '', '', ''),
('106212121', '10', '12', '121', 'ROSIDAH/SBK', '', 'KEL. MUARA SARI', '1', '6', '', '', ''),
('106312121', '10', '12', '121', 'SITI JULIAH/SBK', '', 'KEL. MUARA SARI', '2', '2', '', '', ''),
('106412121', '10', '12', '121', 'BU MAR/SBK', '', 'KEL. MUARA SARI', '5', '6', '', '', ''),
('106512121', '10', '12', '121', 'BU SUPRIYADI', '', 'KEL MUARA SARI', '3', '6', '', '', ''),
('106612121', '10', '12', '121', 'IVAN/SBK', '', 'KEL. MUARA SARI', '3', '4', '', '', ''),
('106712121', '10', '12', '121', 'H. AYI', '', 'KEL MUARA SARI', '5', '3', '', '', ''),
('106812121', '10', '12', '121', 'AHYAR/SBK', '', 'KEL.. MUARA SARI', '2', '4', '', '', ''),
('106912121', '10', '12', '121', 'P.ADI.SBK', '', 'KEL. MUARA SARI', '2', '4', '', '', ''),
('107012121', '10', '12', '121', 'KURNIAJAYA/TB', '', 'JL. RAYA TAJUR', '', '', '', '', ''),
('107112121', '10', '12', '121', 'P. NASKAR/BUAH', '', 'JL. R. BATU TULIS', '2', '7', '', '', ''),
('10712121', '10', '12', '121', 'BU UTIN', '', 'JL. CISADANE', '', '', '', '', ''),
('107212121', '10', '12', '121', 'KAMAL/BUAH', '', 'JL. R. BATU TULIS', '2', '7', '', '', ''),
('107312121', '10', '12', '121', 'LINDA/BUAH', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('107412121', '10', '12', '121', 'H. AHMAD/SBK', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('107512121', '10', '12', '121', 'JAHRA/SBK', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('107612121', '10', '12', '121', 'NAFA/SBK', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('107712121', '10', '12', '121', 'RAS/PLASTIK', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('107812121', '10', '12', '121', 'GLORI/AQUARIUM', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('107912121', '10', '12', '121', 'TK SIMPATI/SBK', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('108012121', '10', '12', '121', 'HERI/TELOR', '', 'JL. DRS. SALEH', '2', '7', '', '', ''),
('108112121', '10', '12', '121', 'NANDIN/SBK', '', 'JL. RAYA CIPAKU', '2', '1', '', '', ''),
('10812121', '10', '12', '121', 'BU INTAN', '', 'JL. CISADANE', '', '', '', '', ''),
('108212121', '10', '12', '121', 'RANGKUTI/ZA', '', 'JL. RAYA CIPAKU', '3', '1', '', '', ''),
('108312121', '10', '12', '121', 'EMI/SBK', '', 'JL. RAYA CIPAKU', '2', '10', '', '', ''),
('108412121', '10', '12', '121', 'P.HAJI/SBK', '', 'PERUM 2 NO. 2', '2', '2', '', '', ''),
('108512121', '10', '12', '121', 'TB. ADAM JAYA', '', 'JL. RAYA CIPAKU', '2', '3', '', '', ''),
('108612121', '10', '12', '121', 'BG LUBIS 1/SBK', '', 'JL. RAYA CIPAKU', '3', '3', '', '', ''),
('108712121', '10', '12', '121', 'MULYADI/SBK', '', 'JL. RAYA CIPAKU', '3', '3', '', '', ''),
('108812121', '10', '12', '121', 'TK PUTRI/SBK', '', 'JL. RAYA CIPAKU', '3', '12', '', '', ''),
('108912121', '10', '12', '121', 'TK NASUTION/SBK', '', 'JL. RAYA CIPAKU SUKAASIH', '4', '12', '', '', ''),
('109012121', '10', '12', '121', 'TK PRIMA AIDIL/SBK', '', 'JL. RAYA CIPAKU SUKAASIH', '4', '12', '', '', ''),
('109112121', '10', '12', '121', 'TK GUNUNGJATI/KUE', '', 'JL. RAYA CIPAKU SUKAASIH', '4', '12', '', '', ''),
('10912121', '10', '12', '121', 'BU SANTI', '', 'JL. CISADANE', '', '', '', '', ''),
('109212121', '10', '12', '121', 'KHOTIMA/SBK', '', 'JL. RAYA CIPAKU SUKAASIH', '1', '4', '', '', ''),
('109312121', '10', '12', '121', 'TOTONG/SBK', '', 'JL. RAYA CIPAKU SUKAASIH', '1', '3', '', '', ''),
('109412121', '10', '12', '121', 'TK ANUGERAH MAKMUR', '', 'JL. RAYA CIPAKU', '3', '1', '', '', ''),
('109512121', '10', '12', '121', 'GALERI PLASTIK', '', 'JL. RAYA CIPAKU', '3', '1', '', '', ''),
('109612121', '10', '12', '121', 'JULI/SBK', '', 'KEL. BATU TULIS', '1', '7', '', '', ''),
('109712121', '10', '12', '121', 'YUSRA.H/SBK', '', 'KEL. BATU TULIS', '1', '7', '', '', ''),
('109812121', '10', '12', '121', 'YULI/SBK', '', 'KEL. BATU TULIS', '3', '10', '', '', ''),
('109912121', '10', '12', '121', 'IYUS/SBK', '', 'KEL. BATU TULIS', '2', '10', '', '', ''),
('2010012121', '20', '12', '121', 'TK VITA', '', 'JL. BABADAK', '', '', '', '', ''),
('2010112121', '20', '12', '121', 'TK YUNI', '', 'JL. BABADAK', '', '', '', '', ''),
('201012121', '20', '12', '121', 'TK IDOLA', '', 'BTR KEMANG', '', '', '', '', ''),
('20102111', '20', '1', '11', 'TK IBU SUMINIH', '', 'JL. BABADAK', '', '', '', '', ''),
('2010312121', '20', '12', '121', 'TK SUSI', '', 'JL. BABADAK', '', '', '', '', ''),
('2010412121', '20', '12', '121', 'TK LUBIS', '', 'JL. BABADAK', '', '', '', '', ''),
('2010512121', '20', '12', '121', 'WR BP MAIL', '', 'JL. BABADAK', '', '', '', '', ''),
('2010612121', '20', '12', '121', 'TK 10 A', '', 'JL. BABADAK', '', '', '', '', ''),
('2010712121', '20', '12', '121', 'WR SAYUR BU ELAH/RUDI', '', 'JL. BABADAK', '', '', '', '', ''),
('2010812121', '20', '12', '121', 'TK ATEP', '', 'KP MUARA', '', '', '', '', ''),
('2010912121', '20', '12', '121', 'WR KEMBAR', '', 'MUARA', '1', '8', '', '', ''),
('2011012121', '20', '12', '121', 'TK TATANG', '', 'KP MUARA', '', '', '', '', ''),
('2011112121', '20', '12', '121', 'TK ASRAP', '', 'KP MUARA', '', '', '', '', ''),
('201112121', '20', '12', '121', 'TK YUNUS', '', 'BTR KEMANG', '', '', '', '', ''),
('20112121', '20', '12', '121', 'BP IMBALO', '', 'BTR KEMANG', '', '', '', '', ''),
('2011212121', '20', '12', '121', 'TK REHAN', '', 'KP MUARA', '', '', '', '', ''),
('2011312121', '20', '12', '121', 'TK BANG RIZKI', '', 'KP MUARA', '', '', '', '', ''),
('2011412121', '20', '12', '121', 'TK DINA', '', 'KP MUARA', '', '', '', '', ''),
('2011512121', '20', '12', '121', 'TK BU ISMI', '', 'KP MUARA', '', '', '', '', ''),
('2011612121', '20', '12', '121', 'TK JUJUN', '', 'KP MUARA', '', '', '', '', ''),
('2011712121', '20', '12', '121', 'WR ENDANG', '', 'KP MUARA', '', '', '', '', ''),
('2011812121', '20', '12', '121', 'WR IBU OKA', '', 'KP MUARA', '', '', '', '', ''),
('2011912121', '20', '12', '121', 'WR BU NENENG', '', 'KP MUARA', '', '', '', '', ''),
('2012012121', '20', '12', '121', 'TK RIZKY', '', 'KP MUARA', '', '', '', '', ''),
('2012112121', '20', '12', '121', 'TK DAFA', '', 'KP MUARA', '', '', '', '', ''),
('201212121', '20', '12', '121', 'TK LILIH', '', 'PERUMNAS BTR KEMANG', '', '', '', '', ''),
('2012212121', '20', '12', '121', 'TK SULAIMAN', '', 'KP MUARA', '', '', '', '', ''),
('2012312121', '20', '12', '121', 'TK IBU DANAH', '', 'KP MUARA', '', '', '', '', ''),
('2012412121', '20', '12', '121', 'TK PAK AKIM', '', 'KP MUARA', '', '', '', '', ''),
('2012512121', '20', '12', '121', 'WR BU YULI', '', 'KP MUARA', '', '', '', '', ''),
('2012612121', '20', '12', '121', 'TK GOMEZ', '', 'KP MUARA', '', '', '', '', ''),
('2012712121', '20', '12', '121', 'TK IIS', '', 'JL. BABADAK', '', '', '', '', ''),
('2012812121', '20', '12', '121', 'TK YENI', '', 'JL. BABADAK', '', '', '', '', ''),
('2012912121', '20', '12', '121', 'TK IIS', '', 'JL. BABADAK', '', '', '', '', ''),
('2013012121', '20', '12', '121', 'TK SAMSUL', '', 'KP MUARA', '', '', '', '', ''),
('2013112121', '20', '12', '121', 'TK ABDURAHMAN', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('201312121', '20', '12', '121', 'DUA SODARA', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2013212121', '20', '12', '121', 'TK AISYAH', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013312121', '20', '12', '121', 'TK RIZKY', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013412121', '20', '12', '121', 'TK NUSASARI', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013512121', '20', '12', '121', 'TK IBU PIPIH', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013612121', '20', '12', '121', 'WR IBU MUMUN', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013712121', '20', '12', '121', 'TK PAK MAMAT', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013812121', '20', '12', '121', 'TK IBU TETI', '', 'PD HASAN SINDANGRASA', '', '', '', '', ''),
('2013912121', '20', '12', '121', 'TK PAK RUDI', '', 'SINDANGSARI', '', '', '', '', ''),
('2014012121', '20', '12', '121', 'TK BU RATNA', '', 'SINDANGSARI', '', '', '', '', ''),
('2014112121', '20', '12', '121', 'TK BAPA ALI', '', 'SINDANGSARI', '', '', '', '', ''),
('201412121', '20', '12', '121', 'TB SINAR SURYA', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2014212121', '20', '12', '121', 'TB SUBUR JAYA', '', 'JL RAYA CIAWI', '', '', '', '', ''),
('2014312121', '20', '12', '121', 'TK SUKASARI', '', 'JL. RAYA CIAWI', '', '', '', '', ''),
('2014412121', '20', '12', '121', 'TK IBU EKA', '', 'JL RAYA CIAWI', '', '', '', '', ''),
('2014512121', '20', '12', '121', 'TK HELMI', '', 'JL WANGUN', '', '', '', '', ''),
('2014612121', '20', '12', '121', 'TK PITRA', '', 'WANGUN', '', '', '', '', ''),
('2014712121', '20', '12', '121', 'TK IIS', '', 'WANGUN', '', '', '', '', ''),
('2014812121', '20', '12', '121', 'TK ARI', '', 'WANGUN', '', '', '', '', ''),
('2014912121', '20', '12', '121', 'TK DEMAMOTEKAR', '', 'WANGUN', '', '', '', '', ''),
('2015012121', '20', '12', '121', 'TK HAMID', '', 'WANGUN', '', '', '', '', ''),
('2015112121', '20', '12', '121', 'TK PA BABAN', '', 'WANGUN CIBALOK', '', '', '', '', ''),
('201512121', '20', '12', '121', 'TK ROJAK', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2015212121', '20', '12', '121', 'TK IBU NINA', '', 'WANGUN CIBALOK', '', '', '', '', ''),
('2015312121', '20', '12', '121', 'TK BU RUS', '', 'WANGUN CIBALOK', '', '', '', '', ''),
('2015412121', '20', '12', '121', 'TK KURNIA', '', 'WANGUN TENGAH', '', '', '', '', ''),
('2015512121', '20', '12', '121', 'TK PAK NANANG', '', 'WANGUN ATAS', '', '', '', '', ''),
('2015612121', '20', '12', '121', 'TK ARIF', '', 'WANGUN', '', '', '', '', ''),
('2015712121', '20', '12', '121', 'TK SUNDA JAYA', '', 'JL RAYA CIAWI', '', '', '', '', ''),
('2015812121', '20', '12', '121', 'WR SAYUR IBU IDA', '', 'BABADAK', '', '', '', '', ''),
('2015912121', '20', '12', '121', 'TK DS', '', 'JL. KATULAMPA', '', '', '', '', ''),
('2016012121', '20', '12', '121', 'TK BERKAH LUBIS', '', 'JL. KATULAMPA', '', '', '', '', ''),
('2016112121', '20', '12', '121', 'TK ARIS', '', 'JL. KATULAMPA', '', '', '', '', ''),
('201612121', '20', '12', '121', 'BP DADAN', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2016212121', '20', '12', '121', 'TK MUTIARA JAYA', '', 'JL. KATULAMPA', '', '', '', '', ''),
('2016312121', '20', '12', '121', 'TK PANCING ANDRI', '', 'JL. KATULAMPA', '', '', '', '', ''),
('2016412121', '20', '12', '121', 'TK KUE', '', 'PERUMNAS BTR KEMANG', '', '', '', '', ''),
('2016512121', '20', '12', '121', 'WR SAYURAN', '', 'PERUMNAS BTR KEMANG', '', '', '', '', ''),
('2016612121', '20', '12', '121', 'TK H JALIL', '', 'BTR KEMANG', '', '', '', '', ''),
('2016712121', '20', '12', '121', 'TK BP KARTA', '', 'BTR KEMANG', '', '', '', '', ''),
('2016812121', '20', '12', '121', 'TK IBU MAMAH', '', 'BTR KEMANG', '', '', '', '', ''),
('2016912121', '20', '12', '121', 'TK IDOLA', '', 'BTR KEMANG', '', '', '', '', ''),
('2017012121', '20', '12', '121', 'TK LIA', '', 'BTR KEMANG', '', '', '', '', ''),
('2017112121', '20', '12', '121', 'TK PUTRA LANGKAT II', '', 'BTR KEMANG', '', '', '', '', ''),
('201712121', '20', '12', '121', 'TK LATIF', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2017212121', '20', '12', '121', 'TK H IBU ICAH', '', 'BABAKAN PERUMNAS', '', '', '', '', ''),
('2017312121', '20', '12', '121', 'TK PAK SUJADI', '', 'BABAKAN PERUMNAS', '', '', '', '', ''),
('2017412121', '20', '12', '121', 'TK AFIQAH', '', 'BABAKAN PERUMNAS', '', '', '', '', ''),
('2017512121', '20', '12', '121', 'TK BU SULIS', '', 'BABAKAN PERUMNAS', '', '', '', '', ''),
('2017612121', '20', '12', '121', 'TK SAYUR BEER', '', 'BTR KEMANG', '', '', '', '', ''),
('2017712121', '20', '12', '121', 'TK IBU NOVI', '', 'BTR KEMANG', '', '', '', '', ''),
('2017812121', '20', '12', '121', 'TK PAK ENING', '', 'BTR KEMANG', '', '', '', '', ''),
('2017912121', '20', '12', '121', 'TK AHYAR', '', 'BTR KEMANG', '', '', '', '', ''),
('2018012121', '20', '12', '121', 'TK IBU LILIS', '', 'BTR KEMANG', '', '', '', '', ''),
('2018112121', '20', '12', '121', 'TK BAPAK JAJANG', '', 'BTR KEMANG', '', '', '', '', ''),
('201812121', '20', '12', '121', 'TK BN', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2018212121', '20', '12', '121', 'TK PAK ADE', '', 'BTR KEMANG', '', '', '', '', ''),
('2018312121', '20', '12', '121', 'TK CIAMIS', '', 'BTR KEMANG', '', '', '', '', ''),
('2018412121', '20', '12', '121', 'TK SAYUR RUMAISA', '', 'BTR KEMANG', '', '', '', '', ''),
('2018512121', '20', '12', '121', 'TK NINING', '', 'BTR KEMANG', '', '', '', '', ''),
('2018612121', '20', '12', '121', 'TK RANGKUTI', '', '', '', '', '', '', ''),
('2018712121', '20', '12', '121', 'TK PECOK', '', 'BTR KEMANG', '', '', '', '', ''),
('2018812121', '20', '12', '121', 'TK IBU H. NENAH', '', 'BTR KEMANG', '', '', '', '', ''),
('2018912121', '20', '12', '121', 'TK PAK OJI', '', 'BTR KEMANG', '', '', '', '', ''),
('2019012121', '20', '12', '121', 'TK ARIS', '', 'KP. TANGKIL', '', '', '', '', ''),
('2019112121', '20', '12', '121', 'TK SYIFA', '', 'KP. TANGKIL', '', '', '', '', ''),
('201912121', '20', '12', '121', 'TK ARI SAFARI', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2019212121', '20', '12', '121', 'WR IBU RAGIL', 'KP. SAWAH GG MASJID', '', '', '', '', '', ''),
('2019312121', '20', '12', '121', 'TK LUBIS', '', 'KP. SAWAH', '', '', '', '', ''),
('2019412121', '20', '12', '121', 'TK REMPEYEK SUPERTIN', '', 'KP. TANGKIL', '', '', '', '', ''),
('2019512121', '20', '12', '121', 'WR ADENG', '', 'KP. CIKONDANG', '', '', '', '', ''),
('2019612121', '20', '12', '121', 'WR BU ANIS', '', 'KP. CIKONDANG', '', '', '', '', ''),
('2019712121', '20', '12', '121', 'TK UCU', '', 'PAJAJARAN/PUARMIN', '', '', '', '', ''),
('2019812121', '20', '12', '121', 'DAUR ULANG/TAJUDIN', '', 'PAJAJARAN/PUARMIN', '', '', '', '', ''),
('2019912121', '20', '12', '121', 'WR H YANTO', '', '', '', '', '', '', ''),
('2020012121', '20', '12', '121', 'WR YAYAT', '', 'PAJAJARAN/PUARMIN', '', '', '', '', ''),
('2020112121', '20', '12', '121', 'WR PAK DAYAT', '', 'PAJAJARAN/PUARMIN', '', '', '', '', ''),
('202012121', '20', '12', '121', 'TK BORNEO', '', 'PARUNG BANTENG', '', '', '', '', ''),
('2020212121', '20', '12', '121', 'WR UMI', '', 'CIBURIAL', '', '', '', '', ''),
('202112121', '20', '12', '121', 'TK BUAH JAMALUDIN', '', 'PARUNG BANTENG', '', '', '', '', ''),
('20212121', '20', '12', '121', 'MARTIBUN', '', 'BTR KEMANG', '', '', '', '', ''),
('202212121', '20', '12', '121', 'TK KURNIA AGUNG', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202312121', '20', '12', '121', 'TOKO PAKAN BURUNG ANDRI', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202412121', '20', '12', '121', 'TK LUBIS', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202512121', '20', '12', '121', 'TK NASUTION', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202612121', '20', '12', '121', 'TK DARWIS', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202712121', '20', '12', '121', 'TK MURAH JAYA', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202812121', '20', '12', '121', 'TK MITRA TANI', '', 'PARUNG BANTENG', '', '', '', '', ''),
('202912121', '20', '12', '121', 'TOOKO AYAM POTONG', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203012121', '20', '12', '121', 'TOKO BUAH LATIF', '', 'PRG BANTENG', '', '', '', '', ''),
('203112121', '20', '12', '121', 'TK RADITIA', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('20312121', '20', '12', '121', 'AMANDA', '', 'BTR KEMANG', '', '', '', '', ''),
('203212121', '20', '12', '121', 'TK PAIZAL', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203312121', '20', '12', '121', 'TK ILHAM', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203412121', '20', '12', '121', 'TK H DIDIN', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203512121', '20', '12', '121', 'TK SALMAN', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203612121', '20', '12', '121', 'TK MURAH JAYA', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203712121', '20', '12', '121', 'TK BUYA TANI', '', 'JL. PANGGULAAN WATES', '', '', '', '', ''),
('203812121', '20', '12', '121', 'TK ENTIB', '', 'JL. PARUNG BANTENG', '', '', '', '', ''),
('203912121', '20', '12', '121', 'TK ERLANGGA', '', 'JL PARUNG BANTENG', '', '', '', '', ''),
('204012121', '20', '12', '121', 'IBU UCU', '', 'JL PARUNG BANTENG', '', '', '', '', ''),
('204112121', '20', '12', '121', 'TK UJANG', '', 'JL. PARUNG BANTENG', '', '', '', '', ''),
('20412121', '20', '12', '121', 'TOKO BANGUNAN INDRAJAYA', '', 'BTR KEMANG', '', '', '', '', ''),
('204212121', '20', '12', '121', 'TOKO ABDURAHMAN', '', 'JL PARUNG BANTENG', '', '', '', '', ''),
('204312121', '20', '12', '121', 'TK KHOERUDIN', '', 'JL. PARUNG BANTENG', '', '', '', '', ''),
('204412121', '20', '12', '121', 'BP ATIN', '', 'JL. PARUNG BANTENG', '', '', '', '', ''),
('204512121', '20', '12', '121', 'TK H FIRDAUS', '', 'JL PARUNG BANTENG', '', '', '', '', ''),
('204612121', '20', '12', '121', 'TK IBU LINDA', '', 'JL PARUNG BANTENG', '', '', '', '', ''),
('204712121', '20', '12', '121', 'TK HENDI/ KUE KERING', '', 'JL. PARUNG BANTENG', '', '', '', '', ''),
('204812121', '20', '12', '121', 'TK H ISAK', '', 'JL. KATULAMPA', '', '', '', '', ''),
('204912121', '20', '12', '121', 'TK H BABAN', '', 'JL KATULAMPA', '', '', '', '', ''),
('205012121', '20', '12', '121', 'TK ISMET', '', 'JL KATULAMPA', '', '', '', '', ''),
('205112121', '20', '12', '121', 'TK IYANG', '', 'JL KATULAMPA', '', '', '', '', ''),
('20512121', '20', '12', '121', 'JAMALUDIN', '', 'BTR KEMANG', '', '', '', '', ''),
('205212121', '20', '12', '121', 'TK UTAMA PAKAN', '', 'JL KATULAMPA', '', '', '', '', ''),
('205312121', '20', '12', '121', 'TK BAROKAH', '', 'JL KATULAMPA', '', '', '', '', ''),
('205412121', '20', '12', '121', 'TK ADE', '', 'JL KATULAMPA', '', '', '', '', ''),
('205512121', '20', '12', '121', 'TK BU NENENG', '', 'JL KATULAMPA', '', '', '', '', ''),
('205612121', '20', '12', '121', 'TK NABIL', '', 'JL KATULAMPA', '', '', '', '', ''),
('205712121', '20', '12', '121', 'TK PAK MIN', '', 'JL KATULAMPA', '', '', '', '', ''),
('205812121', '20', '12', '121', 'TK IBU NUR', '', 'JL KATULAMPA', '', '', '', '', ''),
('205912121', '20', '12', '121', 'TK RIZKI', '', 'JL KATULAMPA', '', '', '', '', ''),
('206012121', '20', '12', '121', 'TK NIRWANA', '', '', '', '', '', '', ''),
('206112121', '20', '12', '121', 'TK BU GATOT', '', 'JL KATULAMPA', '', '', '', '', ''),
('20612121', '20', '12', '121', 'TOKO MUDAH REZEKI', '', 'PARUNG BANTENG', '', '', '', '', ''),
('206212121', '20', '12', '121', 'TK PADJEH', '', 'JL KATULAMPA', '', '', '', '', ''),
('206312121', '20', '12', '121', 'TK RIAN', '', 'JL KATULAMPA', '', '', '', '', ''),
('206412121', '20', '12', '121', 'TK BU ANI', '', 'JL KATULAMPA', '', '', '', '', ''),
('206512121', '20', '12', '121', 'TK DIVA', '', 'JL TAMAN PAJAJARAN', '', '', '', '', ''),
('206613131', '20', '13', '131', 'TK ATIK', '', 'KP TANGKIL', '', '', '', '', ''),
('206713131', '20', '13', '131', 'TK HASAN', '', 'KP PASIR', '', '', '', '', ''),
('206812121', '20', '12', '121', 'TK RUSDI', '', 'KP PASIR', '', '', '', '', ''),
('206912121', '20', '12', '121', 'TK IBU ICAH', '', 'KP PASIR', '', '', '', '', ''),
('207012121', '20', '12', '121', 'TK YAYANG', '', 'KP SAWAH', '', '', '', '', ''),
('207112121', '20', '12', '121', 'TK ANDI', '', 'KP SAWAH', '', '', '', '', ''),
('20712121', '20', '12', '121', 'TB SEDERHANA', '', 'BTR KEMANG', '', '', '', '', ''),
('207212121', '20', '12', '121', 'TK BU CIPTO', '', 'KP SAWAH', '', '', '', '', ''),
('207312121', '20', '12', '121', 'TK PAKAN BURUNG', '', 'CIHEULEUT- JL PADI', '', '', '', '', ''),
('207412121', '20', '12', '121', 'TK IBU SULASIH', '', 'CIHEULEUT- JL PADI', '', '', '', '', ''),
('207512121', '20', '12', '121', 'TOKO ROBI', '', 'CIHEULEUT-JL PADI', '', '', '', '', ''),
('207612121', '20', '12', '121', 'TK IBU YULI', '', 'CIHEULEUT', '', '', '', '', ''),
('207712121', '20', '12', '121', 'TK OPUNG', '', 'CIHEULEUT', '', '', '', '', ''),
('207812121', '20', '12', '121', 'TK ELI', '', 'CIHEULEUT', '', '', '', '', ''),
('207912121', '20', '12', '121', 'TK LUBIS', '', 'BTR KEMANG', '', '', '', '', ''),
('208012121', '20', '12', '121', 'TK MARRIMBUN', '', 'BTR KEMANG', '', '', '', '', ''),
('208113131', '20', '13', '131', 'TK FARHAN', '', 'BTR KEMANG', '', '', '', '', ''),
('20812121', '20', '12', '121', 'TOKO HALIM', '', 'TMN PAJAJARAN', '', '', '', '', ''),
('208212121', '20', '12', '121', 'TK IBU YAYAN', '', 'JL SUKASARI', '', '', '', '', ''),
('208312121', '20', '12', '121', 'TK HERI', '', 'JL SUKASARI', '', '', '', '', ''),
('208412121', '20', '12', '121', 'TK BU DIAN', '', 'BANTAR PEUTEUY TAJUR', '', '', '', '', ''),
('208512121', '20', '12', '121', 'TK IBU TINAH', '', 'BANTAR PEUTEUY TAJUR', '', '', '', '', ''),
('208612121', '20', '12', '121', 'TK BANG IKHSAN', '', 'BANTAR PEUTEUY TAJUR', '', '', '', '', ''),
('208712121', '20', '12', '121', 'TK ABAS', '', 'BANTAR PEUTEUY TAJUR', '', '', '', '', ''),
('208812121', '20', '12', '121', 'TK RAHMAT', '', 'BANTAR PEUTEUY TAJUR', '', '', '', '', ''),
('208912121', '20', '12', '121', 'TK ENDIN', '', 'BANTAR PEUTEUY TAJUR', '', '', '', '', ''),
('209012121', '20', '12', '121', 'TK IYOS', '', 'GG LURAH', '', '', '', '', ''),
('209112121', '20', '12', '121', 'TK INAZA', '', 'GG TANUWIJAYA', '', '', '', '', ''),
('20912121', '20', '12', '121', 'TK MEKARJAYA ABADI', '', 'BTR KEMANG', '', '', '', '', ''),
('209212121', '20', '12', '121', 'TK RENDI', '', 'GG TANUWIJAYA', '', '', '', '', ''),
('209312121', '20', '12', '121', 'TK ELPI', '', 'TANUWIJAYA', '', '', '', '', ''),
('209412121', '20', '12', '121', 'TK KARSA', '', 'JL TAJUR', '', '', '', '', ''),
('209512121', '20', '12', '121', 'TK ENDANG', '', 'SUKAJAYA', '', '', '', '', ''),
('209612121', '20', '12', '121', ' TK PAK BOY', '', 'JL. BABADAK', '', '', '', '', ''),
('209712121', '20', '12', '121', 'TK KUE MUGI BERKAH', '', 'JL. BABADAK', '', '', '', '', ''),
('209812121', '20', '12', '121', 'WR BANG ALI', '', 'JL. BABADAK', '', '', '', '', ''),
('209912121', '20', '12', '121', 'TK KUE ANDANI', '', 'JL. BABADAK', '', '', '', '', ''),
('301012121', '30', '12', '121', 'DENI PAKAN BURUNG', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('301112121', '30', '12', '121', 'TOKO SALMAN', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('30112121', '30', '12', '121', 'Kios Burung Man', '', 'Kedunghalang Talang', '', '', '', '', ''),
('301212121', '30', '12', '121', 'TOKO LAILA KUE KERING', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('301312121', '30', '12', '121', 'TOKO PLASTIK ARDI', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('301412121', '30', '12', '121', 'TOKO ERASIN SEMBAKO', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('301512121', '30', '12', '121', 'TOKO RANGKUTI', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('301612121', '30', '12', '121', 'NASUTION SEMBAKO', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('301712121', '30', '12', '121', 'TOKO JUARNO PAKAN BURUNG', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('301812121', '30', '12', '121', 'LUBIS SEMBAKO', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('301912121', '30', '12', '121', 'JUMARTI SEMBAKO', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('302012121', '30', '12', '121', 'BAPAK HERI TOKO BUAH', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('302112121', '30', '12', '121', 'A HANAPI SEMBAKO', '', 'JALAN TANAH BARU', '', '', '', '', ''),
('30212121', '30', '12', '121', 'Darin Jaya', '', 'Jl. Raya Pemda', '', '', '', '', ''),
('302212121', '30', '12', '121', 'BUAH JAYA SWALAYAN', '', '', '', '', '', '', ''),
('302312121', '30', '12', '121', 'DAULAU SEMBAKO', '', 'JL ASOGORI TAMAN CANARI TANAH BARU', '', '', '', '', ''),
('302412121', '30', '12', '121', 'TOKO UCOK SEMBAKO', '', 'JALAN ASOGORI TAMAN CANARI TANAH BARU', '', '', '', '', ''),
('302512121', '30', '12', '121', 'NURYANTO LIMBAH', '', 'JALAN ASOGORI TAMAN CANARI TANAH BARU', '', '', '', '', ''),
('302612121', '30', '12', '121', 'HASIGUAN', '', 'JALAN ASOGIR TANAH BARU', '', '', '', '', ''),
('302712121', '30', '12', '121', 'MAKANDANG SEMBAKO', '', '', '', '', '', '', ''),
('302812121', '30', '12', '121', 'KASIH SBK', '', 'TANAH BARU', '', '', '', '', ''),
('302912121', '30', '12', '121', 'SUTOTO SBK', '', 'ASOGIRI TANAH BARU', '', '', '', '', ''),
('303012121', '30', '12', '121', 'NURU SBK', '', 'JL. AJIBAR 3', '', '', '', '', ''),
('303112121', '30', '12', '121', 'HAMIN NURDIN SBK', '', 'TEGALEGA', '', '', '', '', ''),
('30312121', '30', '12', '121', 'Toko Beras Berkasjaya', '', 'Jl. Raya Pemda', '', '', '', '', ''),
('303212121', '30', '12', '121', 'SAEPUDIN SBK', '', 'JL AJIMAR 3', '', '', '', '', ''),
('303312121', '30', '12', '121', 'H MUSLIM SBK', '', 'JL AJIMAR 3', '', '', '', '', ''),
('303412121', '30', '12', '121', 'PA WAHAB SBK', '', 'JL AJIMAR 3', '', '', '', '', ''),
('303512121', '30', '12', '121', 'SUPARTI SBK', '', 'JL AJIMAR 3', '', '', '', '', ''),
('303612121', '30', '12', '121', 'RANGKUTI 2 SBK', '', 'JL AJIMAR 3', '', '', '', '', ''),
('303712121', '30', '12', '121', 'TB BANGUNAN', '', 'JL TEGALEGA NO. 1', '', '', '', '', ''),
('303812121', '30', '12', '121', 'TOKO KASIMA SBK', '', 'JL TEGALEGA', '', '', '', '', ''),
('303912121', '30', '12', '121', 'HOERUDIN SBK', '', 'JL TASMANIA', '', '', '', '', ''),
('304012121', '30', '12', '121', 'JUNAEDI SBK', '', 'JL TEGALEGA', '', '', '', '', ''),
('304112121', '30', '12', '121', 'TB. SINARJAYA', '', 'JL PANDU RAYA', '', '', '', '', ''),
('30412121', '30', '12', '121', 'TOKO DERHA SIMATUPANG', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('304212121', '30', '12', '121', 'TOKO PURBAJATIS', '', 'JL R KAN AN', '', '', '', '', ''),
('304312121', '30', '12', '121', 'TB HAJI THOHA', '', 'JL R KAN AN', '', '', '', '', ''),
('304412121', '30', '12', '121', 'PEGADAIAN', '', 'JL KS TUBUN', '', '', '', '', ''),
('304512121', '30', '12', '121', 'TOKO BERAS LUBIS', '', 'JL KS TUBUN', '', '', '', '', ''),
('304612121', '30', '12', '121', ' TOKO TRISIA', '', '', '', '', '', '', ''),
('304712121', '30', '12', '121', 'TOKO DASMIN SBK', '', 'JL KEDUNGHALANG', '', '', '', '', ''),
('304812121', '30', '12', '121', 'TOKO BERAS BULUNGAN', '', 'JL KEDUNGHALANG', '', '', '', '', ''),
('304912121', '30', '12', '121', 'TOKO ZIO', '', 'KEDUNGHALANG PANGKALAN', '', '', '', '', ''),
('305012121', '30', '12', '121', 'TB INORA', '', 'JL KEDUNGHALANG PANGKALAN 2', '', '', '', '0818105820', ''),
('30512121', '30', '12', '121', 'PRIMA KLON LAUNDRY', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('3051551', '30', '5', '51', 'APOTIK TUNGGAS', '', 'JL KEDUNGHALANG', '', '', '', '', ''),
('305212121', '30', '12', '121', 'TOKO PUJI PLASTIK', '', 'JALAN RAYA PEMUDA KERADENAN', '', '', '', '', ''),
('305312121', '30', '12', '121', 'WARUNG ABI 6 SBK', '', 'PERUM VILLA BOGOR INDAH', '', '', '', '', ''),
('305413131', '30', '13', '131', 'PEGADAIAN VILLA BOGOR INDAH', '', 'VILA BOGOR INDAHBLOK BB1/NO.17', '', '', '', '', ''),
('3055445', '30', '4', '45', 'PRIMA FRESHMART', '', 'VILA BOGOR INDAH 2', '', '', '', '', ''),
('305612121', '30', '12', '121', 'LAUNDRY YASFINA', '', 'VILA BOGOR INDAH 2', '', '', '', '', ''),
('3057551', '30', '5', '51', 'APOTEK PUTRIANA', '', 'VILA BOGOR INDAH 2 KEDUNGHALANG', '', '', '', '081219292811', ''),
('305812121', '30', '12', '121', 'TOKO BUAH PAK AJUM', '', 'VILA BOGOR INDAH', '', '', '', '', ''),
('305912121', '30', '12', '121', 'LANCAR BERKAH', '', 'VILA BOGOR INDAH', '', '', '', '', ''),
('306012121', '30', '12', '121', 'FAMILY LAUNDRY', '', 'VILA BOGOR INDAH', '', '', '', '082213426824', ''),
('306112121', '30', '12', '121', 'TOKO BUAH BANG JALY', '', 'VILA BOGOR INDAH', '', '', '', '', ''),
('30612121', '30', '12', '121', 'TOKO BUAH LESTARI', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('3062332', '30', '3', '32', 'AGEN TIKI', '', 'RUKO VILA BOGOR INDAH BLOK C2 NO 16', '', '', '', '', ''),
('306313131', '30', '13', '131', 'KANTOR POS', '', '', '', '', '', '', ''),
('306412121', '30', '12', '121', 'TOKO BUAH BP EDI', '', 'JL VILA BOGOR INDAH', '', '', '', '', ''),
('306512121', '30', '12', '121', 'TOKO SUMBER REJEKI', '', 'VILA BOGOR INDAH 5', '', '', '', '081332806161', ''),
('3066551', '30', '5', '51', 'APOTEK ZHAFIRAH', '', 'RUKO VILA BOGOR INDAH BLOK C A3 NO 10', '', '', '', '085811923472', ''),
('306712121', '30', '12', '121', 'TOKO BRAYA SBK', '', 'VILA BOGOR INDAH BLOK CA NO 20', '', '', '', '081522766894', ''),
('306812121', '30', '12', '121', 'TOKO NN', '', 'RUKO VILA BOGOR INDAH', '', '', '', '', ''),
('306912121', '30', '12', '121', 'TOKO BUAH HENDRA', '', 'VILA BOGOR INDAH', '', '', '', '', ''),
('307012121', '30', '12', '121', 'TOKO BUAH ROBERT', '', 'VILA BOGOR INDAH', '', '', '', '', ''),
('30712121', '30', '12', '121', 'TOKO DAPA', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('3071445', '30', '4', '45', '212 MART', '', 'JL VILA BOGOR INDAH CIPARIGI BLOK C', '', '', '', '08986025539', ''),
('307212121', '30', '12', '121', 'TOKO PELANGI SBK', '', 'VILA BOGOR BLOK G NO 3', '', '', '', '', ''),
('307312121', '30', '12', '121', 'TOKO VIRA CELL SBK', '', 'VILA BOGOR INDAH BLOK D1 NO 23', '', '', '', '', ''),
('307412121', '30', '12', '121', 'TOKO BIMA SBK', '', 'RUKO VILA BOGOR INDAH BLOK D1 NO 22', '', '', '', '', ''),
('307512121', '30', '12', '121', 'TOKO RAJA SBK', '', 'VILA BOGOR INDAH BLOK D1', '', '', '', '', ''),
('307613131', '30', '13', '131', 'PEGADAIAN', '', 'JL RUKO SIMPANG POMAD 3231', '', '', '', '', ''),
('3077551', '30', '5', '51', 'APOTEK ADIPA', '', 'JL MANDALA POMAD CIPARIGI', '', '', '', '', ''),
('307812121', '30', '12', '121', 'TOKO UJANG SBK', '', 'CIPARIGI', '', '', '', '', ''),
('307912121', '30', '12', '121', 'TOKO SNACK', '', 'CIPARIGI', '', '', '', '', ''),
('308012121', '30', '12', '121', 'TOKO BUAH SEGAR', '', 'JL MANDALA POMAD', '', '', '', '', ''),
('308112121', '30', '12', '121', 'TOKO ASHIE PLASTIK', '', 'JL MANDALA POMAD', '', '', '', '', ''),
('30812121', '30', '12', '121', 'TOKO BUAH BAPAK AGUS', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('308213131', '30', '13', '131', 'KANTOR POS', '', 'JL PANDAWA RAYA', '', '', '', '', ''),
('308313131', '30', '13', '131', 'BOLT', '', '', '', '', '', '', ''),
('308413131', '30', '13', '131', 'J&T', '', 'JL PANDAWA RAYA', '', '', '', '', ''),
('3085332', '30', '3', '32', 'TIKI', '', 'JL PANDAWA RAYA NO 15', '', '', '', '', ''),
('3086445', '30', '4', '45', 'PRIMA FRESHMART', '', 'JL PANDAWA RAYA NO 41', '', '', '', '', ''),
('308712121', '30', '12', '121', 'PANDAWA SEGAR', '', 'JL PANDAWA RAYA', '', '', '', '', ''),
('308812121', '30', '12', '121', 'TOKO BUAH MUGI BAROKAH', '', 'JL PANDU RAYA', '', '', '', '085888474452', ''),
('3089331', '30', '3', '31', 'JNE', '', 'JL PANDU RAYA NO 121', '', '', '', '', ''),
('3090551', '30', '5', '51', 'APOTEK MULIA PANDU', '', 'JL PANDU RAYA NO. 10', '', '', '', '', ''),
('309112121', '30', '12', '121', 'TOKO BERAS SUMBER NEGERI', '', 'JL PANDU RAYA', '', '', '', '', ''),
('30912121', '30', '12', '121', 'TOKO YUNUS SEMBAKO', '', 'JALAN RAYA PEMUDA', '', '', '', '', ''),
('309212121', '30', '12', '121', 'TOKO KUE GLORIA', '', 'JL PANDU RAYA ROLIO NO. 4', '', '', '', '', ''),
('309312121', '30', '12', '121', 'TOKO BAHAN KUE', '', 'JALAN PANDURAYA/86', '', '', '', '', ''),
('3094881', '30', '8', '81', 'AGEN LPG 3 Kg PT BINTANA EKA NUSA', '', 'JL. PANDU RAYA NO.13', '', '', '', '', ''),
('401012121', '40', '12', '121', 'P. TONO', '', 'MALAKBAK/BABAKAN', '', '', '', '', ''),
('401112121', '40', '12', '121', 'P. BONDIN', '', 'MALAKBAK', '', '', '', '', ''),
('40112121', '40', '12', '121', 'EDI', '', 'CIHEULUET', '', '', '', '', ''),
('401212121', '40', '12', '121', 'YAHYA', '', 'MALAKBAK', '', '', '', '', ''),
('401312121', '40', '12', '121', 'KOH. AKIM', '', 'JL. OTISTA PSR BOGOR', '', '', '', '', ''),
('401412121', '40', '12', '121', 'TK. CIANJUR', '', 'JL. OTISTA PSR BOGOR', '', '', '', '', ''),
('401512121', '40', '12', '121', 'P. ULUNG', '', 'PSR BOGOR', '', '', '', '', ''),
('401612121', '40', '12', '121', 'P. LATIP', '', 'PSR BOGOR', '', '', '', '', ''),
('401712121', '40', '12', '121', 'TK. ZUBAIR DAULAH', '', 'KEBON KALAPA', '', '', '', '', ''),
('401812121', '40', '12', '121', 'TK. SUKIRNO', '', '', '', '', '', '', ''),
('401912121', '40', '12', '121', 'BU HARNO', '', 'JL. SEMPUR KALER', '', '', '', '', ''),
('402012121', '40', '12', '121', 'TOKO KAYU MANIS', '', 'NO. 22, KEL. SEMPUR. JL SEMPUR KALER', '', '', '', '', ''),
('402112121', '40', '12', '121', 'BU MAMI', '', 'JL. LODAYA NO. 20, KEL. BABAKAN', '', '', '', '', ''),
('40212121', '40', '12', '121', 'AJI', '', 'BABAKAN FAKULTAS', '', '', '', '', ''),
('402212121', '40', '12', '121', 'H. ENJAI', '', 'JL. LODAYA NO. 5, KEL. BABAKAN', '', '', '', '', ''),
('402312121', '40', '12', '121', 'BUB NYAI', '', 'JL. LODAYA NO. 6, KEL. BABAKAN', '', '', '', '', ''),
('402412121', '40', '12', '121', 'SEMARJO', '', 'JL. LODAYA NO. 24, KEL. BABAKAN', '', '', '', '', ''),
('402512121', '40', '12', '121', 'DERI', '', 'JL. LODAYA NO. 9, KEL. BABAKAN', '', '', '', '', ''),
('402612121', '40', '12', '121', 'WOSING WOSING LAUNDRY', '', 'JL. LODAYA II NO. 5, KEL. BABAKAN', '', '', '', '', ''),
('402712121', '40', '12', '121', 'FAHMI', '', 'JL. MALABAR UJUNGAN NO. 20', '', '', '', '', ''),
('402812121', '40', '12', '121', 'SOLANA LAUNDRY', '', 'JL. MALABAR UJUNG NO. 25 RT. 01/RW. 07', '', '', '', '', ''),
('402912', '40', '12', '', 'ZIHRIN/YANI', '', 'JL. MALABAR UJUNG RT. 03/RW. 02', '', '', '', '', ''),
('403012121', '40', '12', '121', 'MALABAR', '', 'JL MALABAR UJUNG', '', '', '', '', ''),
('403112121', '40', '12', '121', 'TONO', '', 'JL. MALABAR UJUNG NO. 15', '', '', '', '', ''),
('40312121', '40', '12', '121', 'FATIMAH', '', '', '', '', '', '', ''),
('403212121', '40', '12', '121', 'BU TATI', '', 'JL. MALABAR UJUNG, KEL. BABAKAN', '', '', '', '', ''),
('403312121', '40', '12', '121', 'PASARIBU', '', 'JL. MALABAR UJUNG', '', '', '', '', ''),
('403412121', '40', '12', '121', 'BU ETI', '', 'JL. MALABAR UJUNG, KEL. BABAKAN', '', '', '', '', ''),
('403512121', '40', '12', '121', 'TK. BAHDI', '', 'JL. MALABAR UJUNG, KEL. BABAKAN', '', '', '', '', ''),
('403612121', '40', '12', '121', 'TK. UCOK', '', 'DANAU KALIMUTU TEGALEGA RT.01/RW.03', '', '', '', '', ''),
('403712121', '40', '12', '121', 'BU IDA', '', 'DANAU KALIMUTU, NO. 37', '1', '3', '', '', ''),
('403812121', '40', '12', '121', 'TOKO JAGORAWI', '', 'JL. CIDANGIANG NO.41, KEL TEGALEGA', '', '', '', '', ''),
('403912121', '40', '12', '121', 'ROYAL LAUNDRY', '', 'JL. CIDANGIANG, TEGALEGA', '2', '8', '', '', ''),
('404012121', '40', '12', '121', 'TOKO CITRA USAHA', '', 'NO. 14, KEL. TEGALEGA', '', '', '', '', ''),
('404112121', '40', '12', '121', 'TOKO PURWAJATI', '', 'JL. PALEDANG NO. 49 ', '', '', '', '', ''),
('40412121', '40', '12', '121', 'SUHARTO', '', 'TEGAL MANGGA', '', '', '', '', ''),
('404212121', '40', '12', '121', 'TOKO RUDI', '', 'JL. GEREJA NO. 3, KEL. PALEDANG', '', '', '', '', ''),
('404312121', '40', '12', '121', 'TOKO NURKAS', '', 'JL. NURKAS NO. 1', '3', '2', '', '', ''),
('404412121', '40', '12', '121', 'TOKO REDJO', '', 'JL. CIBALOK NO. 2, KEL. PABATON', '', '', '', '', ''),
('404512121', '40', '12', '121', 'MARUN', '', 'JL. CIBALOK NO. 6, KEL.PABATON', '', '', '', '', ''),
('404612121', '40', '12', '121', 'LANI', '', 'JL. CIBALOK NO. 2', '', '', '', '', ''),
('404712121', '40', '12', '121', 'TOKO HAJI TAUFIQ', '', 'JL. SAWOJAJAR NO. 10', '', '', '', '', ''),
('404812121', '40', '12', '121', 'TOKO HEJO', '', 'JL. CIBOGOR NO. 29, KEL. CIBOGOR', '', '', '', '', ''),
('404912121', '40', '12', '121', 'TOKO DATUK', '', 'JL. CIBOGOR 30, KEL. PABATON', '', '', '', '', ''),
('405012121', '40', '12', '121', 'TOKO ASEP', '', 'JL. KANTIN NO. 13, KEL. PABATON', '', '', '', '', ''),
('405112121', '40', '12', '121', 'TOKO ULFAH', '', 'JL. CIWARINGIN 17 NO. 22 ', '4', '2', '', '', ''),
('40512121', '40', '12', '121', 'UCOK HASAN', '', 'BOGOR BARU', '', '', '', '', ''),
('405212121', '40', '12', '121', 'BU IDA', '', 'JL. CIWARINGIN KALER NO 13', '3', '9', '', '', ''),
('405312121', '40', '12', '121', 'PA. ADE', '', 'JL. CIWARINGIN KAUM GG MAESJID NO. 11', '', '', '', '', ''),
('405412121', '40', '12', '121', 'PAK OMAN', '', 'JL. CWARINGIN KAUM GG MASJID NO. 6', '', '', '', '', ''),
('405512121', '40', '12', '121', 'TOKO LOKASARI', '', 'JL. CIWARINGIN NO. 19', '', '', '', '', ''),
('405612121', '40', '12', '121', 'TOKO MUDA MUDI', '', 'JL. ABESIN NO. 58', '', '', '', '', ''),
('405712121', '40', '12', '121', 'TOKO ANISA', '', 'JL. ABESIN NO. 57', '', '', '', '', ''),
('405812121', '40', '12', '121', 'TOKO DAFA', '', 'JL. ABESIN NO. 55', '', '', '', '', ''),
('40612121', '40', '12', '121', 'ASEP', '', 'CILIBENDE', '', '', '', '', ''),
('40712121', '40', '12', '121', 'BU ETI', '', 'BABAKAN PASAR', '', '', '', '', ''),
('40812121', '40', '12', '121', 'TK. BUDI', '', 'MALABAK/BABAKAN', '', '', '', '', ''),
('40912121', '40', '12', '121', 'TATI', '', 'MALAKBAK/BABAKAN', '', '', '', '', ''),
('501012121', '50', '12', '121', 'POSYANDU MATAHARI', '', 'DRAMAGA LONCENG', '1', '4', '', '', ''),
('501112', '50', '12', '', 'POSYANDU DAHLIA', '', 'DRAMAGA LONCENG', '4', '4', '', '', ''),
('50112121', '50', '12', '121', 'TK LIYATHA', '', 'JL. PEMUDA CARINGIN NO. 14 A', '', '', '', '081324436925', ''),
('501212121', '50', '12', '121', 'POSYANDU ANGGREK', '', 'DRAMAGA KAUM ', '2', '5', '', '', ''),
('501312121', '50', '12', '121', 'POSYANDU WIJAYA KUSUMA', '', 'CARINGIN', '1', '6', '', '', ''),
('501412121', '50', '12', '121', 'POSYANDU PERENCY', '', 'PAKUAN REGENCY', '2', '7', '', '', ''),
('501512121', '50', '12', '121', 'POSYANDU MEKARWANGI', '', 'GARDU JAYA', '1', '2', '', '', ''),
('501612', '50', '12', '', 'SUMBER NEGERI', '', 'PANCASAN PASIR JAYA', '', '', '', '', ''),
('501712121', '50', '12', '121', 'SUMBER NEGERI', '', 'PANCASAN PASIRJAYA', '', '', '', '', ''),
('501812121', '50', '12', '121', 'TOKO BERAS SRI', '', 'PANCASAN PASIRJAYA', '', '', '', '', ''),
('501912121', '50', '12', '121', 'TK BANG UDIN', '', 'BABAKAN', '1', '8', '', '087830373636', ''),
('502012121', '50', '12', '121', 'TK BANG HERI', '', 'BABAKAN', '2', '8', '', '', ''),
('502112121', '50', '12', '121', 'TK MUNTE', '', 'JL. BALUMBANG JAYA ', '4', '4', '', '', ''),
('50212121', '50', '12', '121', 'TK BU ATIKAH', '', 'JL. PEMUDA CARINGIN MARGAJAYA', '', '', '', '', ''),
('502212', '50', '12', '', 'WR BU YANAH', '', 'JL. BALUMBANG JAYA', '4', '4', '', '', ''),
('502312', '50', '12', '', 'TK SABRINA LUBIS', '', 'JL. BALUMBANG JAYA', '4', '4', '', '081317814228', ''),
('502412121', '50', '12', '121', 'WR BU IRMA', '', 'JL. BALUMBANG JAYA', '4', '4', '', '089607631809', ''),
('502512121', '50', '12', '121', 'TK RASYA', '', 'JL. BALUMBANG JAYA', '1', '3', '', '081289353293', ''),
('502612', '50', '12', '', 'TK PAK UAHAR', '', 'JL. BALUMBANG JAYA', '1', '3', '', '085881338178', ''),
('502712', '50', '12', '', 'WR PAK HARUN', '', 'JL. BALUMBANG JAYA', '1', '3', '', '089537236041', ''),
('502812', '50', '12', '', 'WR BU ENTIH', '', 'JL. BALUMBANG JAYA ', '3', '2', '', '082213163575', ''),
('502912121', '50', '12', '121', 'WR BU ENTIH', '', 'JL. BALUMBANG JAYA ', '3', '2', '', '082213163575', ''),
('503012121', '50', '12', '121', 'TK IKBAL', '', 'JL. BALUMBANG JAYA', '3', '2', '', '085216241934', ''),
('503112', '50', '12', '', 'WR BU KOKOM', '', 'JL. BALUMBANG JAYA', '3', '1', '', '', ''),
('50312121', '50', '12', '121', 'WR BU APAS', '', 'JL. PEMUDA CARINGIN', '', '', '', '', ''),
('503212121', '50', '12', '121', 'WR BU ENIH', '', 'JL. BALUMBANG JAYA', '3', '1', '', '', ''),
('503312121', '50', '12', '121', 'TK SYARIF', '', 'JL. BALUMBANG JAYA', '1', '2', '', '', ''),
('503412121', '50', '12', '121', 'TK RAJA', '', 'JL. BALUMBANG JAYA', '1', '2', '', '085280632332', ''),
('503512121', '50', '12', '121', 'TB MEGAJAYA', '', 'JL. SINDANGBARANG NO. 185', '', '', '', '', ''),
('503612121', '50', '12', '121', 'TK SINDANG RAYA', '', 'JL. RY IBRAHIM AJI NO. 201', '', '', '', '', ''),
('503712121', '50', '12', '121', 'TK MADINAH', '', 'JL. RY IBRAHIM AJI NO. 338', '', '', '', '', ''),
('503812121', '50', '12', '121', 'TOKO BUAH LOJI', '', 'JL. LETJEN IBRAHIM AJI', '', '', '', '', ''),
('503912121', '50', '12', '121', 'TK RAY', '', 'JL. RY SINDANG BARANG', '', '', '', '', ''),
('504012121', '50', '12', '121', 'TK JAGUAR', '', 'JL. RY SINDANG BARANG', '', '', '', '', ''),
('504112121', '50', '12', '121', 'TK PAK OTIB', '', 'SINDANG BARANG (SBJ)', '', '', '', '', ''),
('504212121', '50', '12', '121', 'TK PRASETIA', '', 'SINDANG BARANG (SBJ)', '', '', '', '', ''),
('504312121', '50', '12', '121', 'TK ADAM', '', 'JL. RY SINDANG BARANG', '', '', '', '', ''),
('504412121', '50', '12', '121', 'TK DINI', '', 'JL. RY SINDANG BARANG', '', '', '', '', ''),
('504512121', '50', '12', '121', 'TK PUAS', '', 'JL. RY SINDANG BARANG PILAR', '', '', '', '', ''),
('504612121', '50', '12', '121', 'TK LUBIS', '', 'JL. CILUBANG LEBAK', '', '', '', '081262371669', ''),
('504712121', '50', '12', '121', 'TK DIANA', '', 'JL. CILUBANG LEBAK', '', '', '', '', ''),
('504812121', '50', '12', '121', 'TK RAJA', '', 'JL. CILUBANG LEBAK', '', '', '', '', ''),
('504912121', '50', '12', '121', 'TK RAHMAT', '', 'JL. CILUBANG LEBAK', '', '', '', '', ''),
('505012121', '50', '12', '121', 'WR TEH IMA', '', 'JL. CILUBANG LEBAK', '', '', '', '', ''),
('505112121', '50', '12', '121', 'POSYANDU MELATI 1', '', 'JL. SITU GEDE', '', '', '', '', ''),
('50512', '50', '12', '', 'WR. ERMAN', '', 'JL. BATUHULUNG', '1', '3', '', '', ''),
('50512121', '50', '12', '121', 'TB BINTANG BARU', '', 'JL. PEMUDA CARINGIN', '', '', '', '', ''),
('505212121', '50', '12', '121', 'POSYANDU MELATI 2', '', 'JL. SITU GEDE', '', '', '', '', ''),
('505312121', '50', '12', '121', 'POSYANDU ANYELIR', '', 'JL. SITU GEDE', '', '', '', '', ''),
('505412121', '50', '12', '121', 'POSYANDU FLAMBOYAN', '', 'JL. SITU GEDE', '', '', '', '', ''),
('505512121', '50', '12', '121', 'POSYANDU SAKURA', '', 'JL. SITUGEDE', '', '', '', '', ''),
('505612121', '50', '12', '121', 'POSYANDU KENANGA', '', 'JL. SITU GEDE', '', '', '', '', ''),
('505712121', '50', '12', '121', 'POSYANDU ANGGREK', '', 'JL. SITU GEDE', '', '', '', '', ''),
('505812121', '50', '12', '121', 'POSYANDU MAWAR', '', 'JL. SITU GEDE', '', '', '', '', ''),
('505912121', '50', '12', '121', 'POSYANDU DAHLIA', '', 'JL. SITU GEDE', '', '', '', '', ''),
('506012121', '50', '12', '121', 'POSYANDU CEMPAKA', '', 'JL. SITU GEDE', '', '', '', '', ''),
('506112121', '50', '12', '121', 'POSYANDU BUGENVIL', '', 'JL. SITU GEDE', '', '', '', '', ''),
('50612121', '50', '12', '121', 'TK PAK LUBIS', '', 'JL. BATUHULUNG', '', '', '', '081931112412', ''),
('506212121', '50', '12', '121', 'TK SADULUR', '', 'JL. NAGRAK SITU GEDE', '', '', '', '', ''),
('506312121', '50', '12', '121', 'TB DIKA', '', 'JL. CIFOR NAGRAK SITU GEDE', '', '', '', '', ''),
('506412121', '50', '12', '121', 'TK MEKAR', '', 'JL. CIFOR NAGRAK SITU GEDE', '', '', '', '', ''),
('506512121', '50', '12', '121', 'WR SAYURAN', '', 'SITU GEDE', '', '', '', '', ''),
('506612121', '50', '12', '121', 'TK AULIA', '', 'SITU GEDE', '', '', '', '', ''),
('506712121', '50', '12', '121', 'WR ILHAM', '', 'SITU GEDE', '', '', '', '', ''),
('506812121', '50', '12', '121', 'TK FAISAL', '', 'SITU GEDE', '', '', '', '', ''),
('506912121', '50', '12', '121', 'TB SUMBER BANGUNAN', '', 'JL. SITU GEDE', '', '', '', '', ''),
('507012121', '50', '12', '121', 'TB RIMB MANDIRI', '', 'JL. CIFOR SITU GEDE', '', '', '', '', ''),
('507112121', '50', '12', '121', 'TB WAHANA BARU', '', 'JL. CIFOR SITU GEDE', '', '', '', '', ''),
('50712121', '50', '12', '121', 'TUKANG BUAH', '', 'MARGAJAYA', '', '', '', '', ''),
('507212121', '50', '12', '121', 'TK BUAH DESTA', '', 'JL. CIFOR SITU GEDE', '', '', '', '', ''),
('507312121', '50', '12', '121', 'TB SINAR BANGUNAN', '', 'JL. CIFOR SITU GEDE', '', '', '', '02548410390', ''),
('507412121', '50', '12', '121', 'TB BANJAR PUTRA', '', 'JL. RY PASIR MULYA', '', '', '', '', ''),
('507512121', '50', '12', '121', 'TOKO PELANGI', '', 'PANCASAN PASIR JAYA', '', '', '', '', ''),
('507612121', '50', '12', '121', 'ARI JAYA SEMBAKO', '', 'PANCASAN PASIR JAYA', '', '', '', '', ''),
('507712121', '50', '12', '121', 'KARYA BANGUNAN', '', 'PASIR KUDA NO. 166', '', '', '', '', ''),
('507812121', '50', '12', '121', 'TB GRAND PERKASA II NO. 6A', '', 'JL. RA. ABDULLAH NO. 6A', '', '', '', '', ''),
('507912121', '50', '12', '121', 'MUTIARA PUTRA', '', 'JL. RA. ABDULLAH NO. 18 PASIR KUDA', '', '', '', '', ''),
('508012121', '50', '12', '121', 'TK AL-ADIB', '', 'JL. RA. ABDULLAH', '', '', '', '', ''),
('508112121', '50', '12', '121', 'TK ANUGERAH', '', 'JL. RA. ABDULLAH NO. 2', '', '', '', '', ''),
('50812121', '50', '12', '121', 'POSYANDU BOUGENVILLE', '', 'GUGAH SARI', '2', '2', '', '', ''),
('508212121', '50', '12', '121', 'TK SUMBER NEGERI', '', 'JL. RY BOJONG MENTENG NO. 16', '', '', '', '', ''),
('508312121', '50', '12', '121', 'TK PANCING', '', 'JL. RY BOJONG MENTENG NO. 20', '', '', '', '', ''),
('508412121', '50', '12', '121', 'TK HEMAN', '', 'JL. RY BOJONG MENTENG', '', '', '', '', ''),
('508512121', '50', '12', '121', 'TK HASAN', '', 'JL. CIBALAGUNG PASUR KUDA', '', '', '', '', '');
INSERT INTO `t_pengguna` (`kd_pengguna`, `id_kecamatan`, `kd_kategori`, `kd_subkategori`, `nm_pengguna`, `nm_usaha`, `alamat`, `rt`, `rw`, `telp`, `hp`, `email`) VALUES
('508612121', '50', '12', '121', 'TK DANI', '', 'JL. JABARU DUA PASIR KUDA', '', '', '', '', ''),
('508712121', '50', '12', '121', 'TK AMANDA', '', 'JL. JABARU PASIR KUDA', '', '', '', '087770022221', ''),
('508812121', '50', '12', '121', 'TK JAYA BARU', '', 'JL. JABARU DUA PASIR KUDA', '', '', '', '089664164209', ''),
('50912121', '50', '12', '121', 'POSYANDU MAWAR', '', 'PERUM.ZIARA VALLEY', '6', '2', '', '', ''),
('601012121', '60', '12', '121', 'TB. Mandiri', '', '', '5', '13', '', '', ''),
('601112121', '60', '12', '121', 'Bpk. Mujianto SM', '', '', '1', '13', '', '', ''),
('60112121', '60', '12', '121', 'sopian/kios makan burung', '', '', '5', '3', '', '087771910594', ''),
('601212121', '60', '12', '121', 'Parno Sembako', '', '', '1', '13', '', '', ''),
('601312121', '60', '12', '121', 'H. HOLIS/SEMBAKO', '', '', '1', '13', '', '087870155799', ''),
('601412121', '60', '12', '121', 'Agung/Sembako', '', '', '3', '5', '', '085771364221', ''),
('601512121', '60', '12', '121', 'Beno/Sembako', '', 'Jl. Pondok Rumput (07)', '', '', '', '081932457115', ''),
('601612121', '60', '12', '121', 'Mumun', '', '', '2', '5', '', '085817394483', ''),
('601712121', '60', '12', '121', 'Aci/Sembako', '', '', '2', '5', '', '', ''),
('601812121', '60', '12', '121', 'Yudi/Sembako', '', '', '2', '2', '', '', ''),
('601912121', '60', '12', '121', 'ACAM/SEMBAKO', '', '', '2', '2', '', '', ''),
('602012121', '60', '12', '121', 'EDI/SEMBAKO', '', '', '3', '11', '', '', ''),
('602112121', '60', '12', '121', 'PURNOMO/SEMBAKO', '', '', '3', '11', '', '085711727789', ''),
('60212121', '60', '12', '121', 'Bpk. Kardi/Telur', '', '', '', '', '', '081315796982', ''),
('602212121', '60', '12', '121', 'SUNARDI/SEMBAKO', '', '', '4', '11', '', '', ''),
('602312121', '60', '12', '121', 'ARIF/SEMBAKO', '', '', '1', '11', '', '08997517572', ''),
('602412121', '60', '12', '121', 'RAHMAN', '', '10', '5', '', '', '085717040803', ''),
('602512121', '60', '12', '121', 'YADI/SBK', '', '', '6', '8', '', '081399275028', ''),
('602612121', '60', '12', '121', 'HOIRUL/SBK', '', '', '1', '8', '', '089612146530', ''),
('602712121', '60', '12', '121', 'ENGKOS/SBK', '', '', '1', '8', '', '', ''),
('602812121', '60', '12', '121', 'NURUL HIDAYAH/SBK', '', '', '1', '8', '', '085221914506', ''),
('602912121', '60', '12', '121', 'UKAT/SBK', '', '', '', '', '', '', ''),
('603012121', '60', '12', '121', 'UKAT/SBK', '', '', '4', '8', '', '083819206240', ''),
('603112121', '60', '12', '121', 'SUBARI/SBK', '', '', '5', '8', '', '', ''),
('60312121', '60', '12', '121', 'Bu Santi', '', '', '5', '13', '', '', ''),
('603212121', '60', '12', '121', 'T. FAJAR', '', '', '1', '8', '', '082311076729', ''),
('603312121', '60', '12', '121', 'RAHMAT/SBK', '', '', '', '', '', '', ''),
('603412121', '60', '12', '121', 'T MALKOM', '', '', '1', '8', '', '081388680904', ''),
('603512121', '60', '12', '121', 'TK RAKAN/SBK', '', '', '1', '9', '', '', ''),
('603612121', '60', '12', '121', 'Loandroid', '', '', '3', '3', '', '081221908426', ''),
('603712121', '60', '12', '121', 'TK RAJA/SBK', '', '', '2', '8', '082310842406', '', ''),
('603812121', '60', '12', '121', 'TK H YANAH/SBK', '', '', '', '', '', '', ''),
('603912121', '60', '12', '121', 'TK TASLIM JAYA', '', '', '4', '3', '', '083818409559', ''),
('604012121', '60', '12', '121', 'TB UTAMA', '', '', '5', '3', '02518335057', '', ''),
('604112121', '60', '12', '121', 'TONO', '', '', '4', '3', '', '087887809175', ''),
('60412121', '60', '12', '121', 'ASEP/BURUNG', '', '', '4', '6', '', '085780839720', ''),
('604212121', '60', '12', '121', 'TK NATUSION', '', '', '5', '9', '', '087874136335', ''),
('604312121', '60', '12', '121', 'TK. RANGKUTI', '', '', '5', '9', '', '085312819632', ''),
('604412121', '60', '12', '121', 'DIANA/SBK', '', '', '1', '9', '', '', ''),
('604512121', '60', '12', '121', 'ALROYAN', '', '', '1', '3', '02518333917', '', ''),
('60512121', '60', '12', '121', 'Bu. Eni/Sembako', '', '', '5', '6', '', '085888046702', ''),
('60612121', '60', '12', '121', 'Bu.Suwolo/Sembako', '', '', '5', '6', '', '081297993368', ''),
('60712121', '60', '12', '121', 'Bpk. Apudin/Sembako', '', '', '5', '6', '', '085718008558', ''),
('60812121', '60', '12', '121', 'Bu Mira', '', '', '6', '1', '', '085692809481', ''),
('60912121', '60', '12', '121', 'Bpk. Varis/Tukang Bangunan', '', '', '5', '13', '', '087872310809', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_pengguna_online`
--

CREATE TABLE `t_pengguna_online` (
  `kd_pengguna` varchar(12) NOT NULL,
  `id_kecamatan` varchar(2) NOT NULL,
  `kd_kategori` varchar(2) NOT NULL,
  `kd_subkategori` varchar(4) NOT NULL,
  `nm_pengguna` varchar(50) NOT NULL,
  `nm_usaha` varchar(20) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `rt` varchar(30) NOT NULL,
  `rw` varchar(30) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `hp` varchar(12) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` enum('not_confirmed','confirmed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_pengguna_online`
--

INSERT INTO `t_pengguna_online` (`kd_pengguna`, `id_kecamatan`, `kd_kategori`, `kd_subkategori`, `nm_pengguna`, `nm_usaha`, `alamat`, `rt`, `rw`, `telp`, `hp`, `email`, `status`) VALUES
('601881', '60', '8', '81', 'Markum', 'Penyalur Gas', 'cimanggu', '1', '2', '12345678', '', '', 'not_confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `t_ptera`
--

CREATE TABLE `t_ptera` (
  `kd_ptera` varchar(30) NOT NULL,
  `no_trans` int(10) NOT NULL,
  `no_reg` varchar(10) DEFAULT NULL,
  `kd_pengguna` varchar(12) NOT NULL,
  `kd_jenis` varchar(2) NOT NULL,
  `kd_subjenis` int(2) NOT NULL,
  `kd_subsubjenis` int(4) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `no_seri` varchar(100) NOT NULL,
  `kapasitas` varchar(20) NOT NULL,
  `stok_liter` varchar(20) NOT NULL,
  `tgl_daftar` date DEFAULT NULL,
  `tgl_bubuh` date DEFAULT NULL,
  `tgl_serah` date NOT NULL,
  `terakhir_tera` date DEFAULT NULL,
  `masaberlaku` date DEFAULT NULL,
  `tgl_bayar` date NOT NULL,
  `cap_tera` enum('Ada','Tidak Ada') DEFAULT NULL,
  `sertifikat` enum('Ada','Tidak Ada') DEFAULT NULL,
  `stiker` enum('Ada','Tidak Ada') DEFAULT NULL,
  `kondisi` enum('Baik','Rusak') DEFAULT NULL,
  `tindakan` enum('Diganti','Tidak Diganti','DiTera') DEFAULT NULL,
  `hasil_uji` enum('Sah','Tidak Sah') DEFAULT NULL,
  `keterangan` text,
  `jml_bayar` int(10) DEFAULT NULL,
  `status2` enum('baru','ulang') NOT NULL,
  `status` enum('proses','selesai','expired') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ptera`
--

INSERT INTO `t_ptera` (`kd_ptera`, `no_trans`, `no_reg`, `kd_pengguna`, `kd_jenis`, `kd_subjenis`, `kd_subsubjenis`, `merk`, `no_seri`, `kapasitas`, `stok_liter`, `tgl_daftar`, `tgl_bubuh`, `tgl_serah`, `terakhir_tera`, `masaberlaku`, `tgl_bayar`, `cap_tera`, `sertifikat`, `stiker`, `kondisi`, `tindakan`, `hasil_uji`, `keterangan`, `jml_bayar`, `status2`, `status`) VALUES
('501121211150-1', 1, 'REG-1', '50112121', '1', 15, 0, '', '', '10 Kg', '0', '2018-05-28', '2018-05-28', '2018-05-28', '2018-05-28', '2019-05-28', '2018-05-28', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 10000, 'baru', 'expired'),
('502121211110-1', 2, 'REG-2', '50212121', '1', 11, 0, 'TB', '', '5 kg', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '2018-05-28', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('503121211120-1', 3, 'REG-3', '50312121', '1', 12, 0, 'ZON', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('505121211110-1', 6, 'REG-6', '50512121', '1', 11, 0, '', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('505121110-1', 7, 'REG-7', '50512', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('506121211110-1', 8, 'REG-8', '50612121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('507121211120-1', 9, 'REG-9', '50712121', '1', 12, 0, '', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('508121211130-1', 10, 'REG-10', '50812121', '1', 13, 0, '', '', '25', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('509121211130-1', 11, 'REG-11', '50912121', '1', 13, 0, '', '', '25', '0', '0000-00-00', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5010121211130-1', 12, 'REG-12', '501012121', '1', 13, 0, '', '', '25', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5011121130-1', 13, 'REG-13', '501112', '1', 13, 0, '', '', '25', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5012121211130-1', 14, 'REG-14', '501212121', '1', 13, 0, '', '', '25', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5013121211130-1', 15, 'REG-15', '501312121', '1', 13, 0, '', '', '25', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5014121211130-1', 16, 'REG-16', '501412121', '1', 13, 0, '', '', '25', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5015121211130-1', 17, 'REG-17', '501512121', '1', 13, 0, '', '', '25', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-28', '2019-05-28', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5017121211170-1', 21, 'REG-21', '501712121', '1', 17, 0, '', '', '150', '0', '2016-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '2017-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('5018121211140-1', 22, 'REG-22', '501812121', '1', 14, 0, 'CB', '', '150', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '2017-05-28', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5019121211110-1', 23, 'REG-23', '501912121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '2018-05-28', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5020121211110-1', 24, 'REG-24', '502012121', '1', 11, 0, 'AB', '', '10', '0', '2018-05-28', '2018-05-28', '0000-00-00', '2018-05-28', '2019-05-28', '2018-05-28', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5021121211110-1', 25, 'REG-25', '502112121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '2013-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5022121110-1', 26, 'REG-26', '502212', '1', 11, 0, 'RAJA', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5023121110-1', 27, 'REG-27', '502312', '1', 11, 0, 'BTD', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2016-05-31', '2016-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5024121211110', 28, 'REG-28', '502412121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5025121211110', 29, 'REG-29', '502512121', '1', 11, 0, 'TB', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5026121110', 30, 'REG-30', '502612', '1', 11, 0, 'RAJA', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5027121110', 31, 'REG-31', '502712', '1', 11, 0, 'PDS', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5029121211110', 33, 'REG-33', '502912121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5029121211120', 34, 'REG-34', '502912121', '1', 12, 0, '', '', '20', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5030121211110', 35, 'REG-35', '503012121', '1', 11, 0, '', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5031121120', 36, 'REG-36', '503112', '1', 12, 0, '', '', '15', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5032121211110', 37, 'REG-37', '503212121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5033121211110', 38, 'REG-38', '503312121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5034121211110', 39, 'REG-39', '503412121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5035121211110', 40, 'REG-40', '503512121', '1', 11, 0, 'WS', '', '10', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5036121211160-1', 44, 'REG-44', '503612121', '1', 16, 0, 'QUATRO', '', '30', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-28', '2015-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('5036121211160-1', 45, 'REG-45', '503612121', '1', 16, 0, 'CLOSER', '', '40', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-28', '2015-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('5036121211160-1', 46, 'REG-46', '503612121', '1', 16, 0, 'ELEKTO TORA', '', '100', '0', '2018-05-28', '0000-00-00', '0000-00-00', '2018-05-28', '2015-05-28', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('5037121211170', 47, 'REG-47', '503712121', '1', 17, 0, '', '', '100', '0', '2018-05-28', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5038121211150', 48, 'REG-48', '503812121', '1', 15, 0, '', '', '30', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5039121211120-1', 49, 'REG-49', '503912121', '1', 12, 0, 'CAPACITY', '', '15', '0', '2018-05-31', '2018-05-31', '2018-05-31', '2018-05-31', '2019-05-31', '2018-05-31', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 10000, 'baru', 'selesai'),
('5040121211110', 50, 'REG-50', '504012121', '1', 11, 0, 'DS', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '2012-05-31', '2013-05-31', '2012-05-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'expired'),
('5041121211110', 51, 'REG-51', '504112121', '1', 11, 0, 'AB', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5042121211110', 52, 'REG-52', '504212121', '1', 11, 0, 'AB', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5043121211110', 53, 'REG-53', '504312121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5044121211110-1', 54, 'REG-54', '504412121', '1', 11, 0, 'HMN', '', '10', '0', '2018-05-31', '2018-05-31', '2018-05-31', '2018-05-31', '2019-05-31', '2018-05-31', 'Ada', 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 10000, 'baru', 'selesai'),
('5045121211150-1', 55, 'REG-55', '504512121', '1', 15, 0, 'PRECIO', '', '30', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '2018-05-31', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5046121211120-1', 56, 'REG-56', '504612121', '1', 12, 0, '', '', '10', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5047121211110-1', 57, 'REG-57', '504712121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5048121211110-1', 58, 'REG-58', '504812121', '1', 11, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5049121211110-1', 59, 'REG-59', '504912121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5050121211120-1', 60, 'REG-60', '505012121', '1', 12, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5051121211130', 61, 'REG-61', '505112121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5052121211130', 62, 'REG-62', '505212121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5053121211130', 63, 'REG-63', '505312121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5054121211130', 64, 'REG-64', '505412121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5055121211130', 65, 'REG-65', '505512121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5056121211130', 66, 'REG-66', '505612121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5057121211130', 67, 'REG-67', '505712121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5058121211130', 68, 'REG-68', '505812121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5059121211130', 69, 'REG-69', '505912121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5060121211130', 70, 'REG-70', '506012121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5061121211130', 71, 'REG-71', '506112121', '1', 13, 0, '', '', '25', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5062121211110', 72, 'REG-72', '506212121', '1', 11, 0, 'AB', '', '30', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5063121211150', 73, 'REG-73', '506312121', '1', 15, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5064121211110', 74, 'REG-74', '506412121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5065121211110', 75, 'REG-75', '506512121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5066121211110', 76, 'REG-76', '506612121', '1', 11, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5067121211110', 77, 'REG-77', '506712121', '1', 11, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5068121211110', 78, 'REG-78', '506812121', '1', 11, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('112121112', 79, 'REG-79', '', '1', 12, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5069121211120', 80, 'REG-80', '506912121', '1', 12, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5070121211130', 81, 'REG-81', '507012121', '1', 13, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5071121211130', 82, 'REG-82', '507112121', '1', 13, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5072121211130', 83, 'REG-83', '507212121', '1', 13, 0, '', '', '5', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5073121211130', 84, 'REG-84', '507312121', '1', 13, 0, '', '', '5', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5074121211110-1', 85, 'REG-85', '507412121', '1', 11, 0, 'PM', '', '10', '0', '2016-05-31', '0000-00-00', '0000-00-00', '2015-05-31', '2016-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5075121211140-1', 86, 'REG-86', '507512121', '1', 14, 0, '', '', '50', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('5076121211170', 87, 'REG-87', '507612121', '1', 17, 0, 'PGB', '', '50', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5077121211110-1', 88, 'REG-88', '507712121', '1', 11, 0, 'KENMASTER', '', '2', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '2018-05-31', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5078121211120-1', 89, 'REG-89', '507812121', '1', 12, 0, 'FIVE GOATS', '', '5', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5079121211110', 90, 'REG-90', '507912121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('5080121211150-1', 91, 'REG-91', '508012121', '1', 15, 0, 'NAGATA', '', '30', '0', '2018-05-31', '2009-05-31', '0000-00-00', '2009-05-31', '2010-05-31', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('5081121211110-1', 92, 'REG-92', '508112121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('5082121211140-1', 93, 'REG-93', '508212121', '1', 14, 0, 'FA GANI', '', '500', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '2017-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5083121211120-1', 94, 'REG-94', '508312121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '2017-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5084121211120-1', 95, 'REG-95', '508412121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2017-05-31', '2018-05-31', '2018-05-31', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('5085121211110-1', 96, 'REG-96', '508512121', '1', 11, 0, 'IB', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '2014-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5086121211110-1', 97, 'REG-97', '508612121', '1', 11, 0, '', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '2013-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('5087121211110-1', 98, 'REG-98', '508712121', '1', 11, 0, 'NSH', '', '10', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2018-05-31', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('5088121211110-1', 99, 'REG-99', '508812121', '1', 11, 0, 'KRS', '', '10', '0', '2018-05-31', '0000-00-00', '0000-00-00', '0000-00-00', '2017-05-31', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('301121211110-1', 102, 'REG-102', '30112121', '1', 11, 0, '', '', '10', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '2018-05-31', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('302121211170-1', 103, 'REG-103', '30212121', '1', 17, 0, '', '', '150', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('303121211170-1', 104, 'REG-104', '30312121', '1', 17, 0, '', '', '100', '0', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('30312121224243-1', 105, 'REG-105', '30312121', '2', 24, 243, '', '', '0', '1', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('30312121224243-1', 106, 'REG-106', '30312121', '2', 24, 243, '', '', '0', '0.5', '2018-05-31', '2018-05-31', '0000-00-00', '2018-05-31', '2019-05-31', '0000-00-00', NULL, NULL, NULL, 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('101121211150-1', 107, 'REG-107', '10112121', '1', 15, 0, 'NAGATA', '', '30', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('101121211150-1', 108, 'REG-108', '10112121', '1', 15, 0, 'PRECIO', '', '30', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('102121211120-1', 109, 'REG-109', '10212121', '1', 12, 0, '', '', '10', '0', '2018-06-04', '2018-06-04', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, 'Rusak', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('102121211140-1', 110, 'REG-110', '10212121', '1', 14, 0, 'cb', '', '300', '0', '2018-06-04', '2018-06-04', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, 'Rusak', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('103121211170-1', 111, 'REG-111', '10312121', '1', 17, 0, '', '', '50', '0', '2018-06-04', '2016-07-02', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('104121211110-1', 112, 'REG-112', '10412121', '1', 11, 0, 'UNI', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('105121211110-1', 113, 'REG-113', '10512121', '1', 11, 0, 'DS', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('106121211110-1', 114, 'REG-114', '10612121', '1', 11, 0, 'UNI', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('107121211110-1', 115, 'REG-115', '10712121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('108121211110-1', 116, 'REG-116', '10812121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('109121211110-1', 117, 'REG-117', '10912121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1010121211110-1', 118, 'REG-118', '101012121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1011121211120-1', 119, 'REG-119', '101112121', '1', 12, 0, '', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1012121211110-1', 120, 'REG-120', '101212121', '1', 11, 0, 'AB', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1013121211110-1', 121, 'REG-121', '101312121', '1', 11, 0, 'REMAJA', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1014121211110-1', 122, 'REG-122', '101412121', '1', 11, 0, 'UNI', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1015121211110-1', 123, 'REG-123', '101512121', '1', 11, 0, 'AB', '', '10', '0', '2018-06-04', '2016-06-04', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('1016121211110-1', 124, 'REG-124', '101612121', '1', 11, 0, 'DINASTI', '', '10', 'O', '2018-06-04', '2016-06-04', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('1017121211110-1', 125, 'REG-125', '101712121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('1018121211110-1', 126, 'REG-126', '101812121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('1019121211150-1', 127, 'REG-127', '101912121', '1', 15, 0, 'PRECISE', '', '30', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('1020121211110-1', 128, 'REG-128', '102012121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2016-06-04', '2017-06-04', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('1021121211110', 129, 'REG-129', '102112121', '1', 11, 0, 'HMN', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1022121211110-1', 130, 'REG-130', '102212121', '1', 11, 0, '', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('1023121211110-1', 131, 'REG-131', '102312121', '1', 11, 0, 'TB', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('1024121211110-1', 132, 'REG-132', '102412121', '1', 11, 0, 'UNI', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('1025121211120', 133, 'REG-133', '102512121', '1', 12, 0, 'NAGANI', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1026121211110', 134, 'REG-134', '102612121', '1', 11, 0, 'TB', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1027121211140-1', 135, 'REG-135', '102712121', '1', 14, 0, 'CB', '', '500', '0', '2018-06-04', '2018-04-04', '2018-04-04', '2018-04-04', '2019-04-04', '2018-04-04', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 10000, 'baru', 'selesai'),
('1028121211170-1', 136, 'REG-136', '102812121', '1', 17, 0, '', '', '150', '0', '2018-04-04', '2018-04-04', '2018-04-04', '2018-04-04', '2019-04-04', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 100000, 'baru', 'selesai'),
('1029121211110-1', 137, 'REG-137', '102912121', '1', 11, 0, 'PM', '', '10', '0', '2013-06-04', '2013-06-04', '0000-00-00', '2013-06-04', '2014-06-04', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('1030121211110', 138, 'REG-138', '103012121', '1', 11, 0, 'RUKUN', '', '10', '0', '2015-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1031121211110', 139, 'REG-139', '103112121', '1', 11, 0, 'PTS', '', '10', '0', '2015-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1032121211120-1', 140, 'REG-140', '103212121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '2018-06-04', '2019-06-04', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('1033121211110', 141, 'REG-141', '103312121', '1', 11, 0, 'GS', '', '10', '0', '2013-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1034121211110', 142, 'REG-142', '103412121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1035121211110', 143, 'REG-143', '103512121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1036121211110', 144, 'REG-144', '103612121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1037121211110', 145, 'REG-145', '103712121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1038121211120', 146, 'REG-146', '103812121', '1', 12, 0, 'FIVE GOATS', '', '10', 'O', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1039121211110', 147, 'REG-147', '103912121', '1', 11, 0, 'TM', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1040121211110', 148, 'REG-148', '104012121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1041121211110', 149, 'REG-149', '104112121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1042121211110', 150, 'REG-150', '104212121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1043121211110', 151, 'REG-151', '104312121', '1', 11, 0, 'BIMA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1044121211120', 152, 'REG-152', '104412121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1045121211110', 153, 'REG-153', '104512121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1046121211120', 154, 'REG-154', '104612121', '1', 12, 0, 'NAGAMI', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1047121211120', 155, 'REG-155', '104712121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1048121211120', 156, 'REG-156', '104812121', '1', 12, 0, 'CAMRY', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1049121211120', 157, 'REG-157', '104912121', '1', 12, 0, 'CAMRY', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1050121211110', 158, 'REG-158', '105012121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1051121211120', 159, 'REG-159', '105112121', '1', 12, 0, 'CAMRY', '', '10', 'O', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1052121211110', 160, 'REG-160', '105212121', '1', 11, 0, 'BIMA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1053121211140', 161, 'REG-161', '105312121', '1', 14, 0, 'CB KARYA MULIA', '', '300', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1054121211110', 162, 'REG-162', '105412121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1055121211120', 163, 'REG-163', '105512121', '1', 12, 0, 'CAMRY', '', '60', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1055121211120', 164, 'REG-164', '105512121', '1', 12, 0, 'CAMRY', '', '2', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1056121211110', 165, 'REG-165', '105612121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1057121211110', 166, 'REG-166', '105712121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1057121211120', 167, 'REG-167', '105712121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1058121211110', 168, 'REG-168', '105812121', '1', 11, 0, 'TB', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1059121211110', 169, 'REG-169', '105912121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1060121211110', 170, 'REG-170', '106012121', '1', 11, 0, 'REMAJA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1061121211120', 171, 'REG-171', '106112121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1062121211110', 172, 'REG-172', '106212121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1063121211110', 173, 'REG-173', '106312121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1064121211110', 174, 'REG-174', '106412121', '1', 11, 0, 'REMAJA', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1064121211110', 175, 'REG-175', '106412121', '1', 11, 0, 'YOYO', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1065121211120', 176, 'REG-176', '106512121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1066121211110', 177, 'REG-177', '106612121', '1', 11, 0, 'NSN', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1067121211110', 178, 'REG-178', '106712121', '1', 11, 0, 'DS', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1068121211110', 179, 'REG-179', '106812121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2018-06-04', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1069121211110', 180, 'REG-180', '106912121', '1', 11, 0, 'DS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1070121211110', 181, 'REG-181', '107012121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1070121211110', 182, 'REG-182', '107012121', '1', 11, 0, 'DC', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1071121211120', 183, 'REG-183', '107112121', '1', 12, 0, 'NAGAMI', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1071121211150', 184, 'REG-184', '107112121', '1', 15, 0, 'PRECIO', '', '30', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1072121211120', 185, 'REG-185', '107212121', '1', 12, 0, 'WESTON', '', '15', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1072121211120', 186, 'REG-186', '107212121', '1', 12, 0, 'NAGAMI', '', '15', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1073121211150', 187, 'REG-187', '107312121', '1', 15, 0, 'PRECIO', '', '30', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1073121211120', 188, 'REG-188', '107312121', '1', 12, 0, 'FIVE GOATS', '', '20', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1074121211110', 189, 'REG-189', '107412121', '1', 11, 0, 'KRS', '', '10', '0', '2016-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1075121211110', 190, 'REG-190', '107512121', '1', 11, 0, 'KRS', '', '10', '0', '2016-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1076121211110', 191, 'REG-191', '107612121', '1', 11, 0, 'PM', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1077121211150', 192, 'REG-192', '107712121', '1', 15, 0, 'CMOS', '', '30', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1078121211120', 193, 'REG-193', '107812121', '1', 12, 0, 'FIVE GOATS', '', '15', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1079121211110', 194, 'REG-194', '107912121', '1', 11, 0, 'DS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1080121211150', 195, 'REG-195', '108012121', '1', 15, 0, 'CAMRY', '', '30', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1081121211120', 196, 'REG-196', '108112121', '1', 12, 0, 'NAGAMI', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1082121211110', 197, 'REG-197', '108212121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1083121211110', 198, 'REG-198', '108312121', '1', 11, 0, 'PM', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1084121211110', 199, 'REG-199', '108412121', '1', 11, 0, 'UNI', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1085121211110', 200, 'REG-200', '108512121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1086121211110', 201, 'REG-201', '108612121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1087121211120', 202, 'REG-202', '108712121', '1', 12, 0, 'NAGAKO', '', '2', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1088121211120', 203, 'REG-203', '108812121', '1', 12, 0, 'FIVE GOATS', '', '40', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1088121211110', 204, 'REG-204', '108812121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1089121211110', 205, 'REG-205', '108912121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1090121211120', 206, 'REG-206', '109012121', '1', 12, 0, 'CROWN', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1090121211120', 207, 'REG-207', '109012121', '1', 12, 0, 'KINGMASTER', '', '2', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1091121211120', 208, 'REG-208', '109112121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1092121211110', 209, 'REG-209', '109212121', '1', 11, 0, 'PTS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1093121211120', 210, 'REG-210', '109312121', '1', 12, 0, 'FIVE GOATS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1094121211120', 211, 'REG-211', '109412121', '1', 12, 0, 'FIVE GOATS', '', '20', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1095121211150', 212, 'REG-212', '109512121', '1', 15, 0, 'CAMRY', '', '30', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1096121211110', 213, 'REG-213', '109612121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1097121211110', 214, 'REG-214', '109712121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1098121211110', 215, 'REG-215', '109812121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('1099121211120', 216, 'REG-216', '109912121', '1', 12, 0, 'FIVE GOATS', '', '5', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('10100121211140', 217, 'REG-217', '1010012121', '1', 14, 0, 'CA', '', '500', '0', '2018-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('10101121211110', 218, 'REG-218', '1010112121', '1', 11, 0, 'GS', '', '10', '0', '2016-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('10102121211110', 219, 'REG-219', '1010212121', '1', 11, 0, 'DC', '', '10', '0', '2017-06-05', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('304121211110-1', 221, 'REG-221', '30412121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('305121211120-1', 222, 'REG-222', '30512121', '1', 12, 0, '', '', '30', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('306121211150-1', 223, 'REG-223', '30612121', '1', 15, 0, '', '', '30', '0', '2009-06-07', '2009-06-07', '0000-00-00', '2009-06-07', '2010-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('306121211150-1', 224, 'REG-224', '30612121', '1', 15, 0, '', '', '30', '0', '2009-06-07', '2009-06-07', '0000-00-00', '2009-06-07', '2010-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('307121211110-1', 225, 'REG-225', '30712121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('308121211120-1', 226, 'REG-226', '30812121', '1', 12, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('309121211110-1', 227, 'REG-227', '30912121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses');
INSERT INTO `t_ptera` (`kd_ptera`, `no_trans`, `no_reg`, `kd_pengguna`, `kd_jenis`, `kd_subjenis`, `kd_subsubjenis`, `merk`, `no_seri`, `kapasitas`, `stok_liter`, `tgl_daftar`, `tgl_bubuh`, `tgl_serah`, `terakhir_tera`, `masaberlaku`, `tgl_bayar`, `cap_tera`, `sertifikat`, `stiker`, `kondisi`, `tindakan`, `hasil_uji`, `keterangan`, `jml_bayar`, `status2`, `status`) VALUES
('3010121211170-1', 228, 'REG-228', '301012121', '1', 17, 0, '', '', '300', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3011121211120-1', 229, 'REG-229', '301112121', '1', 12, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('', 230, 'REG-230', '301212121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3013121211150-1', 231, 'REG-231', '301312121', '1', 15, 0, '', '', '30', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3014121211150-1', 232, 'REG-232', '301412121', '1', 15, 0, '', '', '30', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3015121211110-1', 233, 'REG-233', '301512121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3016121211110-1', 234, 'REG-234', '301612121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3017121211110-1', 235, 'REG-235', '301712121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3018121211110-1', 236, 'REG-236', '301812121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3019121211110-1', 237, 'REG-237', '301912121', '1', 11, 0, '', '', '10', '0', '2018-06-07', '2018-06-07', '0000-00-00', '2018-06-07', '2019-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('3020121211110-1', 238, 'REG-238', '302012121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3021121211110-1', 239, 'REG-239', '302112121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3022121211110-1', 240, 'REG-240', '302212121', '1', 11, 0, '', '', '10', '', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3021121211120-1', 241, 'REG-241', '302112121', '1', 12, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3023121211110-1', 242, 'REG-242', '302312121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3024121211110-1', 243, 'REG-243', '302412121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3025121211170-1', 244, 'REG-244', '302512121', '1', 17, 0, '', '', '500', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3026121211110-1', 245, 'REG-245', '302612121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2017-06-07', '0000-00-00', '2017-06-07', '2018-06-07', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('3027121211110', 246, 'REG-246', '302712121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3028121211110', 247, 'REG-247', '302812121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3029121211120', 248, 'REG-248', '302912121', '1', 12, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3030121211110', 249, 'REG-249', '303012121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3031121211110', 250, 'REG-250', '303112121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3032121211120', 251, 'REG-251', '303212121', '1', 12, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3033121211110', 252, 'REG-252', '303312121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3034121211110', 253, 'REG-253', '303412121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3035121211110', 254, 'REG-254', '303512121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3036121211110', 255, 'REG-255', '303612121', '1', 11, 0, '', '', '10', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3037121211110', 256, 'REG-256', '303712121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3038121211110', 257, 'REG-257', '303812121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3039121211110', 258, 'REG-258', '303912121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3040121211110', 259, 'REG-259', '304012121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3041121211110-1', 260, 'REG-260', '304112121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('3042121211110', 261, 'REG-261', '304212121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3043121211110', 262, 'REG-262', '304312121', '1', 11, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3044121211110', 263, 'REG-263', '304412121', '1', 15, 0, 'GRAINS', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3045121211120', 264, 'REG-264', '304512121', '1', 12, 0, '', '', '10', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3046121211110', 265, 'REG-265', '304612121', '1', 11, 0, '', '', '10', '0', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3047121211150', 266, 'REG-266', '304712121', '1', 15, 0, '', '', '30', '0', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3048121211170', 267, 'REG-267', '304812121', '1', 17, 0, '', '', '500', '0', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3045121215510', 268, 'REG-268', '304512121', '5', 51, 0, '', '', '0', '2', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3045121215510', 269, 'REG-269', '304512121', '5', 51, 0, '', '', '0', '1', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3045121215510', 270, 'REG-270', '304512121', '5', 51, 0, '', '', '0', '0.5', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3049121215510', 271, 'REG-271', '304912121', '5', 51, 0, '', '', '0', '2', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3049121215510', 272, 'REG-272', '304912121', '5', 51, 0, '', '', '0', '1', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3049121215510', 273, 'REG-273', '304912121', '5', 51, 0, '', '', '0', '0.5', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3048121215510', 274, 'REG-274', '304812121', '5', 51, 0, '', '', '0', '2', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3048121215510', 275, 'REG-275', '304812121', '5', 51, 0, '', '', '0', '1', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3048121215510', 276, 'REG-276', '304812121', '5', 51, 0, '', '', '0', '0.5', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3047121215510', 277, 'REG-277', '304712121', '5', 51, 0, '', '', '0', '2', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3047121215510', 278, 'REG-278', '304712121', '5', 51, 0, '', '', '0', '1', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3047121215510', 279, 'REG-279', '304712121', '5', 51, 0, '', '', '', '0.5', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3050121211130', 280, 'REG-280', '305012121', '1', 13, 0, '', '', '25', '0', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30515511180', 281, 'REG-281', '3051551', '1', 18, 0, '', '', '50', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3052121211120', 282, 'REG-282', '305212121', '1', 12, 0, '', '', '10', '0', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3053121211120', 283, 'REG-283', '305312121', '1', 12, 0, '', '', '5', '0', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3053121215510', 284, 'REG-284', '305312121', '5', 51, 0, '', '', '', '1', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3053121215510', 285, 'REG-285', '305312121', '5', 51, 0, '', '', '', '0.5', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3054131311150', 286, 'REG-286', '305413131', '1', 15, 0, 'GRAINS', '', '2200 ', '0', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30554451150', 287, 'REG-287', '3055445', '1', 15, 0, '', '', '30', '0', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30575511180', 288, 'REG-288', '3057551', '1', 18, 0, 'YUMA', '', '1000', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3058121211120', 289, 'REG-289', '305812121', '1', 12, 0, '', '', '10', '0', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3059121211120', 290, 'REG-290', '305912121', '1', 12, 0, '', '', '20', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3059121211110', 291, 'REG-291', '305912121', '1', 11, 0, '', '', '10', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3059121215510', 292, 'REG-292', '305912121', '5', 51, 0, '', '', '', '2', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3059121215510', 293, 'REG-293', '305912121', '5', 51, 0, '', '', '', '1', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3060121211150', 294, 'REG-294', '306012121', '1', 15, 0, 'YOKOMAWA', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3061121211120', 295, 'REG-295', '306112121', '1', 12, 0, '', '', '10', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30623321120', 296, 'REG-296', '3062332', '1', 12, 0, '', '', '60', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3063131311150', 297, 'REG-297', '306313131', '1', 15, 0, '', '', '150', '', '2014-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3063131311150', 298, 'REG-298', '306313131', '1', 15, 0, 'QUATRO', '', '30', '', '2014-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3064121211120', 299, 'REG-299', '306412121', '1', 12, 0, '', '', '15', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3065121211150', 300, 'REG-300', '306512121', '1', 15, 0, 'GUATTRO', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30665511180', 301, 'REG-301', '3066551', '1', 18, 0, 'PROTINOL', '', '50', '', '2014-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30665511180', 302, 'REG-302', '3066551', '1', 18, 0, 'PROTINOL', '', '1000', '', '2014-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3067121211110', 303, 'REG-303', '306712121', '1', 11, 0, '', '', '10', '', '2009-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3067121215510', 304, 'REG-304', '306712121', '5', 51, 0, '', '', '', '2', '2009-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3067121215510', 305, 'REG-305', '306712121', '5', 51, 0, '', '', '', '1', '2009-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3067121215510', 306, 'REG-306', '306712121', '5', 51, 0, '', '', '', '0.5', '2009-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3067121211150', 307, 'REG-307', '306712121', '1', 15, 0, '', '', '40', '', '2009-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3068121211150', 308, 'REG-308', '306812121', '1', 15, 0, 'TANITA', '', '1000', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3069121211150', 309, 'REG-309', '306912121', '1', 15, 0, 'CMOS', '', '30', '', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3070121211150', 310, 'REG-310', '307012121', '1', 15, 0, 'TORA', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30714451150', 311, 'REG-311', '3071445', '1', 15, 0, 'CAMRY', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3072121211120', 312, 'REG-312', '307212121', '1', 12, 0, '', '', '10', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3073121211120', 313, 'REG-313', '307312121', '1', 12, 0, '', '', '10', '', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3073121215510', 314, 'REG-314', '307312121', '5', 51, 0, '', '', '', '2', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3073121215510', 315, 'REG-315', '307312121', '5', 51, 0, '', '', '', '1', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3073121215510', 316, 'REG-316', '307312121', '5', 51, 0, '', '', '', '0.5', '2013-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3074121211110', 317, 'REG-317', '307412121', '1', 11, 0, '', '', '10', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3074121211150', 318, 'REG-318', '307412121', '1', 15, 0, 'DIGI', '', '30', '0', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3075121211120', 319, 'REG-319', '307512121', '1', 12, 0, '', '', '5', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3075121211120', 320, 'REG-320', '307512121', '1', 12, 0, '', '', '1000', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3076131311150', 321, 'REG-321', '307613131', '1', 15, 0, 'GRAINS', '', '2200', '', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30775511180', 322, 'REG-322', '3077551', '1', 18, 0, '', '', '50', '', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3078121211110', 323, 'REG-323', '307812121', '1', 11, 0, '', '', '10', '', '2012-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3078121215510', 324, 'REG-324', '307812121', '5', 51, 0, '', '', '', '1', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3078121215510', 325, 'REG-325', '307812121', '5', 51, 0, '', '', '', '2', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3078121215510', 326, 'REG-326', '307812121', '5', 51, 0, '', '', '', '0.5', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3079121211110', 327, 'REG-327', '307912121', '1', 11, 0, '', '', '10', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3080121211150', 328, 'REG-328', '308012121', '1', 15, 0, '', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3080121211150', 329, 'REG-329', '308012121', '1', 15, 0, '', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3081121211150', 330, 'REG-330', '308112121', '1', 15, 0, '', '', '1000', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3082131311150', 331, 'REG-331', '308213131', '1', 15, 0, 'QUATTRO', '', '30', '', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3083131311150', 332, 'REG-332', '308313131', '1', 15, 0, 'QUATTRO', '', '150', '', '2017-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3084131311150', 333, 'REG-333', '308413131', '1', 15, 0, 'SONIC', '', '150', '', '2016-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30853321120', 334, 'REG-334', '3085332', '1', 12, 0, '', '', '150', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30864451150', 335, 'REG-335', '3086445', '1', 15, 0, 'JPA', '', '6', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3087121211120', 336, 'REG-336', '308712121', '1', 12, 0, '', '', '20', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3088121211150', 337, 'REG-337', '308812121', '1', 15, 0, 'NAGATA', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3088121211150', 338, 'REG-338', '308812121', '1', 15, 0, 'NAGATA', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30893311150', 339, 'REG-339', '3089331', '1', 15, 0, 'CAMRY', '', '30', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('30905511180', 340, 'REG-340', '3090551', '1', 18, 0, 'YUMA', '', '1000', '', '2018-06-07', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('3091121211170-1', 341, 'REG-341', '309112121', '1', 17, 0, '', '', '150', '', '2017-06-07', '2017-06-08', '0000-00-00', '2017-06-08', '2018-06-08', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('3092121211150-1', 342, 'REG-342', '309212121', '1', 15, 0, '', '', '30', '', '2017-06-08', '2017-06-08', '0000-00-00', '2017-06-08', '2018-06-08', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('3092121211150-1', 343, 'REG-343', '309212121', '1', 15, 0, '', '', '30', '', '2017-06-08', '2017-06-08', '0000-00-00', '2017-06-08', '2018-06-08', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('3093121211150-1', 344, 'REG-344', '309312121', '1', 15, 0, 'ACS', '', '1', '', '2015-06-08', '2015-06-08', '0000-00-00', '2015-06-08', '2016-06-08', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('30948811150', 345, 'REG-345', '3094881', '1', 15, 0, 'TORA', '', '', '', '2017-06-08', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('401121211110-1', 346, 'REG-346', '40112121', '1', 11, 0, '', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '2016-06-25', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('402121211110-1', 347, 'REG-347', '40212121', '1', 11, 0, 'WRS', '', '10', '', '2017-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('403121211110-1', 348, 'REG-348', '40312121', '1', 11, 0, 'AB', '', '10', '', '2016-06-25', '0000-00-00', '0000-00-00', '2016-06-25', '2017-06-25', '2016-06-25', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('404121211110-1', 349, 'REG-349', '40412121', '1', 11, 0, 'AB', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '2016-06-25', 'Ada', NULL, 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('404121211110-1', 350, 'REG-350', '40412121', '1', 11, 0, 'DS', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '2016-06-25', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('405121211110-1', 351, 'REG-351', '40512121', '1', 11, 0, 'KRS', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('406121211110-1', 352, 'REG-352', '40612121', '1', 11, 0, 'DS', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('407121211110-1', 353, 'REG-353', '40712121', '1', 11, 0, 'DS', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('408121211120-1', 354, 'REG-354', '40812121', '1', 12, 0, '', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('409121211110-1', 355, 'REG-355', '40912121', '1', 11, 0, 'DS', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4010121211110-1', 356, 'REG-356', '401012121', '1', 11, 0, 'KURNIA', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4011121211120-1', 357, 'REG-357', '401112121', '1', 12, 0, '', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4012121211110-1', 358, 'REG-358', '401212121', '1', 11, 0, 'TB', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', NULL, 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4013121211110-1', 359, 'REG-359', '401312121', '1', 11, 0, 'TB', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4014121211140-1', 360, 'REG-360', '401412121', '1', 14, 0, 'CB', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4015121211110-1', 361, 'REG-361', '401512121', '1', 11, 0, 'PAMOR', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4016121211120-1', 362, 'REG-362', '401612121', '1', 12, 0, '', '', '5', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4017121211110-1', 363, 'REG-363', '401712121', '1', 11, 0, 'TB', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4018121211110-1', 364, 'REG-364', '401812121', '1', 11, 0, 'UNI', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4019121211110-1', 365, 'REG-365', '401912121', '1', 11, 0, 'RABUIN', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', NULL, 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4020121211150-1', 366, 'REG-366', '402012121', '1', 15, 0, 'TORA', '', '10', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4021121211110-1', 367, 'REG-367', '402112121', '1', 11, 0, 'NS', '', '10', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4022121211110-1', 368, 'REG-368', '402212121', '1', 11, 0, 'KRS', '', '10', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4023121211110-1', 369, 'REG-369', '402312121', '1', 11, 0, 'NS', '', '10', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4024121211110-1', 370, 'REG-370', '402412121', '1', 11, 0, 'ROSE', '', '10', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4025121211120-1', 371, 'REG-371', '402512121', '1', 12, 0, 'NAGAMI', '10', '', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4026121211150-1', 372, 'REG-372', '402612121', '1', 15, 0, 'CAMRY', '', '30', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4027121211150-1', 373, 'REG-373', '402712121', '1', 15, 0, 'FLECO', '', '30', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4028121211150-1', 374, 'REG-374', '402812121', '1', 15, 0, 'NORIS', '', '40', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4029121120-1', 375, 'REG-375', '402912', '1', 12, 0, 'CAMRY', '', '30', '', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4030121211110-1', 376, 'REG-376', '403012121', '1', 11, 0, 'TB', '', '30', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('4031121211110-1', 377, 'REG-377', '403112121', '1', 11, 0, 'KURNIA', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('4032121211110-1', 378, 'REG-378', '403212121', '1', 11, 0, 'DUA SERANGKAI', '', '10', '', '2017-06-25', '2017-06-25', '0000-00-00', '2017-06-25', '2018-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4033121211110-1', 379, 'REG-379', '403312121', '1', 11, 0, 'PAMOR', '', '10', '', '2012-06-25', '2012-06-25', '0000-00-00', '2012-06-25', '2013-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4034121211110-1', 380, 'REG-380', '403412121', '1', 11, 0, 'RTD', '', '10', '', '2017-06-25', '2017-06-25', '0000-00-00', '2017-06-25', '2018-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4035121211120-1', 381, 'REG-381', '403512121', '1', 12, 0, '', '', '10', '', '2017-06-25', '2017-06-25', '0000-00-00', '2017-06-25', '2018-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4036121211110-1', 382, 'REG-382', '403612121', '1', 11, 0, 'KRB', '', '10', '', '2016-06-25', '2016-06-25', '0000-00-00', '2016-06-25', '2017-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4037121211110-1', 385, 'REG-385', '403712121', '1', 11, 0, 'KURNIA', '', '10', '0', '2014-06-25', '2014-06-25', '0000-00-00', '2014-06-25', '2015-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4038121211110-1', 386, 'REG-386', '403812121', '1', 11, 0, 'AB', '', '10', '0', '2017-06-25', '2017-06-25', '0000-00-00', '2017-06-25', '2018-06-25', '2017-06-25', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('4039121211120-1', 387, 'REG-387', '403912121', '1', 12, 0, '', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4040121211110-1', 388, 'REG-388', '404012121', '1', 11, 0, 'KRS', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4040121211110-1', 389, 'REG-389', '404012121', '1', 11, 0, 'CAMRY', '', '30', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4041121211110-1', 390, 'REG-390', '404112121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4042121211110-1', 391, 'REG-391', '404212121', '1', 11, 0, 'NSN', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4042121211110-1', 392, 'REG-392', '404212121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4043121211110-1', 393, 'REG-393', '404312121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4044121211110-1', 394, 'REG-394', '404412121', '1', 11, 0, 'RAJA', '', '10', '0', '2012-06-25', '0000-00-00', '0000-00-00', '2012-06-25', '2013-06-25', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'expired'),
('4045121211120-1', 395, 'REG-395', '404512121', '1', 12, 0, '', '', '5', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4046121211120-1', 396, 'REG-396', '404612121', '1', 12, 0, 'NAKAMI', '', '13', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4047121211150-1', 397, 'REG-397', '404712121', '1', 15, 0, 'ALEGRA', '', '150', '0', '2017-06-25', '2017-06-25', '0000-00-00', '2017-06-25', '2018-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('4048121211110-1', 398, 'REG-398', '404812121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4049121211110-1', 399, 'REG-399', '404912121', '1', 11, 0, 'TTS', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'proses'),
('4050121211120-1', 400, 'REG-400', '405012121', '1', 12, 0, 'CIPACI', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4051121211120-1', 401, 'REG-401', '405112121', '1', 12, 0, '', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4052121211110-1', 402, 'REG-402', '405212121', '1', 11, 0, 'UNI', '', '10', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4053121211120-1', 403, 'REG-403', '405312121', '1', 12, 0, '', '', '5', '0', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4054121211110-1', 404, 'REG-404', '405412121', '1', 11, 0, 'KUDA TERBANG', '', '10', '0', '0000-00-00', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4055121211110-1', 405, 'REG-405', '405512121', '1', 11, 0, '', '', '10', 'O', '2018-06-25', '2018-06-25', '0000-00-00', '2018-06-25', '2019-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('4056121211110-1', 406, 'REG-406', '405612121', '1', 11, 0, 'ML', '', '0', '0', '2015-06-25', '2015-06-25', '0000-00-00', '2015-06-25', '2016-06-25', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('4057121211110-1', 407, 'REG-407', '405712121', '1', 11, 0, 'NS', '', '10', '0', '2015-06-26', '2015-06-26', '0000-00-00', '2015-06-26', '2016-06-26', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('4058121211110-1', 408, 'REG-408', '405812121', '1', 11, 0, '', '', '10', '0', '2018-06-26', '2018-06-26', '0000-00-00', '2018-06-26', '2019-06-26', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('201121211110-1', 409, 'REG-409', '20112121', '1', 11, 0, 'TB', '', '10', '', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', NULL, NULL, NULL, 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('202121211110-1', 410, 'REG-410', '20212121', '1', 11, 0, 'KRS', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('203121211110-1', 411, 'REG-411', '20312121', '1', 11, 0, 'SUN', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('204121211110-1', 412, 'REG-412', '20412121', '1', 11, 0, 'NAGAMI', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('205121211110-1', 413, 'REG-413', '20512121', '1', 11, 0, 'AKUR', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('206121211120-1', 414, 'REG-414', '20612121', '1', 12, 0, 'CAPACITY', '', '10', '0', '2018-06-29', '2018-06-29', '0000-00-00', '2018-06-29', '2019-06-29', '0000-00-00', NULL, 'Ada', NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('207121211110-1', 415, 'REG-415', '20712121', '1', 11, 0, 'TB', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('207121211130-1', 416, 'REG-416', '20712121', '1', 13, 0, '', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('208121211110-1', 417, 'REG-417', '20812121', '1', 11, 0, 'UNI GARUDA', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('209121211120-1', 418, 'REG-418', '20912121', '1', 12, 0, '', '', '10', '0', '2016-06-29', '2017-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2010121211110-1', 419, 'REG-419', '201012121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2011121211110-1', 420, 'REG-420', '201112121', '1', 11, 0, 'LM', '', '10', '0', '2016-06-29', '0000-00-00', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2012121211110-1', 421, 'REG-421', '201212121', '1', 11, 0, 'KURNIA', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2013121211120-1', 422, 'REG-422', '201312121', '1', 12, 0, '', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2014121211120', 423, 'REG-423', '201412121', '1', 12, 0, '', '', '10', '0', '2016-06-29', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2015121211110-1', 424, 'REG-424', '201512121', '1', 11, 0, 'KRS', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2016121211110-1', 425, 'REG-425', '201612121', '1', 11, 0, 'KURNIA', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2017121211110-1', 426, 'REG-426', '201712121', '1', 11, 0, 'NSN', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2018121211110-1', 427, 'REG-427', '201812121', '1', 11, 0, 'CROWN', '', '10', '0', '2018-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', NULL, 'Ada', NULL, 'Rusak', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('2019121211110-1', 428, 'REG-428', '201912121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2019121211110-1', 429, 'REG-429', '201912121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2020121211120-1', 430, 'REG-430', '202012121', '1', 12, 0, 'CAPACITY', '', '10', '0', '2018-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', NULL, 'Ada', NULL, 'Rusak', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('2021121211130-1', 431, 'REG-431', '202112121', '1', 13, 0, '', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2022121211110-1', 432, 'REG-432', '202212121', '1', 11, 0, 'RAJA', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2023121211120-1', 433, 'REG-433', '202312121', '1', 12, 0, '', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2024121211120-1', 434, 'REG-434', '202412121', '1', 12, 0, 'CROWN', '', '10', '0', '2018-06-29', '2018-06-29', '0000-00-00', '2018-06-29', '2019-06-29', '0000-00-00', NULL, 'Ada', NULL, 'Rusak', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('2025121211110-1', 435, 'REG-435', '202512121', '1', 11, 0, 'AB', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2026121211120-1', 436, 'REG-436', '202612121', '1', 12, 0, '', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2027121211120-1', 437, 'REG-437', '202712121', '1', 12, 0, '', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2028121211110-1', 438, 'REG-438', '202812121', '1', 11, 0, 'AB', '', '10', '0', '2016-06-29', '2016-06-29', '0000-00-00', '2016-06-29', '2017-06-29', '0000-00-00', 'Ada', 'Ada', 'Ada', 'Baik', 'DiTera', 'Sah', '', 0, 'baru', 'expired'),
('2029121211120-1', 439, 'REG-439', '202912121', '1', 12, 0, 'CAPACITY', '', '10', '0', '2018-06-29', '2018-06-29', '0000-00-00', '2018-06-29', '2019-06-29', '0000-00-00', NULL, 'Ada', NULL, 'Rusak', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('601121211110-1', 440, 'REG-440', '60112121', '1', 11, 0, 'PM', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('602121211120-1', 441, 'REG-441', '60212121', '1', 12, 0, 'CARMIX', '', '30', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('603121211120-1', 442, 'REG-442', '60312121', '1', 12, 0, 'PRECIO', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses');
INSERT INTO `t_ptera` (`kd_ptera`, `no_trans`, `no_reg`, `kd_pengguna`, `kd_jenis`, `kd_subjenis`, `kd_subsubjenis`, `merk`, `no_seri`, `kapasitas`, `stok_liter`, `tgl_daftar`, `tgl_bubuh`, `tgl_serah`, `terakhir_tera`, `masaberlaku`, `tgl_bayar`, `cap_tera`, `sertifikat`, `stiker`, `kondisi`, `tindakan`, `hasil_uji`, `keterangan`, `jml_bayar`, `status2`, `status`) VALUES
('604121211110-1', 443, 'REG-443', '60412121', '1', 11, 0, 'NS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('605121211110-1', 444, 'REG-444', '60512121', '1', 11, 0, 'SUN', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('606121211110-1', 445, 'REG-445', '60612121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Diganti', 'Sah', '', 0, 'baru', 'proses'),
('607121211120-1', 446, 'REG-446', '60712121', '1', 12, 0, 'GOATS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '2018-07-02', '2019-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('608121211110-1', 447, 'REG-447', '60812121', '1', 11, 0, 'RADJIN', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('609121211110-1', 448, 'REG-448', '60912121', '1', 11, 0, 'RADJIN', '', '10', '0', '2016-07-02', '2016-07-02', '0000-00-00', '2016-07-02', '2017-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6010121211110-1', 449, 'REG-449', '601012121', '1', 11, 0, 'CENTRAL', '', '10', '0', '2015-07-02', '0000-00-00', '0000-00-00', '2015-07-02', '2016-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6011121211110-1', 450, 'REG-450', '601112121', '1', 11, 0, 'UNI', '', '10', '0', '2006-07-02', '2006-07-02', '0000-00-00', '2006-07-02', '2007-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6012121211110-1', 451, 'REG-451', '601212121', '1', 11, 0, 'RAJA', '', '10', '0', '2014-07-02', '2014-07-02', '0000-00-00', '2014-07-02', '2015-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6012121211110-1', 452, 'REG-452', '601212121', '1', 11, 0, 'KSR', '', '10', '0', '2014-07-02', '2014-07-02', '0000-00-00', '2014-07-02', '2015-07-02', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6013121211120-1', 453, 'REG-453', '601312121', '1', 12, 0, 'WESTON', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6014121211110-1', 454, 'REG-454', '601412121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6015121211110-1', 455, 'REG-455', '601512121', '1', 11, 0, 'REMAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6015121211110-1', 456, 'REG-456', '601512121', '1', 11, 0, 'REMAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6016121211110-1', 457, 'REG-457', '601612121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6017121211110-1', 458, 'REG-458', '601712121', '1', 11, 0, 'RAHAYU', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6018121211110-1', 459, 'REG-459', '601812121', '1', 11, 0, 'PM', '', '10', '0', '0000-00-00', '2015-07-03', '0000-00-00', '2015-07-03', '2016-07-03', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6019121211110-1', 460, 'REG-460', '601912121', '1', 11, 0, 'PM', '', '10', '0', '2018-07-02', '2015-07-03', '0000-00-00', '2015-07-03', '2016-07-03', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6020121211120-1', 461, 'REG-461', '602012121', '1', 12, 0, 'CAMBA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6021121211110-1', 462, 'REG-462', '602112121', '1', 11, 0, 'KRS', '', '10', '0', '2016-07-02', '2016-07-03', '0000-00-00', '2016-07-03', '2017-07-03', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6022121211120-1', 463, 'REG-463', '602212121', '1', 12, 0, 'PRECIO', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6023121211150-1', 464, 'REG-464', '602312121', '1', 15, 0, 'KRISBOW', '', '3', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6024121211110-1', 465, 'REG-465', '602412121', '1', 11, 0, 'TS', '', '10', '0', '2015-07-02', '2015-07-03', '0000-00-00', '2015-07-03', '2016-07-03', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'expired'),
('6025121211110-1', 466, 'REG-466', '602512121', '1', 11, 0, 'CENTRAL', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6026121211110-1', 467, 'REG-467', '602612121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6027121211110-1', 468, 'REG-468', '602712121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, 'Baik', 'Tidak Diganti', 'Sah', '', 0, 'baru', 'proses'),
('6028121211110', 469, 'REG-469', '602812121', '1', 11, 0, 'DS', '', '10', '0', '2015-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6030121211110', 470, 'REG-470', '603012121', '1', 11, 0, '', '', '10', '0', '2014-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6031121211110', 471, 'REG-471', '603112121', '1', 11, 0, 'UNI', '', '10', '0', '2014-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6032121211110', 472, 'REG-472', '603212121', '1', 11, 0, 'PERTIS', '', '10', '0', '2014-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6032121211110', 473, 'REG-473', '603212121', '1', 11, 0, 'SWADAYA', '', '10', '0', '2014-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6033121211110', 474, 'REG-474', '603312121', '1', 11, 0, 'CENTRAL', '', '10', '0', '2014-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6034121211110', 475, 'REG-475', '603412121', '1', 11, 0, 'NS', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6034121211110', 476, 'REG-476', '603412121', '1', 11, 0, 'NSN', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6035121211110', 477, 'REG-477', '603512121', '1', 11, 0, 'RAJA', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6036121211150', 478, 'REG-478', '603612121', '1', 15, 0, 'PRECIO', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6036121211120', 479, 'REG-479', '603612121', '1', 12, 0, 'FIVEGOATS', '', '0', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6037121211150', 480, 'REG-480', '603712121', '1', 15, 0, 'PRECIO', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6038121211110', 481, 'REG-481', '603812121', '1', 11, 0, 'PTS', '', '10', '0', '2013-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6039121211110', 482, 'REG-482', '603912121', '1', 11, 0, 'CENTRAL', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6040121211130', 483, 'REG-483', '604012121', '1', 13, 0, '', '', '25', '0', '2015-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6040121211110', 484, 'REG-484', '604012121', '1', 11, 0, 'RAJA', '', '10', '0', '2015-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6041121211110', 485, 'REG-485', '604112121', '1', 11, 0, 'RAJA', '', '10', '0', '2007-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6042121211110', 486, 'REG-486', '604212121', '1', 11, 0, 'REMAJA', '', '10', '0', '2015-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2030121211120', 487, 'REG-487', '203012121', '1', 12, 0, '', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2031121211120', 488, 'REG-488', '203112121', '1', 12, 0, '20SO', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2032121211110', 489, 'REG-489', '203212121', '1', 11, 0, 'BIMA', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2033121211110', 490, 'REG-490', '203312121', '1', 11, 0, 'RAJA', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2034121211110', 491, 'REG-491', '203412121', '1', 11, 0, 'TB', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2034121211110', 492, 'REG-492', '203412121', '1', 11, 0, 'NAGAMI', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2035121211110', 493, 'REG-493', '203512121', '1', 11, 0, 'LOSO', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2036121211110', 494, 'REG-494', '203612121', '1', 11, 0, 'PDS', '', '10', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2037121211120', 495, 'REG-495', '203712121', '1', 12, 0, '', '', '15', '0', '2016-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2038121211110-1', 496, 'REG-496', '203812121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2039121211120-1', 497, 'REG-497', '203912121', '1', 12, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2040121211110-1', 498, 'REG-498', '204012121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2041121211110-1', 499, 'REG-499', '204112121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2042121211110-1', 500, 'REG-500', '204212121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2043121211120-1', 501, 'REG-501', '204312121', '1', 12, 0, '', '', '1', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2044121211110-1', 502, 'REG-502', '204412121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2045121211120-1', 503, 'REG-503', '204512121', '1', 12, 0, '', '', '10', '10', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2046121211110-1', 504, 'REG-504', '204612121', '1', 11, 0, 'PAMOR', '', '', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2047121211150-1', 505, 'REG-505', '204712121', '1', 15, 0, '', '', '30', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2048121211110-1', 506, 'REG-506', '204812121', '1', 11, 0, 'REMAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, 'Sah', '', 0, 'baru', 'proses'),
('2049121211110', 507, 'REG-507', '204912121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2050121211110', 508, 'REG-508', '205012121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2051121211110', 509, 'REG-509', '205112121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2052121211120', 510, 'REG-510', '205212121', '1', 12, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2053121211110', 511, 'REG-511', '205312121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2054121211110', 512, 'REG-512', '205412121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2055121211110', 513, 'REG-513', '205512121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2056121211110', 514, 'REG-514', '205612121', '1', 11, 0, 'DS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2057121211110', 515, 'REG-515', '205712121', '1', 11, 0, 'R4', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2058121211110', 516, 'REG-516', '205812121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2059121211110', 517, 'REG-517', '205912121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2061121211110', 518, 'REG-518', '206112121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2062121211110', 519, 'REG-519', '206212121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2063121211110', 520, 'REG-520', '206312121', '1', 11, 0, 'DS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2064121211120', 521, 'REG-521', '206412121', '1', 12, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2065121211110', 522, 'REG-522', '206512121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2066131311120', 523, 'REG-523', '206613131', '1', 12, 0, '', '', '15', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2067131311110', 524, 'REG-524', '206713131', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2068121211110', 525, 'REG-525', '206812121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2069121211120', 526, 'REG-526', '206912121', '1', 12, 0, '', '', '5', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2070121211110', 527, 'REG-527', '207012121', '1', 11, 0, 'DS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2071121211110', 528, 'REG-528', '207112121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2072121211110', 529, 'REG-529', '207212121', '1', 11, 0, 'DAMAI', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2073121211120', 530, 'REG-530', '207312121', '1', 12, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2074121211110', 531, 'REG-531', '207412121', '1', 11, 0, 'BIMA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2075121211110', 532, 'REG-532', '207512121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2076121211110', 533, 'REG-533', '207612121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2077121211110', 534, 'REG-534', '207712121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2078121211120', 535, 'REG-535', '207812121', '1', 12, 0, '', '', '5', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2079121211110', 536, 'REG-536', '207912121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2080121211110', 537, 'REG-537', '208012121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2081131311110', 538, 'REG-538', '208113131', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2082121211110', 539, 'REG-539', '208212121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2083121211110', 540, 'REG-540', '208312121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2084121211110', 541, 'REG-541', '208412121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2085121211110', 542, 'REG-542', '208512121', '1', 11, 0, 'UNI', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2086121211110', 543, 'REG-543', '208612121', '1', 11, 0, 'TIMBUL', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2087121211110', 544, 'REG-544', '208712121', '1', 11, 0, 'DS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2088121211110', 545, 'REG-545', '208812121', '1', 11, 0, 'WS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2089121211110', 546, 'REG-546', '208912121', '1', 11, 0, 'CENTRAL', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2090121211110', 547, 'REG-547', '209012121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2091121211110', 548, 'REG-548', '209112121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2092121211110', 549, 'REG-549', '209212121', '1', 11, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2093121211120', 550, 'REG-550', '209312121', '1', 12, 0, '', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2094121211110', 551, 'REG-551', '209412121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2095121211110', 552, 'REG-552', '209512121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-02', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2096121211110', 553, 'REG-553', '209612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2097121211150', 554, 'REG-554', '209712121', '1', 15, 0, '', '', '30', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2098121211110', 555, 'REG-555', '209812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('2099121211150', 556, 'REG-556', '209912121', '1', 15, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20100121211120', 557, 'REG-557', '2010012121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20101121211110', 558, 'REG-558', '2010112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('201021111120', 559, 'REG-559', '20102111', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20103121211110', 560, 'REG-560', '2010312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20104121211110', 561, 'REG-561', '2010412121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20105121211110', 562, 'REG-562', '2010512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20106121211110', 563, 'REG-563', '2010612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20107121211120', 564, 'REG-564', '2010712121', '1', 12, 0, '', '', '20', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20108121211110', 565, 'REG-565', '2010812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20109121211110', 566, 'REG-566', '2010912121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20110121211110', 567, 'REG-567', '2011012121', '1', 11, 0, 'RAJA', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20111121211110', 568, 'REG-568', '2011112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20112121211110', 569, 'REG-569', '2011212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20113121211110', 570, 'REG-570', '2011312121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20114121211110', 571, 'REG-571', '2011412121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20115121211110', 572, 'REG-572', '2011512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20116121211110', 573, 'REG-573', '2011612121', '1', 11, 0, 'DS', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20117121211110', 574, 'REG-574', '2011712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20118121211110', 575, 'REG-575', '2011812121', '1', 11, 0, 'KURNIA', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20119121211110', 576, 'REG-576', '2011912121', '1', 11, 0, 'SWADAYA', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20120121211110', 577, 'REG-577', '2012012121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20121121211110', 578, 'REG-578', '2012112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20122121211110', 579, 'REG-579', '2012212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20123121211110', 580, 'REG-580', '2012312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20124121211110', 581, 'REG-581', '2012412121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20125121211110', 582, 'REG-582', '2012512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20126121211110', 583, 'REG-583', '2012612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20127121211110', 584, 'REG-584', '2012712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20128121211110', 585, 'REG-585', '2012812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20129121211150', 586, 'REG-586', '2012912121', '1', 15, 0, 'SAYAKI', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20130121211110', 587, 'REG-587', '2013012121', '1', 11, 0, 'TB', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20131121211110', 588, 'REG-588', '2013112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20132121211110', 589, 'REG-589', '2013212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20133121211110', 590, 'REG-590', '2013312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20134121211110', 591, 'REG-591', '2013412121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20135121211110', 592, 'REG-592', '2013512121', '1', 11, 0, 'GS', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20136121211120', 593, 'REG-593', '2013612121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20137121211110', 594, 'REG-594', '2013712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20138121211110', 595, 'REG-595', '2013812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20139121211120', 596, 'REG-596', '2013912121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20140121211110', 597, 'REG-597', '2014012121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20141121211110', 598, 'REG-598', '2014112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20142121211110', 599, 'REG-599', '2014212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20143121211150', 600, 'REG-600', '2014312121', '1', 15, 0, '', '', '300', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20144121211110', 601, 'REG-601', '2014412121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20145121211120', 602, 'REG-602', '2014512121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20146121211110', 603, 'REG-603', '2014612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20147121211110', 604, 'REG-604', '2014712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20148121211110', 605, 'REG-605', '2014812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20149121211120', 606, 'REG-606', '2014912121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20150121211120', 607, 'REG-607', '2015012121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20151121211110', 608, 'REG-608', '2015112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20152121211110', 609, 'REG-609', '2015212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20153121211110', 610, 'REG-610', '2015312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20154121211120', 611, 'REG-611', '2015412121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20155121211110', 612, 'REG-612', '2015512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20156121211120', 613, 'REG-613', '2015612121', '1', 12, 0, '', '', '15', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20157121211150', 614, 'REG-614', '2015712121', '1', 15, 0, '', '', '50', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20158121211110', 615, 'REG-615', '2015812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20159121211110', 616, 'REG-616', '2015912121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20160121211110', 617, 'REG-617', '2016012121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20161121211110', 618, 'REG-618', '2016112121', '1', 11, 0, '', '', '30', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20162121211110', 619, 'REG-619', '2016212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20163121211110', 620, 'REG-620', '2016312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20164121211150', 621, 'REG-621', '2016412121', '1', 15, 0, '', '', '30', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20165121211110', 622, 'REG-622', '2016512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20166121211110', 623, 'REG-623', '2016612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20167121211110', 624, 'REG-624', '2016712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20168121211110', 625, 'REG-625', '2016812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20169121211110', 626, 'REG-626', '2016912121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20170121211110', 627, 'REG-627', '2017012121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20171121211150', 628, 'REG-628', '2017112121', '1', 15, 0, '', '', '30', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20172121211110', 629, 'REG-629', '2017212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20173121211110', 630, 'REG-630', '2017312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20174121211110', 631, 'REG-631', '2017412121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20175121211110', 632, 'REG-632', '2017512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20176121211110', 633, 'REG-633', '2017612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20177121211110', 634, 'REG-634', '2017712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20178121211110', 635, 'REG-635', '2017812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20179121211110', 636, 'REG-636', '2017912121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20180121211110', 637, 'REG-637', '2018012121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20181121211110', 638, 'REG-638', '2018112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20182121211110', 639, 'REG-639', '2018212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20183121211110', 640, 'REG-640', '2018312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20184121211150', 641, 'REG-641', '2018412121', '1', 15, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20185121211110', 642, 'REG-642', '2018512121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20186121211110', 643, 'REG-643', '2018612121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20187121211150', 644, 'REG-644', '2018712121', '1', 15, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20188121211110', 645, 'REG-645', '2018812121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20189121211110', 646, 'REG-646', '2018912121', '1', 11, 0, 'KRS', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20190121211110', 647, 'REG-647', '2019012121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20191121211110', 648, 'REG-648', '2019112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20192121211110', 649, 'REG-649', '2019212121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20193121211110', 650, 'REG-650', '2019312121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20194121211120', 651, 'REG-651', '2019412121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20195121211120', 652, 'REG-652', '2019512121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20196121211120', 653, 'REG-653', '2019612121', '1', 12, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20197121211110', 654, 'REG-654', '2019712121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20198121211170', 655, 'REG-655', '2019812121', '1', 17, 0, '', '', '300', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20199121211110', 656, 'REG-656', '2019912121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20200121211110', 657, 'REG-657', '2020012121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20201121211110', 658, 'REG-658', '2020112121', '1', 11, 0, '', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('20202121211110', 659, 'REG-659', '2020212121', '1', 11, 0, 'AB', '', '10', '0', '2018-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses');
INSERT INTO `t_ptera` (`kd_ptera`, `no_trans`, `no_reg`, `kd_pengguna`, `kd_jenis`, `kd_subjenis`, `kd_subsubjenis`, `merk`, `no_seri`, `kapasitas`, `stok_liter`, `tgl_daftar`, `tgl_bubuh`, `tgl_serah`, `terakhir_tera`, `masaberlaku`, `tgl_bayar`, `cap_tera`, `sertifikat`, `stiker`, `kondisi`, `tindakan`, `hasil_uji`, `keterangan`, `jml_bayar`, `status2`, `status`) VALUES
('6043121211110', 660, 'REG-660', '604312121', '1', 11, 0, 'SUN', '', '10', '0', '2007-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6044121211110', 661, 'REG-661', '604412121', '1', 11, 0, 'DS', '', '10', '0', '2013-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('6045121211170', 662, 'REG-662', '604512121', '1', 17, 0, '', '', '150', '0', '2007-07-03', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'proses'),
('501121211150-1', 663, 'REG-663', '50112121', '1', 15, 0, '', '', '', '0', '2018-09-26', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'ulang', 'proses');

-- --------------------------------------------------------

--
-- Table structure for table `t_ptera_online`
--

CREATE TABLE `t_ptera_online` (
  `kd_ptera` varchar(30) NOT NULL,
  `no_trans` int(10) NOT NULL,
  `no_reg` varchar(10) DEFAULT NULL,
  `kd_pengguna` varchar(12) NOT NULL,
  `kd_jenis` varchar(2) NOT NULL,
  `kd_subjenis` int(2) NOT NULL,
  `kd_subsubjenis` int(4) NOT NULL,
  `merk` varchar(50) NOT NULL,
  `no_seri` varchar(100) NOT NULL,
  `kapasitas` int(4) NOT NULL,
  `stok_liter` int(5) NOT NULL,
  `tgl_daftar` date DEFAULT NULL,
  `tgl_bubuh` date DEFAULT NULL,
  `tgl_serah` date NOT NULL,
  `terakhir_tera` date DEFAULT NULL,
  `masaberlaku` date DEFAULT NULL,
  `tgl_bayar` date NOT NULL,
  `cap_tera` enum('Ada','Tidak Ada') DEFAULT NULL,
  `sertifikat` enum('Ada','Tidak Ada') DEFAULT NULL,
  `stiker` enum('Ada','Tidak Ada') DEFAULT NULL,
  `kondisi` enum('Baik','Rusak') DEFAULT NULL,
  `tindakan` enum('Diganti','Tidak Diganti','DiTera') DEFAULT NULL,
  `hasil_uji` enum('Sah','Tidak Sah') DEFAULT NULL,
  `keterangan` text,
  `jml_bayar` int(10) DEFAULT NULL,
  `status2` enum('baru','ulang') NOT NULL,
  `status` enum('not_confirmed','confirmed') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ptera_online`
--

INSERT INTO `t_ptera_online` (`kd_ptera`, `no_trans`, `no_reg`, `kd_pengguna`, `kd_jenis`, `kd_subjenis`, `kd_subsubjenis`, `merk`, `no_seri`, `kapasitas`, `stok_liter`, `tgl_daftar`, `tgl_bubuh`, `tgl_serah`, `terakhir_tera`, `masaberlaku`, `tgl_bayar`, `cap_tera`, `sertifikat`, `stiker`, `kondisi`, `tindakan`, `hasil_uji`, `keterangan`, `jml_bayar`, `status2`, `status`) VALUES
('6018814410', 1, 'REG-1', '601881', '4', 41, 0, 'Logitek', '123456red', 3, 0, '2018-09-26', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'baru', 'not_confirmed');

-- --------------------------------------------------------

--
-- Table structure for table `t_subjenis`
--

CREATE TABLE `t_subjenis` (
  `kd_subjenis` varchar(4) NOT NULL,
  `kd_jenis` varchar(3) NOT NULL,
  `nm_subjenis` varchar(50) NOT NULL,
  `tarif_baru` int(6) NOT NULL,
  `tarif_ulang` int(11) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_subjenis`
--

INSERT INTO `t_subjenis` (`kd_subjenis`, `kd_jenis`, `nm_subjenis`, `tarif_baru`, `tarif_ulang`, `satuan`) VALUES
('0', '0', '-', 0, 0, '-'),
('11', '1', 'TIMBANGAN MEJA', 0, 0, 'Unit'),
('12', '1', 'TIMBANGAN PEGAS', 0, 0, 'Unit'),
('13', '1', 'TIMBANGAN DACIM LOGAM', 0, 0, 'Unit'),
('14', '1', 'TIMBANGAN SENTISIMAL', 0, 0, 'Unit'),
('15', '1', 'TIMBANGAN ELEKTRIK', 0, 0, 'Unit'),
('16', '1', 'TIMBANGAN DIGITAL', 0, 0, 'Unit'),
('17', '1', 'TIMBANGAN BOBOT INGSUT', 0, 0, 'Unit'),
('18', '1', 'Neraca', 0, 0, ''),
('21', '2', 'PREMIUM', 0, 0, 'Nosel'),
('22', '2', 'PERTAMAX', 0, 0, 'Unit'),
('23', '2', 'BIO SOLAR', 0, 0, 'Nosel'),
('24', '2', 'PERFORMANCE', 0, 0, ''),
('31', '3', 'METERAN AIR PDAM', 0, 0, 'Unit'),
('32', '3', 'METERAN LISTRIK PLN', 0, 0, 'Unit'),
('41', '4', 'TABUNG GAS 3 KG', 0, 0, 'Tabung'),
('42', '4', 'TABUNG GAS 12 KG', 0, 0, 'Tabung'),
('51', '5', 'Takaran Kering', 0, 0, ''),
('52', '5', 'Takaran Basah', 0, 0, ''),
('53', '5', 'Takaran Pengisi', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `t_subsubjenis`
--

CREATE TABLE `t_subsubjenis` (
  `kd_subsubjenis` varchar(5) NOT NULL,
  `kd_subjenis` varchar(3) NOT NULL,
  `nm_subsubjenis` varchar(50) NOT NULL,
  `tarif_baru` int(5) NOT NULL,
  `tarif_ulang` int(5) NOT NULL,
  `satuan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_subsubjenis`
--

INSERT INTO `t_subsubjenis` (`kd_subsubjenis`, `kd_subjenis`, `nm_subsubjenis`, `tarif_baru`, `tarif_ulang`, `satuan`) VALUES
('0', '0', '-', 0, 0, '-'),
('211', '21', 'PREMIUM-8', 0, 0, 'Tabung'),
('2110', '21', 'PREMIUM-093423', 0, 0, 'Nosel'),
('2111', '21', 'PREMIUM-1221', 0, 0, 'Nosel'),
('2112', '21', 'PREMIUM-2441', 0, 0, 'Nosel'),
('2113', '21', 'PREMIUM-C', 0, 0, 'Nosel'),
('2114', '21', 'PREMIUM-B', 0, 0, 'Nosel'),
('2115', '21', 'PREMIUM-D', 0, 0, 'Nosel'),
('2116', '21', 'PREMIUM-E', 0, 0, 'Nosel'),
('2117', '21', 'PREMIUM-A', 0, 0, ''),
('2118', '21', 'PREMIUM-B21', 0, 0, ''),
('212', '21', 'PREMIUM-6', 0, 0, 'Nosel'),
('213', '21', 'PREMIUM-2', 0, 0, 'Tabung'),
('214', '21', 'PREMIUM-4', 0, 0, 'Tabung'),
('215', '21', 'PREMIUM-9', 0, 0, 'Nosel'),
('216', '21', 'PREMIUM-B3', 0, 0, 'Nosel'),
('217', '21', 'PREMIUM-GS4', 0, 0, 'Nosel'),
('218', '21', 'PREMIUM-093422', 0, 0, 'Nosel'),
('219', '21', 'PREMIUM-093428', 0, 0, 'Tabung'),
('221', '22', 'PERTAMAX', 0, 0, 'Nosel'),
('2210', '22', 'PERTAMAX-Na2', 0, 0, 'Nosel'),
('2211', '22', 'PERTAMAX-820', 0, 0, 'Nosel'),
('2212', '22', 'PERTAMAX-818', 0, 0, 'Nosel'),
('2213', '22', 'PERTAMAX-137', 0, 0, 'Nosel'),
('2214', '22', 'PERTAMAX-A', 0, 0, 'Nosel'),
('2215', '22', 'PERTAMAX-E', 0, 0, 'Nosel'),
('222', '22', 'PERTAMAX-4', 0, 0, 'Nosel'),
('223', '22', 'PERTAMAX-2', 0, 0, 'Nosel'),
('224', '22', 'PERTAMAX-3', 0, 0, 'Nosel'),
('225', '22', 'PERTAMAX-B3', 0, 0, 'Nosel'),
('226', '22', 'PERTAMAX-BB3', 0, 0, 'Nosel'),
('227', '22', 'PERTAMAX-2441', 0, 0, 'Nosel'),
('228', '22', 'PERTAMAX-Na0', 0, 0, 'Nosel'),
('229', '22', 'PERTAMAX-Na1', 0, 0, 'Nosel'),
('231', '23', 'BIO SOLAR', 0, 0, 'Nosel'),
('232', '23', 'BIO SOLAR-2', 0, 0, 'Nosel'),
('233', '23', 'BIO SOLAR-B3', 0, 0, 'Nosel'),
('234', '23', 'BIO SOLAR-B21', 0, 0, 'Nosel'),
('235', '23', 'BIO SOLAR-Na2', 0, 0, 'Nosel'),
('236', '23', 'BIO SOLAR-Na2', 0, 0, 'Nosel'),
('237', '23', 'BIO SOLAR-D', 0, 0, 'Nosel'),
('238', '23', 'BIO SOLAR-4', 0, 0, 'Nosel'),
('239', '23', 'BIO SOLAR-8', 0, 0, 'Nosel'),
('241', '24', 'PERFORMANCE-92', 0, 0, 'Nosel'),
('242', '24', 'PERFORMANCE-95', 0, 0, 'Nosel'),
('243', '24', 'PERFORMANCE-DIESEL', 0, 0, 'Nosel');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(2) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nip` varchar(16) NOT NULL,
  `level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `username`, `password`, `nama`, `nip`, `level`) VALUES
(1, 'super_admin', '21232f297a57a5a743894a0e4a801fc3', 'Super Administrator', '534112292', 'super_admin'),
(2, 'penguji', '42f170485d11215eca9c39de11a3cc78', 'Penguji', '', 'penguji'),
(3, 'pembubuh', '6b836442462ed72b80de655d47edb056', 'Pembubuh', '', 'pembubuh'),
(4, 'penyerahan', '6623e918a81c4dffab6f135094c5af3a', 'Penyerah', '', 'penyerahan'),
(5, 'pendaftaran', 'beb9fb4aa4fbb632a8e029ca1c63e6af', 'Pendaftaran', '', 'pendaftaran'),
(7, 'pembayaran', 'f38cead502f18950326c255e478bedcd', 'Pembayaran', '', 'pembayaran');

-- --------------------------------------------------------

--
-- Table structure for table `view_grafik`
--

CREATE TABLE `view_grafik` (
  `kd_ptera` int(11) NOT NULL,
  `terakhir_tera` date NOT NULL,
  `masaberlaku` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_kecamatan`
--
ALTER TABLE `mst_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `tr_instansi`
--
ALTER TABLE `tr_instansi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jenis`
--
ALTER TABLE `t_jenis`
  ADD PRIMARY KEY (`kd_jenis`);

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD PRIMARY KEY (`kd_kategori`);

--
-- Indexes for table `t_kategori_sub`
--
ALTER TABLE `t_kategori_sub`
  ADD PRIMARY KEY (`kd_subkategori`);

--
-- Indexes for table `t_pengguna`
--
ALTER TABLE `t_pengguna`
  ADD PRIMARY KEY (`kd_pengguna`);

--
-- Indexes for table `t_pengguna_online`
--
ALTER TABLE `t_pengguna_online`
  ADD PRIMARY KEY (`kd_pengguna`);

--
-- Indexes for table `t_ptera`
--
ALTER TABLE `t_ptera`
  ADD PRIMARY KEY (`no_trans`),
  ADD UNIQUE KEY `no_reg` (`no_reg`);

--
-- Indexes for table `t_ptera_online`
--
ALTER TABLE `t_ptera_online`
  ADD PRIMARY KEY (`no_trans`),
  ADD UNIQUE KEY `no_reg` (`no_reg`);

--
-- Indexes for table `t_subjenis`
--
ALTER TABLE `t_subjenis`
  ADD PRIMARY KEY (`kd_subjenis`);

--
-- Indexes for table `t_subsubjenis`
--
ALTER TABLE `t_subsubjenis`
  ADD PRIMARY KEY (`kd_subsubjenis`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tr_instansi`
--
ALTER TABLE `tr_instansi`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_jenis`
--
ALTER TABLE `t_jenis`
  MODIFY `kd_jenis` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
  MODIFY `kd_kategori` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `t_ptera`
--
ALTER TABLE `t_ptera`
  MODIFY `no_trans` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=664;

--
-- AUTO_INCREMENT for table `t_ptera_online`
--
ALTER TABLE `t_ptera_online`
  MODIFY `no_trans` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
